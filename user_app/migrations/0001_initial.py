# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-12-13 20:28
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import user_app.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('knowledge', '0001_initial'),
        ('learn', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Achievement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='upload/achievement/', verbose_name='Изображение')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('desc', models.CharField(max_length=255, verbose_name='Описание')),
                ('notes', models.TextField(blank=True, verbose_name='заметки')),
                ('external_code_id', models.PositiveSmallIntegerField(default=0, help_text='Id дополнительного условия в коде проверки (если нет - 0)')),
            ],
            options={
                'verbose_name_plural': 'Ачивки',
                'verbose_name': 'Ачивка',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='AchievementLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('achievement', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='achievements', to='user_app.Achievement', verbose_name='Достижение')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='achievements', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Достижения',
                'verbose_name': 'Достижение',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
            ],
            options={
                'verbose_name_plural': 'Районы',
                'verbose_name': 'Район',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='BrowserData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('json_string', models.TextField(verbose_name='JSON')),
            ],
            options={
                'verbose_name_plural': 'Данные браузера',
                'verbose_name': 'Данные браузера',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('area', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.Area', verbose_name='Район')),
            ],
            options={
                'verbose_name_plural': 'Города',
                'verbose_name': 'Город',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Текст')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments_by', to=settings.AUTH_USER_MODEL, verbose_name='Автор')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Комментарии',
                'verbose_name': 'Комментарий',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='upload/country/', verbose_name='Флаг')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
            ],
            options={
                'verbose_name_plural': 'Страны',
                'verbose_name': 'Страна',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='LoginTry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login', models.CharField(max_length=255, verbose_name='Логин')),
                ('is_successful', models.BooleanField(default=False, verbose_name='Успешен')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('browser_data', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.BrowserData', verbose_name='Браузер')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Попытки входов',
                'verbose_name': 'Попытка входа',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='PageElement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('element_selector', models.CharField(max_length=255, verbose_name='Селектор')),
                ('element_type', models.CharField(blank=True, choices=[('P_BTN', 'Ссылка на страницу'), ('M_BTN', 'Пункт меню'), ('SHARE', 'Поделиться')], max_length=5, null=True, verbose_name='Тип')),
                ('desc', models.CharField(blank=True, max_length=255, verbose_name='Описание')),
            ],
            options={
                'verbose_name_plural': 'Элементы страницы',
                'verbose_name': 'Элемент страницы',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to=user_app.models.Profile.image_directory_path, verbose_name='Фото')),
                ('square_image', models.ImageField(blank=True, null=True, upload_to=user_app.models.Profile.square_directory_path, verbose_name='Квадратное фото')),
                ('is_real_image', models.NullBooleanField(verbose_name='Реальное фото')),
                ('last_name', models.CharField(blank=True, max_length=100, verbose_name='Фамилия')),
                ('name', models.CharField(blank=True, max_length=50, verbose_name='Имя')),
                ('surname', models.CharField(blank=True, max_length=80, verbose_name='Отчество')),
                ('birth_date', models.DateField(blank=True, null=True, verbose_name='Дата рождения')),
                ('sex', models.CharField(blank=True, choices=[('M', 'Мужской'), ('F', 'Женский')], max_length=1, null=True, verbose_name='Пол')),
                ('mobile_number', models.CharField(blank=True, max_length=20, verbose_name='Номер телефона')),
                ('is_mobile_confirmed', models.BooleanField(default=False, verbose_name='Телефон подтвержден')),
                ('email', models.EmailField(blank=True, max_length=100, null=True, verbose_name='Email')),
                ('is_email_confirmed', models.BooleanField(default=False, verbose_name='Email подтвержден')),
                ('vk_id', models.PositiveIntegerField(blank=True, null=True, verbose_name='Вконтакте привязка')),
                ('fb_id', models.PositiveIntegerField(blank=True, null=True, verbose_name='Facebook привязка')),
                ('ok_id', models.PositiveIntegerField(blank=True, null=True, verbose_name='Одноклассники привязка')),
                ('google_email', models.CharField(blank=True, max_length=200, null=True, verbose_name='Google привязка')),
                ('experience', models.PositiveIntegerField(default=0, verbose_name='Опыт')),
                ('level', models.PositiveIntegerField(default=0, verbose_name='Уровень')),
                ('coins', models.PositiveIntegerField(default=0, verbose_name='Монет')),
                ('cristals', models.PositiveIntegerField(default=0, verbose_name='Кристалов')),
                ('rubles', models.DecimalField(decimal_places=2, default=0, max_digits=19, verbose_name='Рублей')),
                ('profile_occupancy', models.PositiveSmallIntegerField(default=0, verbose_name='Заполненность')),
                ('last_notification_read', models.DateTimeField(auto_now_add=True, verbose_name='Последнее чтение уведомлений')),
                ('city', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.City', verbose_name='Город')),
                ('country', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.Country', verbose_name='Страна')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Профили',
                'verbose_name': 'Профиль',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('country', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.Country', verbose_name='Страна')),
            ],
            options={
                'verbose_name_plural': 'Регионы',
                'verbose_name': 'Регион',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('node', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='knowledge.GraphNode', verbose_name='Нод')),
            ],
            options={
                'verbose_name_plural': 'Типы навыков',
                'verbose_name': 'Тип навыка',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='SkillCache',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('level', models.PositiveSmallIntegerField(verbose_name='Уровень')),
                ('value', models.PositiveIntegerField(verbose_name='Очков')),
                ('skill', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.Skill', verbose_name='Навык')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Уровни навыков',
                'verbose_name': 'Уровень навыка',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='SkillHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField(null=True, verbose_name='Связанный объект')),
                ('event_type', models.CharField(choices=[('VV', 'Просмотрено видео'), ('TC', 'Выполнен тест'), ('SA', 'Набраны звезды'), ('MA', 'Выполнены доп. задания')], max_length=2, verbose_name='Событие')),
                ('points', models.PositiveIntegerField(default=0, verbose_name='Очков')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType', verbose_name='Тип контента')),
                ('skill', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.Skill', verbose_name='Навык')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Изменения навыков',
                'verbose_name': 'Изменение навыка',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='TaskSkill',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gives_experience', models.PositiveSmallIntegerField(verbose_name='Дает опыта')),
                ('skill', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.Skill', verbose_name='Навык')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='learn.Task', verbose_name='Задание')),
            ],
            options={
                'verbose_name_plural': 'Опыт навыков',
                'verbose_name': 'Опыт навыка',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField(null=True, verbose_name='Связанный объект')),
                ('transaction_type', models.CharField(choices=[('SU', 'Регистрация'), ('CA', 'Автоматическая проверка задания'), ('CM', 'Ручная проверка задания'), ('TB', 'Бонус за период'), ('CB', 'Бонус за вход'), ('P', 'Оплата'), ('B', 'Покупка')], max_length=3, verbose_name='Тип')),
                ('experience_points', models.IntegerField(default=0, verbose_name='Опыт')),
                ('coins', models.IntegerField(default=0, verbose_name='Монет')),
                ('cristals', models.IntegerField(default=0, verbose_name='Кристалов')),
                ('rubles', models.DecimalField(decimal_places=2, default=0, max_digits=19, verbose_name='Рублей')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('is_finished', models.BooleanField(default=False, verbose_name='Завершен')),
                ('content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType', verbose_name='Тип контента')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='transactions', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Транзакции',
                'verbose_name': 'Транзакция',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='UnwantedBehaviour',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('json_details', models.TextField(verbose_name='JSON')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
            ],
            options={
                'verbose_name_plural': 'Нежелательные поведения',
                'verbose_name': 'Нежелательное поведение',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Visit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
            ],
            options={
                'verbose_name_plural': 'Посещения',
                'verbose_name': 'Посещение',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='VisitClick',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event_type', models.CharField(choices=[('L', 'Left'), ('R', 'Right'), ('D', 'Double'), ('C', 'Center')], max_length=2, verbose_name='Событие')),
                ('x_pos', models.IntegerField(verbose_name='Позиция по x')),
                ('y_pos', models.IntegerField(verbose_name='Позиция по y')),
                ('millis', models.PositiveIntegerField(verbose_name='Миллисекунд')),
                ('element_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.PageElement', verbose_name='Элемент')),
            ],
            options={
                'verbose_name_plural': 'Клики',
                'verbose_name': 'Клик',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='VisitHover',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('millis', models.PositiveIntegerField(verbose_name='Миллисекунд')),
                ('element_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.PageElement', verbose_name='Элемент')),
            ],
            options={
                'verbose_name_plural': 'Наведения',
                'verbose_name': 'Наведение',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='VisitNext',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
            ],
            options={
                'verbose_name_plural': 'Переходы',
                'verbose_name': 'Переход',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Visitor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('browser_data', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.BrowserData', verbose_name='Браузер')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'Посетители',
                'verbose_name': 'Посетитель',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='VisitPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.TextField(verbose_name='Путь')),
                ('is_full', models.BooleanField(default=False, verbose_name='Полный путь')),
            ],
            options={
                'verbose_name_plural': 'Страницы',
                'verbose_name': 'Страница',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='VisitScroll',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('y_pos', models.IntegerField(verbose_name='Позиция по y')),
                ('browser_width', models.PositiveSmallIntegerField(verbose_name='Ширина браузера')),
                ('browser_height', models.PositiveSmallIntegerField(verbose_name='Высота браузера')),
                ('millis', models.PositiveIntegerField(verbose_name='Миллисекунд')),
            ],
            options={
                'verbose_name_plural': 'Скроллы',
                'verbose_name': 'Скролл',
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='VisitTrack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('visit_next', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.VisitNext', verbose_name='Посещение')),
            ],
            options={
                'verbose_name_plural': 'Треки',
                'verbose_name': 'Трек',
                'ordering': ('-pk',),
            },
        ),
        migrations.AddField(
            model_name='visitscroll',
            name='visit_track',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.VisitTrack', verbose_name='Трек'),
        ),
        migrations.AddField(
            model_name='visitnext',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.VisitPage', verbose_name='Страница'),
        ),
        migrations.AddField(
            model_name='visitnext',
            name='visit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.Visit', verbose_name='Визит'),
        ),
        migrations.AddField(
            model_name='visithover',
            name='visit_track',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.VisitTrack', verbose_name='Трек'),
        ),
        migrations.AddField(
            model_name='visitclick',
            name='visit_scroll',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.VisitScroll', verbose_name='Скролл / ресайз'),
        ),
        migrations.AddField(
            model_name='visit',
            name='full_page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='full_visits', to='user_app.VisitPage', verbose_name='Полный путь'),
        ),
        migrations.AddField(
            model_name='visit',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='visits', to='user_app.VisitPage', verbose_name='Путь'),
        ),
        migrations.AddField(
            model_name='visit',
            name='visitor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.Visitor', verbose_name='Посетитель'),
        ),
        migrations.AddField(
            model_name='unwantedbehaviour',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.VisitPage', verbose_name='Страница'),
        ),
        migrations.AddField(
            model_name='unwantedbehaviour',
            name='visitor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.Visitor', verbose_name='Посетитель'),
        ),
        migrations.AddField(
            model_name='pageelement',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_app.VisitPage', verbose_name='Страница'),
        ),
        migrations.AddField(
            model_name='city',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.Region', verbose_name='Регион'),
        ),
        migrations.AddField(
            model_name='area',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user_app.Region', verbose_name='Регион'),
        ),
    ]
