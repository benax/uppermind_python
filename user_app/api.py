from user_app import models
from user_app import serializers
from rest_framework import viewsets, permissions


class CountryViewSet(viewsets.ModelViewSet):
    """ViewSet for the Country class"""

    queryset = models.Country.objects.all()
    serializer_class = serializers.CountrySerializer
    permission_classes = [permissions.IsAuthenticated]


class RegionViewSet(viewsets.ModelViewSet):
    """ViewSet for the Region class"""

    queryset = models.Region.objects.all()
    serializer_class = serializers.RegionSerializer
    permission_classes = [permissions.IsAuthenticated]


class AreaViewSet(viewsets.ModelViewSet):
    """ViewSet for the Area class"""

    queryset = models.Area.objects.all()
    serializer_class = serializers.AreaSerializer
    permission_classes = [permissions.IsAuthenticated]


class CityViewSet(viewsets.ModelViewSet):
    """ViewSet for the City class"""

    queryset = models.City.objects.all()
    serializer_class = serializers.CitySerializer
    permission_classes = [permissions.IsAuthenticated]


class ProfileViewSet(viewsets.ModelViewSet):
    """ViewSet for the Profile class"""

    queryset = models.Profile.objects.all()
    serializer_class = serializers.ProfileSerializer
    permission_classes = [permissions.IsAuthenticated]


class TransactionViewSet(viewsets.ModelViewSet):
    """ViewSet for the Transaction class"""

    queryset = models.Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer
    permission_classes = [permissions.IsAuthenticated]


class AchievementViewSet(viewsets.ModelViewSet):
    """ViewSet for the Achievement class"""

    queryset = models.Achievement.objects.all()
    serializer_class = serializers.AchievementSerializer
    permission_classes = [permissions.IsAuthenticated]


class AchievementLinkViewSet(viewsets.ModelViewSet):
    """ViewSet for the AchievementLink class"""

    queryset = models.AchievementLink.objects.all()
    serializer_class = serializers.AchievementLinkSerializer
    permission_classes = [permissions.IsAuthenticated]


class VisitPageViewSet(viewsets.ModelViewSet):
    """ViewSet for the VisitPage class"""

    queryset = models.VisitPage.objects.all()
    serializer_class = serializers.VisitPageSerializer
    permission_classes = [permissions.IsAuthenticated]


class BrowserDataViewSet(viewsets.ModelViewSet):
    """ViewSet for the BrowserData class"""

    queryset = models.BrowserData.objects.all()
    serializer_class = serializers.BrowserDataSerializer
    permission_classes = [permissions.IsAuthenticated]


class VisitorViewSet(viewsets.ModelViewSet):
    """ViewSet for the Visitor class"""

    queryset = models.Visitor.objects.all()
    serializer_class = serializers.VisitorSerializer
    permission_classes = [permissions.IsAuthenticated]


class VisitViewSet(viewsets.ModelViewSet):
    """ViewSet for the Visit class"""

    queryset = models.Visit.objects.all()
    serializer_class = serializers.VisitSerializer
    permission_classes = [permissions.IsAuthenticated]


class VisitNextViewSet(viewsets.ModelViewSet):
    """ViewSet for the VisitNext class"""

    queryset = models.VisitNext.objects.all()
    serializer_class = serializers.VisitNextSerializer
    permission_classes = [permissions.IsAuthenticated]


class UnwantedBehaviourViewSet(viewsets.ModelViewSet):
    """ViewSet for the UnwantedBehaviour class"""

    queryset = models.UnwantedBehaviour.objects.all()
    serializer_class = serializers.UnwantedBehaviourSerializer
    permission_classes = [permissions.IsAuthenticated]


class SkillViewSet(viewsets.ModelViewSet):
    """ViewSet for the Skill class"""

    queryset = models.Skill.objects.all()
    serializer_class = serializers.SkillSerializer
    permission_classes = [permissions.IsAuthenticated]


class SkillCacheViewSet(viewsets.ModelViewSet):
    """ViewSet for the SkillCache class"""

    queryset = models.SkillCache.objects.all()
    serializer_class = serializers.SkillCacheSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskSkillViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskSkill class"""

    queryset = models.TaskSkill.objects.all()
    serializer_class = serializers.TaskSkillSerializer
    permission_classes = [permissions.IsAuthenticated]


class SkillHistoryViewSet(viewsets.ModelViewSet):
    """ViewSet for the SkillHistory class"""

    queryset = models.SkillHistory.objects.all()
    serializer_class = serializers.SkillHistorySerializer
    permission_classes = [permissions.IsAuthenticated]


class CommentViewSet(viewsets.ModelViewSet):
    """ViewSet for the Comment class"""

    queryset = models.Comment.objects.all()
    serializer_class = serializers.CommentSerializer
    permission_classes = [permissions.IsAuthenticated]


