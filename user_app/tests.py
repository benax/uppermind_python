import unittest
from django.core.urlresolvers import reverse
from django.test import Client
from .models import Country, Region, Area, City, Profile, Transaction, Achievement, AchievementLink, VisitPage, BrowserData, Evercookie, PageElement, Visitor, Visit, VisitNext, VisitTrack, VisitScroll, VisitClick, VisitHover, UnwantedBehaviour, Skill, SkillCache, TaskSkill, SkillHistory, Comment
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_country(**kwargs):
    defaults = {}
    defaults["image"] = "image"
    defaults["title"] = "title"
    defaults.update(**kwargs)
    return Country.objects.create(**defaults)


def create_region(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    if "country" not in defaults:
        defaults["country"] = create_'user_app_country'()
    return Region.objects.create(**defaults)


def create_area(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    if "region" not in defaults:
        defaults["region"] = create_'user_app_region'()
    return Area.objects.create(**defaults)


def create_city(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    if "region" not in defaults:
        defaults["region"] = create_'user_app_region'()
    if "area" not in defaults:
        defaults["area"] = create_'user_app_area'()
    return City.objects.create(**defaults)


def create_profile(**kwargs):
    defaults = {}
    defaults["image"] = "image"
    defaults["square_image"] = "square_image"
    defaults["is_real_image"] = "is_real_image"
    defaults["last_name"] = "last_name"
    defaults["name"] = "name"
    defaults["surname"] = "surname"
    defaults["birth_date"] = "birth_date"
    defaults["sex"] = "sex"
    defaults["mobile_number"] = "mobile_number"
    defaults["is_mobile_confirmed"] = "is_mobile_confirmed"
    defaults["email"] = "email"
    defaults["is_email_confirmed"] = "is_email_confirmed"
    defaults["vk_id"] = "vk_id"
    defaults["fb_id"] = "fb_id"
    defaults["ok_id"] = "ok_id"
    defaults["google_email"] = "google_email"
    defaults["experience"] = "experience"
    defaults["level"] = "level"
    defaults["coins"] = "coins"
    defaults["cristals"] = "cristals"
    defaults["rubles"] = "rubles"
    defaults["profile_occupancy"] = "profile_occupancy"
    defaults.update(**kwargs)
    if "user" not in defaults:
        defaults["user"] = create_user()
    if "country" not in defaults:
        defaults["country"] = create_'user_app_country'()
    if "city" not in defaults:
        defaults["city"] = create_'user_app_city'()
    return Profile.objects.create(**defaults)


def create_transaction(**kwargs):
    defaults = {}
    defaults["object_id"] = "object_id"
    defaults["transaction_type"] = "transaction_type"
    defaults["experience_points"] = "experience_points"
    defaults["coins"] = "coins"
    defaults["crystals"] = "crystals"
    defaults["rubles"] = "rubles"
    defaults["is_finished"] = "is_finished"
    defaults.update(**kwargs)
    if "user" not in defaults:
        defaults["user"] = create_user()
    if "content_type" not in defaults:
        defaults["content_type"] = create_contenttype()
    return Transaction.objects.create(**defaults)


def create_achievement(**kwargs):
    defaults = {}
    defaults["image"] = "image"
    defaults["title"] = "title"
    defaults["desc"] = "desc"
    defaults["notes"] = "notes"
    defaults["external_code_id"] = "external_code_id"
    defaults.update(**kwargs)
    return Achievement.objects.create(**defaults)


def create_achievementlink(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "achievement" not in defaults:
        defaults["achievement"] = create_'user_app_achievement'()
    if "user" not in defaults:
        defaults["user"] = create_user()
    return AchievementLink.objects.create(**defaults)


def create_visitpage(**kwargs):
    defaults = {}
    defaults["url"] = "url"
    defaults["is_full"] = "is_full"
    defaults.update(**kwargs)
    if "version" not in defaults:
        defaults["version"] = create_'main_version'()
    return VisitPage.objects.create(**defaults)


def create_browserdata(**kwargs):
    defaults = {}
    defaults["json_string"] = "json_string"
    defaults.update(**kwargs)
    return BrowserData.objects.create(**defaults)


def create_evercookie(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return Evercookie.objects.create(**defaults)


def create_pageelement(**kwargs):
    defaults = {}
    defaults["element_selector"] = "element_selector"
    defaults["element_type"] = "element_type"
    defaults["desc"] = "desc"
    defaults.update(**kwargs)
    if "page" not in defaults:
        defaults["page"] = create_'user_app_visitpage'()
    return PageElement.objects.create(**defaults)


def create_visitor(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "user" not in defaults:
        defaults["user"] = create_user()
    if "session" not in defaults:
        defaults["session"] = create_session()
    if "evercookie" not in defaults:
        defaults["evercookie"] = create_'user_app_evercookie'()
    if "browser_data" not in defaults:
        defaults["browser_data"] = create_'user_app_browserdata'()
    return Visitor.objects.create(**defaults)


def create_visit(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "visitor" not in defaults:
        defaults["visitor"] = create_'user_app_visitor'()
    if "page" not in defaults:
        defaults["page"] = create_'user_app_visitpage'()
    if "full_page" not in defaults:
        defaults["full_page"] = create_'user_app_visitpage'()
    return Visit.objects.create(**defaults)


def create_visitnext(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "visit" not in defaults:
        defaults["visit"] = create_'user_app_visit'()
    if "page" not in defaults:
        defaults["page"] = create_'user_app_visitpage'()
    return VisitNext.objects.create(**defaults)


def create_visittrack(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "visit_next" not in defaults:
        defaults["visit_next"] = create_'user_app_visitnext'()
    return VisitTrack.objects.create(**defaults)


def create_visitscroll(**kwargs):
    defaults = {}
    defaults["y_pos"] = "y_pos"
    defaults["browser_width"] = "browser_width"
    defaults["browser_height"] = "browser_height"
    defaults["millis"] = "millis"
    defaults.update(**kwargs)
    if "visit_track" not in defaults:
        defaults["visit_track"] = create_'user_app_visittrack'()
    return VisitScroll.objects.create(**defaults)


def create_visitclick(**kwargs):
    defaults = {}
    defaults["event_type"] = "event_type"
    defaults["x_pos"] = "x_pos"
    defaults["y_pos"] = "y_pos"
    defaults["millis"] = "millis"
    defaults.update(**kwargs)
    if "visit_scroll" not in defaults:
        defaults["visit_scroll"] = create_'user_app_visitscroll'()
    if "element_id" not in defaults:
        defaults["element_id"] = create_'user_app_pageelement'()
    return VisitClick.objects.create(**defaults)


def create_visithover(**kwargs):
    defaults = {}
    defaults["millis"] = "millis"
    defaults.update(**kwargs)
    if "visit_track" not in defaults:
        defaults["visit_track"] = create_'user_app_visittrack'()
    if "element_id" not in defaults:
        defaults["element_id"] = create_'user_app_pageelement'()
    return VisitHover.objects.create(**defaults)


def create_unwantedbehaviour(**kwargs):
    defaults = {}
    defaults["json_details"] = "json_details"
    defaults.update(**kwargs)
    if "visitor" not in defaults:
        defaults["visitor"] = create_visitor()
    if "page" not in defaults:
        defaults["page"] = create_'user_app_visitpage'()
    return UnwantedBehaviour.objects.create(**defaults)


def create_skill(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    if "node" not in defaults:
        defaults["node"] = create_'knowledge_graphnode'()
    return Skill.objects.create(**defaults)


def create_skillcache(**kwargs):
    defaults = {}
    defaults["level"] = "level"
    defaults["value"] = "value"
    defaults.update(**kwargs)
    if "skill" not in defaults:
        defaults["skill"] = create_'user_app_skill'()
    if "user" not in defaults:
        defaults["user"] = create_user()
    return SkillCache.objects.create(**defaults)


def create_taskskill(**kwargs):
    defaults = {}
    defaults["gives_experience"] = "gives_experience"
    defaults.update(**kwargs)
    if "task" not in defaults:
        defaults["task"] = create_'learn_task'()
    if "skill" not in defaults:
        defaults["skill"] = create_'user_app_skill'()
    return TaskSkill.objects.create(**defaults)


def create_skillhistory(**kwargs):
    defaults = {}
    defaults["object_id"] = "object_id"
    defaults["event_type"] = "event_type"
    defaults["points"] = "points"
    defaults.update(**kwargs)
    if "skill" not in defaults:
        defaults["skill"] = create_'user_app_skill'()
    if "user" not in defaults:
        defaults["user"] = create_user()
    if "content_type" not in defaults:
        defaults["content_type"] = create_contenttype()
    return SkillHistory.objects.create(**defaults)


def create_comment(**kwargs):
    defaults = {}
    defaults["text"] = "text"
    defaults.update(**kwargs)
    if "user" not in defaults:
        defaults["user"] = create_user()
    if "author" not in defaults:
        defaults["author"] = create_user()
    return Comment.objects.create(**defaults)


class CountryViewTest(unittest.TestCase):
    '''
    Tests for Country
    '''
    def setUp(self):
        self.client = Client()

    def test_list_country(self):
        url = reverse('user_app_country_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_country(self):
        url = reverse('user_app_country_create')
        data = {
            "image": "image",
            "title": "title",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_country(self):
        country = create_country()
        url = reverse('user_app_country_detail', args=[country.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_country(self):
        country = create_country()
        data = {
            "image": "image",
            "title": "title",
        }
        url = reverse('user_app_country_update', args=[country.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RegionViewTest(unittest.TestCase):
    '''
    Tests for Region
    '''
    def setUp(self):
        self.client = Client()

    def test_list_region(self):
        url = reverse('user_app_region_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_region(self):
        url = reverse('user_app_region_create')
        data = {
            "title": "title",
            "country": create_'user_app_country'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_region(self):
        region = create_region()
        url = reverse('user_app_region_detail', args=[region.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_region(self):
        region = create_region()
        data = {
            "title": "title",
            "country": create_'user_app_country'().pk,
        }
        url = reverse('user_app_region_update', args=[region.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AreaViewTest(unittest.TestCase):
    '''
    Tests for Area
    '''
    def setUp(self):
        self.client = Client()

    def test_list_area(self):
        url = reverse('user_app_area_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_area(self):
        url = reverse('user_app_area_create')
        data = {
            "title": "title",
            "region": create_'user_app_region'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_area(self):
        area = create_area()
        url = reverse('user_app_area_detail', args=[area.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_area(self):
        area = create_area()
        data = {
            "title": "title",
            "region": create_'user_app_region'().pk,
        }
        url = reverse('user_app_area_update', args=[area.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CityViewTest(unittest.TestCase):
    '''
    Tests for City
    '''
    def setUp(self):
        self.client = Client()

    def test_list_city(self):
        url = reverse('user_app_city_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_city(self):
        url = reverse('user_app_city_create')
        data = {
            "title": "title",
            "region": create_'user_app_region'().pk,
            "area": create_'user_app_area'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_city(self):
        city = create_city()
        url = reverse('user_app_city_detail', args=[city.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_city(self):
        city = create_city()
        data = {
            "title": "title",
            "region": create_'user_app_region'().pk,
            "area": create_'user_app_area'().pk,
        }
        url = reverse('user_app_city_update', args=[city.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProfileViewTest(unittest.TestCase):
    '''
    Tests for Profile
    '''
    def setUp(self):
        self.client = Client()

    def test_list_profile(self):
        url = reverse('user_app_profile_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_profile(self):
        url = reverse('user_app_profile_create')
        data = {
            "image": "image",
            "square_image": "square_image",
            "is_real_image": "is_real_image",
            "last_name": "last_name",
            "name": "name",
            "surname": "surname",
            "birth_date": "birth_date",
            "sex": "sex",
            "mobile_number": "mobile_number",
            "is_mobile_confirmed": "is_mobile_confirmed",
            "email": "email",
            "is_email_confirmed": "is_email_confirmed",
            "vk_id": "vk_id",
            "fb_id": "fb_id",
            "ok_id": "ok_id",
            "google_email": "google_email",
            "experience": "experience",
            "level": "level",
            "coins": "coins",
            "cristals": "cristals",
            "rubles": "rubles",
            "profile_occupancy": "profile_occupancy",
            "user": create_user().pk,
            "country": create_'user_app_country'().pk,
            "city": create_'user_app_city'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_profile(self):
        profile = create_profile()
        url = reverse('user_app_profile_detail', args=[profile.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_profile(self):
        profile = create_profile()
        data = {
            "image": "image",
            "square_image": "square_image",
            "is_real_image": "is_real_image",
            "last_name": "last_name",
            "name": "name",
            "surname": "surname",
            "birth_date": "birth_date",
            "sex": "sex",
            "mobile_number": "mobile_number",
            "is_mobile_confirmed": "is_mobile_confirmed",
            "email": "email",
            "is_email_confirmed": "is_email_confirmed",
            "vk_id": "vk_id",
            "fb_id": "fb_id",
            "ok_id": "ok_id",
            "google_email": "google_email",
            "experience": "experience",
            "level": "level",
            "coins": "coins",
            "cristals": "cristals",
            "rubles": "rubles",
            "profile_occupancy": "profile_occupancy",
            "user": create_user().pk,
            "country": create_'user_app_country'().pk,
            "city": create_'user_app_city'().pk,
        }
        url = reverse('user_app_profile_update', args=[profile.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TransactionViewTest(unittest.TestCase):
    '''
    Tests for Transaction
    '''
    def setUp(self):
        self.client = Client()

    def test_list_transaction(self):
        url = reverse('user_app_transaction_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_transaction(self):
        url = reverse('user_app_transaction_create')
        data = {
            "object_id": "object_id",
            "transaction_type": "transaction_type",
            "experience_points": "experience_points",
            "coins": "coins",
            "crystals": "crystals",
            "rubles": "rubles",
            "is_finished": "is_finished",
            "user": create_user().pk,
            "content_type": create_contenttype().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_transaction(self):
        transaction = create_transaction()
        url = reverse('user_app_transaction_detail', args=[transaction.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_transaction(self):
        transaction = create_transaction()
        data = {
            "object_id": "object_id",
            "transaction_type": "transaction_type",
            "experience_points": "experience_points",
            "coins": "coins",
            "crystals": "crystals",
            "rubles": "rubles",
            "is_finished": "is_finished",
            "user": create_user().pk,
            "content_type": create_contenttype().pk,
        }
        url = reverse('user_app_transaction_update', args=[transaction.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AchievementViewTest(unittest.TestCase):
    '''
    Tests for Achievement
    '''
    def setUp(self):
        self.client = Client()

    def test_list_achievement(self):
        url = reverse('user_app_achievement_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_achievement(self):
        url = reverse('user_app_achievement_create')
        data = {
            "image": "image",
            "title": "title",
            "desc": "desc",
            "notes": "notes",
            "external_code_id": "external_code_id",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_achievement(self):
        achievement = create_achievement()
        url = reverse('user_app_achievement_detail', args=[achievement.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_achievement(self):
        achievement = create_achievement()
        data = {
            "image": "image",
            "title": "title",
            "desc": "desc",
            "notes": "notes",
            "external_code_id": "external_code_id",
        }
        url = reverse('user_app_achievement_update', args=[achievement.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AchievementLinkViewTest(unittest.TestCase):
    '''
    Tests for AchievementLink
    '''
    def setUp(self):
        self.client = Client()

    def test_list_achievementlink(self):
        url = reverse('user_app_achievementlink_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_achievementlink(self):
        url = reverse('user_app_achievementlink_create')
        data = {
            "achievement": create_'user_app_achievement'().pk,
            "user": create_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_achievementlink(self):
        achievementlink = create_achievementlink()
        url = reverse('user_app_achievementlink_detail', args=[achievementlink.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_achievementlink(self):
        achievementlink = create_achievementlink()
        data = {
            "achievement": create_'user_app_achievement'().pk,
            "user": create_user().pk,
        }
        url = reverse('user_app_achievementlink_update', args=[achievementlink.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitPageViewTest(unittest.TestCase):
    '''
    Tests for VisitPage
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visitpage(self):
        url = reverse('user_app_visitpage_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visitpage(self):
        url = reverse('user_app_visitpage_create')
        data = {
            "url": "url",
            "is_full": "is_full",
            "version": create_'main_version'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visitpage(self):
        visitpage = create_visitpage()
        url = reverse('user_app_visitpage_detail', args=[visitpage.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visitpage(self):
        visitpage = create_visitpage()
        data = {
            "url": "url",
            "is_full": "is_full",
            "version": create_'main_version'().pk,
        }
        url = reverse('user_app_visitpage_update', args=[visitpage.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class BrowserDataViewTest(unittest.TestCase):
    '''
    Tests for BrowserData
    '''
    def setUp(self):
        self.client = Client()

    def test_list_browserdata(self):
        url = reverse('user_app_browserdata_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_browserdata(self):
        url = reverse('user_app_browserdata_create')
        data = {
            "json_string": "json_string",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_browserdata(self):
        browserdata = create_browserdata()
        url = reverse('user_app_browserdata_detail', args=[browserdata.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_browserdata(self):
        browserdata = create_browserdata()
        data = {
            "json_string": "json_string",
        }
        url = reverse('user_app_browserdata_update', args=[browserdata.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class EvercookieViewTest(unittest.TestCase):
    '''
    Tests for Evercookie
    '''
    def setUp(self):
        self.client = Client()

    def test_list_evercookie(self):
        url = reverse('user_app_evercookie_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_evercookie(self):
        url = reverse('user_app_evercookie_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_evercookie(self):
        evercookie = create_evercookie()
        url = reverse('user_app_evercookie_detail', args=[evercookie.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_evercookie(self):
        evercookie = create_evercookie()
        data = {
            "value": "value",
        }
        url = reverse('user_app_evercookie_update', args=[evercookie.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class PageElementViewTest(unittest.TestCase):
    '''
    Tests for PageElement
    '''
    def setUp(self):
        self.client = Client()

    def test_list_pageelement(self):
        url = reverse('user_app_pageelement_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_pageelement(self):
        url = reverse('user_app_pageelement_create')
        data = {
            "element_selector": "element_selector",
            "element_type": "element_type",
            "desc": "desc",
            "page": create_'user_app_visitpage'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_pageelement(self):
        pageelement = create_pageelement()
        url = reverse('user_app_pageelement_detail', args=[pageelement.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_pageelement(self):
        pageelement = create_pageelement()
        data = {
            "element_selector": "element_selector",
            "element_type": "element_type",
            "desc": "desc",
            "page": create_'user_app_visitpage'().pk,
        }
        url = reverse('user_app_pageelement_update', args=[pageelement.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitorViewTest(unittest.TestCase):
    '''
    Tests for Visitor
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visitor(self):
        url = reverse('user_app_visitor_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visitor(self):
        url = reverse('user_app_visitor_create')
        data = {
            "user": create_user().pk,
            "session": create_session().pk,
            "evercookie": create_'user_app_evercookie'().pk,
            "browser_data": create_'user_app_browserdata'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visitor(self):
        visitor = create_visitor()
        url = reverse('user_app_visitor_detail', args=[visitor.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visitor(self):
        visitor = create_visitor()
        data = {
            "user": create_user().pk,
            "session": create_session().pk,
            "evercookie": create_'user_app_evercookie'().pk,
            "browser_data": create_'user_app_browserdata'().pk,
        }
        url = reverse('user_app_visitor_update', args=[visitor.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitViewTest(unittest.TestCase):
    '''
    Tests for Visit
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visit(self):
        url = reverse('user_app_visit_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visit(self):
        url = reverse('user_app_visit_create')
        data = {
            "visitor": create_'user_app_visitor'().pk,
            "page": create_'user_app_visitpage'().pk,
            "full_page": create_'user_app_visitpage'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visit(self):
        visit = create_visit()
        url = reverse('user_app_visit_detail', args=[visit.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visit(self):
        visit = create_visit()
        data = {
            "visitor": create_'user_app_visitor'().pk,
            "page": create_'user_app_visitpage'().pk,
            "full_page": create_'user_app_visitpage'().pk,
        }
        url = reverse('user_app_visit_update', args=[visit.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitNextViewTest(unittest.TestCase):
    '''
    Tests for VisitNext
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visitnext(self):
        url = reverse('user_app_visitnext_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visitnext(self):
        url = reverse('user_app_visitnext_create')
        data = {
            "visit": create_'user_app_visit'().pk,
            "page": create_'user_app_visitpage'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visitnext(self):
        visitnext = create_visitnext()
        url = reverse('user_app_visitnext_detail', args=[visitnext.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visitnext(self):
        visitnext = create_visitnext()
        data = {
            "visit": create_'user_app_visit'().pk,
            "page": create_'user_app_visitpage'().pk,
        }
        url = reverse('user_app_visitnext_update', args=[visitnext.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitTrackViewTest(unittest.TestCase):
    '''
    Tests for VisitTrack
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visittrack(self):
        url = reverse('user_app_visittrack_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visittrack(self):
        url = reverse('user_app_visittrack_create')
        data = {
            "visit_next": create_'user_app_visitnext'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visittrack(self):
        visittrack = create_visittrack()
        url = reverse('user_app_visittrack_detail', args=[visittrack.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visittrack(self):
        visittrack = create_visittrack()
        data = {
            "visit_next": create_'user_app_visitnext'().pk,
        }
        url = reverse('user_app_visittrack_update', args=[visittrack.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitScrollViewTest(unittest.TestCase):
    '''
    Tests for VisitScroll
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visitscroll(self):
        url = reverse('user_app_visitscroll_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visitscroll(self):
        url = reverse('user_app_visitscroll_create')
        data = {
            "y_pos": "y_pos",
            "browser_width": "browser_width",
            "browser_height": "browser_height",
            "millis": "millis",
            "visit_track": create_'user_app_visittrack'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visitscroll(self):
        visitscroll = create_visitscroll()
        url = reverse('user_app_visitscroll_detail', args=[visitscroll.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visitscroll(self):
        visitscroll = create_visitscroll()
        data = {
            "y_pos": "y_pos",
            "browser_width": "browser_width",
            "browser_height": "browser_height",
            "millis": "millis",
            "visit_track": create_'user_app_visittrack'().pk,
        }
        url = reverse('user_app_visitscroll_update', args=[visitscroll.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitClickViewTest(unittest.TestCase):
    '''
    Tests for VisitClick
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visitclick(self):
        url = reverse('user_app_visitclick_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visitclick(self):
        url = reverse('user_app_visitclick_create')
        data = {
            "event_type": "event_type",
            "x_pos": "x_pos",
            "y_pos": "y_pos",
            "millis": "millis",
            "visit_scroll": create_'user_app_visitscroll'().pk,
            "element_id": create_'user_app_pageelement'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visitclick(self):
        visitclick = create_visitclick()
        url = reverse('user_app_visitclick_detail', args=[visitclick.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visitclick(self):
        visitclick = create_visitclick()
        data = {
            "event_type": "event_type",
            "x_pos": "x_pos",
            "y_pos": "y_pos",
            "millis": "millis",
            "visit_scroll": create_'user_app_visitscroll'().pk,
            "element_id": create_'user_app_pageelement'().pk,
        }
        url = reverse('user_app_visitclick_update', args=[visitclick.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VisitHoverViewTest(unittest.TestCase):
    '''
    Tests for VisitHover
    '''
    def setUp(self):
        self.client = Client()

    def test_list_visithover(self):
        url = reverse('user_app_visithover_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_visithover(self):
        url = reverse('user_app_visithover_create')
        data = {
            "millis": "millis",
            "visit_track": create_'user_app_visittrack'().pk,
            "element_id": create_'user_app_pageelement'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_visithover(self):
        visithover = create_visithover()
        url = reverse('user_app_visithover_detail', args=[visithover.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_visithover(self):
        visithover = create_visithover()
        data = {
            "millis": "millis",
            "visit_track": create_'user_app_visittrack'().pk,
            "element_id": create_'user_app_pageelement'().pk,
        }
        url = reverse('user_app_visithover_update', args=[visithover.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class UnwantedBehaviourViewTest(unittest.TestCase):
    '''
    Tests for UnwantedBehaviour
    '''
    def setUp(self):
        self.client = Client()

    def test_list_unwantedbehaviour(self):
        url = reverse('user_app_unwantedbehaviour_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_unwantedbehaviour(self):
        url = reverse('user_app_unwantedbehaviour_create')
        data = {
            "json_details": "json_details",
            "visitor": create_visitor().pk,
            "page": create_'user_app_visitpage'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_unwantedbehaviour(self):
        unwantedbehaviour = create_unwantedbehaviour()
        url = reverse('user_app_unwantedbehaviour_detail', args=[unwantedbehaviour.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_unwantedbehaviour(self):
        unwantedbehaviour = create_unwantedbehaviour()
        data = {
            "json_details": "json_details",
            "visitor": create_visitor().pk,
            "page": create_'user_app_visitpage'().pk,
        }
        url = reverse('user_app_unwantedbehaviour_update', args=[unwantedbehaviour.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SkillViewTest(unittest.TestCase):
    '''
    Tests for Skill
    '''
    def setUp(self):
        self.client = Client()

    def test_list_skill(self):
        url = reverse('user_app_skill_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_skill(self):
        url = reverse('user_app_skill_create')
        data = {
            "title": "title",
            "node": create_'knowledge_graphnode'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_skill(self):
        skill = create_skill()
        url = reverse('user_app_skill_detail', args=[skill.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_skill(self):
        skill = create_skill()
        data = {
            "title": "title",
            "node": create_'knowledge_graphnode'().pk,
        }
        url = reverse('user_app_skill_update', args=[skill.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SkillCacheViewTest(unittest.TestCase):
    '''
    Tests for SkillCache
    '''
    def setUp(self):
        self.client = Client()

    def test_list_skillcache(self):
        url = reverse('user_app_skillcache_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_skillcache(self):
        url = reverse('user_app_skillcache_create')
        data = {
            "level": "level",
            "value": "value",
            "skill": create_'user_app_skill'().pk,
            "user": create_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_skillcache(self):
        skillcache = create_skillcache()
        url = reverse('user_app_skillcache_detail', args=[skillcache.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_skillcache(self):
        skillcache = create_skillcache()
        data = {
            "level": "level",
            "value": "value",
            "skill": create_'user_app_skill'().pk,
            "user": create_user().pk,
        }
        url = reverse('user_app_skillcache_update', args=[skillcache.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskSkillViewTest(unittest.TestCase):
    '''
    Tests for TaskSkill
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskskill(self):
        url = reverse('user_app_taskskill_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskskill(self):
        url = reverse('user_app_taskskill_create')
        data = {
            "gives_experience": "gives_experience",
            "task": create_'learn_task'().pk,
            "skill": create_'user_app_skill'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskskill(self):
        taskskill = create_taskskill()
        url = reverse('user_app_taskskill_detail', args=[taskskill.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskskill(self):
        taskskill = create_taskskill()
        data = {
            "gives_experience": "gives_experience",
            "task": create_'learn_task'().pk,
            "skill": create_'user_app_skill'().pk,
        }
        url = reverse('user_app_taskskill_update', args=[taskskill.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SkillHistoryViewTest(unittest.TestCase):
    '''
    Tests for SkillHistory
    '''
    def setUp(self):
        self.client = Client()

    def test_list_skillhistory(self):
        url = reverse('user_app_skillhistory_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_skillhistory(self):
        url = reverse('user_app_skillhistory_create')
        data = {
            "object_id": "object_id",
            "event_type": "event_type",
            "points": "points",
            "skill": create_'user_app_skill'().pk,
            "user": create_user().pk,
            "content_type": create_contenttype().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_skillhistory(self):
        skillhistory = create_skillhistory()
        url = reverse('user_app_skillhistory_detail', args=[skillhistory.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_skillhistory(self):
        skillhistory = create_skillhistory()
        data = {
            "object_id": "object_id",
            "event_type": "event_type",
            "points": "points",
            "skill": create_'user_app_skill'().pk,
            "user": create_user().pk,
            "content_type": create_contenttype().pk,
        }
        url = reverse('user_app_skillhistory_update', args=[skillhistory.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CommentViewTest(unittest.TestCase):
    '''
    Tests for Comment
    '''
    def setUp(self):
        self.client = Client()

    def test_list_comment(self):
        url = reverse('user_app_comment_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_comment(self):
        url = reverse('user_app_comment_create')
        data = {
            "text": "text",
            "user": create_user().pk,
            "author": create_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_comment(self):
        comment = create_comment()
        url = reverse('user_app_comment_detail', args=[comment.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_comment(self):
        comment = create_comment()
        data = {
            "text": "text",
            "user": create_user().pk,
            "author": create_user().pk,
        }
        url = reverse('user_app_comment_update', args=[comment.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


