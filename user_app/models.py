from django.core.urlresolvers import reverse
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db.models import *
from django.db import models as models
from django.db import transaction
from django.contrib.auth.models import User
from django.db.models.signals import post_save
import uuid
import binascii
import os
import simplejson
import datetime
from django.utils import timezone



def change_user_money(self, transaction_type, coins=0, crystals=0, rubles=0, experience_points=0, content_object=None):
    if content_object:
        trans = Transaction.objects.create(user=self, coins=coins, crystals=crystals, rubles=rubles, experience_points=experience_points, transaction_type=transaction_type, content_object=content_object)
    else:
        trans = Transaction.objects.create(user=self, coins=coins, crystals=crystals, rubles=rubles, experience_points=experience_points, transaction_type=transaction_type)

    with transaction.atomic():
        profile = Profile.objects.select_for_update().get(user=self)
        if coins != 0:
            profile.coins += coins
        if crystals != 0:
            profile.crystals += crystals
        if rubles != 0:
            profile.rubles += rubles
        profile.updated_at = timezone.now()
        profile.save()
        trans.is_finished = True
        trans.save()




class Country(models.Model):
    image = ImageField("Флаг", null=True, blank=True, max_length=100, upload_to='upload/country/')

    title = CharField("Название", max_length=50)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'

    def __str__(self):
        return self.title



class Region(models.Model):
    country = ForeignKey('user_app.Country', verbose_name="Страна", on_delete=models.CASCADE, null=True, blank=True)

    title = CharField("Название", max_length=50)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'

    def __str__(self):
        return self.title



class Area(models.Model):
    region = ForeignKey('user_app.Region', verbose_name="Регион", on_delete=models.CASCADE, null=True, blank=True)

    title = CharField("Название", max_length=50)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Район'
        verbose_name_plural = 'Районы'

    def __str__(self):
        return self.title



class City(models.Model):
    region = ForeignKey('user_app.Region', verbose_name="Регион", on_delete=models.CASCADE, null=True, blank=True)
    area = ForeignKey('user_app.Area', verbose_name="Район", on_delete=models.CASCADE, null=True, blank=True)

    title = CharField("Название", max_length=50)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.title



class Profile(models.Model):
    SEX_CHOICES = (
        ('M', 'Мужской'),
        ('F', 'Женский'),
    )

    def image_directory_path(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return 'profile/user_{0}/{1}'.format(instance.user.id, filename)

    def square_directory_path(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return 'profile_square/user_{0}/{1}'.format(instance.user.id, filename)

    user = OneToOneField(User, unique=True, verbose_name="Пользователь", on_delete=models.CASCADE, related_name="profile")
    country = ForeignKey('user_app.Country', verbose_name="Страна", on_delete=models.CASCADE, null=True, blank=True)
    city = ForeignKey('user_app.City', verbose_name="Город", on_delete=models.CASCADE, null=True, blank=True)
    
    image = ImageField("Фото", null=True, blank=True, max_length=100, upload_to=image_directory_path)
    square_image = ImageField("Квадратное фото", null=True, blank=True, max_length=100, upload_to=square_directory_path)
    is_real_image = NullBooleanField("Реальное фото", blank=True, null=True)

    last_name = CharField("Фамилия", max_length=100, blank=True)
    name = CharField("Имя", max_length=50, blank=True)
    surname = CharField("Отчество", max_length=80, blank=True)
    birth_date = DateField("Дата рождения", blank=True, null=True)
    sex = CharField("Пол", max_length=1, choices=SEX_CHOICES, blank=True, null=True)

    mobile_number = CharField("Номер телефона", max_length=20, blank=True)
    is_mobile_confirmed = BooleanField("Телефон подтвержден", default=False)

    email = EmailField("Email", max_length=100, blank=True, null=True)
    is_email_confirmed = BooleanField("Email подтвержден", default=False)
    vk_email = CharField("Email из вк", max_length=255, blank=True)
    fb_email = CharField("Email из fb", max_length=255, blank=True)

    vk_id = PositiveIntegerField("Вконтакте привязка", blank=True, null=True)
    fb_id = PositiveIntegerField("Facebook привязка", blank=True, null=True)
    ok_id = PositiveIntegerField("Одноклассники привязка", blank=True, null=True)
    google_id = CharField("Google привязка", blank=True, null=True, max_length=200)

    experience = PositiveIntegerField("Опыт", default=0)
    level = PositiveIntegerField("Уровень", default=0)
    coins = PositiveIntegerField("Монет", default=0)
    crystals = PositiveIntegerField("Кристалов", default=0)
    rubles = DecimalField("Рублей", default=0, max_digits=19, decimal_places=2)
    profile_occupancy = PositiveSmallIntegerField("Заполненность", default=0)
    
    instagram = CharField("Instagram", max_length=50, blank=True)
    skype = CharField("Skype", max_length=50, blank=True)
    vk_public = CharField("Ссылка на профиль ВКонтакте", max_length=50, blank=True)
    fb_public = CharField("Ссылка на профиль Facebook", max_length=50, blank=True)
    
    # settings
    show_vk = BooleanField("Показывать профиль ВК", default=True)
    show_fb = BooleanField("Показывать профиль Facebook", default=True)
    show_ok = BooleanField("Показывать профиль Одноклассники", default=True)
    notify_check = BooleanField("Уведомлять о проверке преподавателем", default=True)
    notify_login_try = BooleanField("Уведомлять о попытках входа в аккаунт", default=True)

    rating = PositiveIntegerField("Рейтинг", default=0)
    last_notification_read = DateTimeField("Последнее чтение уведомлений", auto_now_add=True)
    updated_at = DateTimeField("Сохранен", auto_now_add=True)

    desired_fields = {
        'is_real_image': 20,
        'is_email_confirmed': 10,
        'is_mobile_confirmed': 10,
        'image': 10,
        'last_name': 10,
        'name': 10,
        'surname': 5,
        'birth_date': 10,
        'sex': 5,
        'country': 10,
        'city': 10,
        'vk_id': 10,
        'fb_id': 10,
        'ok_id': 10,
        'google_id': 10,
        'instagram': 5,
        'skype': 5,
        'vk_public': 5,
    }

    def save(self, *args, **kwargs):
        has_changes = False
        new_data = dict()
        if self.pk is not None:
            orig = Profile.objects.get(pk=self.pk)
        else:
            orig = Profile()

        for k in [
            'country', 
            'city', 
            'image', 
            'last_name', 
            'name', 
            'surname', 
            'birth_date', 
            'sex', 
            'mobile_number', 
            'email', 
            'vk_id', 
            'fb_id', 
            'ok_id', 
            'google_id', 
            'level', 
            'instagram', 
            'skype', 
            'show_vk', 
            'show_fb', 
            'show_ok',
            'notify_check',
            'notify_login_try',
        ]:
            if getattr(self, k) != getattr(orig, k) and not (getattr(self, k) == "" and getattr(orig, k) is None):
                has_changes = True
                if k == 'image':
                    new_data[k] = str(getattr(self, k))
                elif k == 'birth_date':
                    new_data[k] = str(getattr(self, k))
                else:
                    new_data[k] = getattr(self, k)

        if orig.image != self.image:
            self.is_real_image = None
            print(self.image)

        if has_changes:
            UseStat.objects.create(user=self.user, stat_type='CF', json_string=simplejson.dumps(new_data, ensure_ascii=False), content_object=self.user)
            self.updated_at = timezone.now()

        self.profile_occupancy = self.get_occupancy()
        super(Profile, self).save(*args, **kwargs)


    def get_occupancy(self):
        summ = 0
        for k, v in self.desired_fields.items():
            if getattr(self, k):
                summ += v
        return summ


    def __str__(self):
        return self.get_short_name()


    def get_short_name(self):
        arr = []
        if self.name:
            arr.append(self.name)
        if self.last_name:
            arr.append(self.last_name)
        if len(arr):
            return " ".join(arr)
        else:
            return self.user.username


    def get_full_name(self):
        arr = []
        if self.last_name:
            arr.append(self.last_name)
        if self.name:
            arr.append(self.name)
            if self.surname:
                arr.append(self.surname)
        if len(arr):
            return " ".join(arr)
        else:
            return self.user.username

    def recalc_money(self):
        coins = 0
        crystals = 0
        rubles = 0
        exp = 0
        for item in self.user.transactions.filter(is_active=True):
            coins += item.coins
            crystals += item.crystals
            rubles += item.rubles
            exp += item.experience_points
        
        self.coins = coins
        self.crystals = crystals
        self.rubles = rubles
        self.experience = exp
        self.save()


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


def create_profile(sender, instance, created, **kwargs):
    if created:
        new_profile = Profile.objects.create(user=instance)
        instance.change_user_money("SU", coins=500, crystals=5, content_object=instance)
        new_profile.recalc_money()


User.add_to_class("change_user_money",change_user_money)
post_save.connect(create_profile, sender=User)



class Transaction(models.Model):
    TYPE_CHOICES = (
        ('SU', 'Регистрация'),
        ('CA', 'Автоматическая проверка задания'),
        ('CM', 'Ручная проверка задания'),
        ('TB', 'Бонус за период'),
        ('CB', 'Бонус за вход'),
        ('P', 'Оплата'),
        ('B', 'Покупка'),
        ('CS', 'Отправка кода'),
        ('MB', 'Возрат денег'),
    )
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE, related_name="transactions")
    content_type = ForeignKey(ContentType, verbose_name="Тип контента", null=True, blank=True)
    object_id = PositiveIntegerField("Связанный объект", null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    
    transaction_type = CharField("Тип", max_length=3, choices=TYPE_CHOICES)
    experience_points = IntegerField("Опыта", default=0)
    coins = IntegerField("Монет", default=0)
    crystals = IntegerField("Кристалов", default=0)
    rubles = DecimalField("Рублей", default=0, max_digits=19, decimal_places=2)
    created_at = DateTimeField("Создан", auto_now_add=True)
    is_finished = BooleanField("Завершен", default=False)
    is_active = BooleanField("Активен", default=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Транзакция'
        verbose_name_plural = 'Транзакции'

    def __str__(self):
        arr = []
        if self.rubles != 0:
            arr.append("{} рублей".format(self.rubles))
        if self.crystals != 0:
            arr.append("{} кристалов".format(self.crystals))
        if self.coins != 0:
            arr.append("{} монет".format(self.coins))
        if self.experience_points != 0:
            arr.append("{} очков".format(self.experience_points))
        return "{} от {:%d.%m.%Y} {}".format(self.user.username, self.created_at, ", ".join(arr))



class AchievementGroup(models.Model):
    title = CharField("Название", max_length=50)

    class Meta:
        verbose_name = 'Группа ачивок'
        verbose_name_plural = 'Группы ачивок'

    def __str__(self):
        return self.title



class Achievement(models.Model):
    group = ForeignKey('user_app.AchievementGroup', verbose_name="Группа достижений", on_delete=models.CASCADE, null=True, blank=True, related_name="achievements")
    order_index = PositiveIntegerField("Индекс сортировки", default=1)
    image = ImageField("Изображение", null=True, blank=True, max_length=100, upload_to='upload/achievement/')
    title = CharField("Название", max_length=50)
    desc = CharField("Описание", max_length=255)
    notes = TextField("заметки", blank=True)
    uid = CharField("UID", max_length=255, unique=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Ачивка'
        verbose_name_plural = 'Ачивки'

    def __str__(self):
        return self.title



class AchievementLink(models.Model):
    achievement = ForeignKey('user_app.Achievement', verbose_name="Достижение", on_delete=models.CASCADE, null=True, blank=True)
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE, related_name="achievements")

    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Достижение'
        verbose_name_plural = 'Достижения'

    def __str__(self):
        return '%s' % self.pk



class VisitPage(models.Model):
    url = TextField("Путь")
    is_full = BooleanField("Полный путь", default=False)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'

    def __str__(self):
        return self.url



class FrontPage(models.Model):
    name = CharField("Название роута", max_length=255)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Страница фронтенда'
        verbose_name_plural = 'Страницы фронтенда'

    def __str__(self):
        return self.name



class FrontPageVisit(models.Model):
    page = ForeignKey('user_app.FrontPage', verbose_name="Страница")
    page_pk = PositiveIntegerField(blank=True, null=True)
    json_string = TextField("Дополнительный JSON", blank=True)
    

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Посещение страницы фронтенда'
        verbose_name_plural = 'Посещения страниц фронтенда'

    def __str__(self):
        return "{} {} {}".format(self.pk, self.page, self.page_pk)



class Value(models.Model):
    value = TextField("Значение")

    class Meta:
        verbose_name = 'Значение'
        verbose_name_plural = 'Значения'

    def __str__(self):
        return self.value


class BrowserData(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_agent_val = ForeignKey('user_app.Value', verbose_name='HTTP_USER_AGENT', blank=True, null=True, related_name='agents')
    http_accept_val = ForeignKey('user_app.Value', verbose_name='HTTP_ACCEPT', blank=True, null=True, related_name='accepts')
    accept_lang_val = ForeignKey('user_app.Value', verbose_name='HTTP_ACCEPT_LANGUAGE', blank=True, null=True, related_name='langs')
    accept_encoding_val = ForeignKey('user_app.Value', verbose_name='HTTP_ACCEPT_ENCODING', blank=True, null=True, related_name='encodings')
    x_forwarded_val = ForeignKey('user_app.Value', verbose_name='HTTP_X_FORWARDED_FOR', blank=True, null=True, related_name='forwards')
    remote_addr_val = ForeignKey('user_app.Value', verbose_name='REMOTE_ADDR', blank=True, null=True, related_name='remotes')
    guid_val = ForeignKey('user_app.Value', verbose_name='GUID', blank=True, null=True, related_name='guids')
    luid_val = ForeignKey('user_app.Value', verbose_name='LUID', blank=True, null=True, related_name='luids')


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Данные браузера'
        verbose_name_plural = 'Данные браузера'

    def __str__(self):
        return '%s' % self.pk


class FrontPageElement(models.Model):
    page = ForeignKey('user_app.FrontPage', verbose_name="Страница")
    element_id = PositiveIntegerField("Внутренний ID элемента")
    desc = CharField("Описание", max_length=255, blank=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Элемент страницы'
        verbose_name_plural = 'Элементы страницы'

    def __str__(self):
        return self.element_id



class Visitor(models.Model):
    user = ForeignKey(User, verbose_name="Пользователь", null=True, blank=True)
    browser_data = ForeignKey('user_app.BrowserData', verbose_name="Браузер")

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Посетитель'
        verbose_name_plural = 'Посетители'

    def __str__(self):
        return '%s' % self.pk



class Visit(models.Model):
    visitor = ForeignKey('user_app.Visitor', verbose_name="Посетитель")
    page = ForeignKey('user_app.VisitPage', verbose_name="Путь", related_name='visits')
    full_page = ForeignKey('user_app.VisitPage', verbose_name="Полный путь", related_name='full_visits')
    start_time = PositiveIntegerField("Время начала")
    time_shift = PositiveSmallIntegerField("Сдвиг времени")
    luid_val = ForeignKey('user_app.Value', verbose_name='LUID', blank=True, null=True)

    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Посещение'
        verbose_name_plural = 'Посещения'

    def __str__(self):
        return '%s' % self.pk


class VisitNext(models.Model):
    STATUS_CHOICES = (
        ('0', 'Запись'),
        ('1', 'Завершена'),
        ('2', 'Перед закрытием'),
    )
    visit = ForeignKey('user_app.Visit', verbose_name="Визит")
    page_visit = ForeignKey('user_app.FrontPageVisit', verbose_name="Посещение фронтенда")
    
    index = PositiveIntegerField("Индекс трека")
    status = CharField("Тип", max_length=1, choices=STATUS_CHOICES)
    offset = PositiveIntegerField("Сдвиг", default=0)
    history_length = PositiveSmallIntegerField("Длина истории")

    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Переход'
        verbose_name_plural = 'Переходы'

    def __str__(self):
        return '%s' % self.pk


class VisitScroll(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    y_pos = PositiveIntegerField("Сдвиг")
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Скролл'
        verbose_name_plural = 'Трек: Скроллы'

    def __str__(self):
        return '%s' % self.id


class VisitResize(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    w = PositiveIntegerField("Ширина")
    h = PositiveIntegerField("Высота")
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Ресайз'
        verbose_name_plural = 'Трек: Ресайзы'

    def __str__(self):
        return '%s' % self.id


class VisitMonitorChange(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    sw = PositiveSmallIntegerField("Ширина")
    sh = PositiveSmallIntegerField("Высота")
    asw = PositiveSmallIntegerField("Доступная ширина")
    ash = PositiveSmallIntegerField("Доступная высота")
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Изменение монитора'
        verbose_name_plural = 'Трек: Изменения мониторов'

    def __str__(self):
        return '%s' % self.id


class VisitKeyPress(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    ctrl_hold = BooleanField("Зажат Ctrl", default=False)
    shift_hold = BooleanField("Зажат Shift", default=False)
    alt_hold = BooleanField("Зажат Alt", default=False)
    key_code = SmallIntegerField("Код клавиши")
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Нажатие клавиш'
        verbose_name_plural = 'Трек: Нажатия клавиш'

    def __str__(self):
        return '%s' % self.id


class VisitActive(models.Model):
    TYPE_CHOICES = (
        ('1', 'Видимость вкладки'),
        ('2', 'Потенциальная (4 сек)'),
        ('3', 'document page'),
        ('4', 'window page'),
    )
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    event_type = CharField("Тип", max_length=1, choices=TYPE_CHOICES)
    is_active = BooleanField("Активен")
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Событие активности'
        verbose_name_plural = 'Трек: События активности'

    def __str__(self):
        return '%s' % self.id


class VisitMouseMove(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Движение мыши'
        verbose_name_plural = 'Трек: Движения мыши'

    def __str__(self):
        return '%s' % self.id


class VisitBuffer(models.Model):
    TYPE_CHOICES = (
        ('1', 'Вырезание'),
        ('2', 'Копирование'),
        ('3', 'Вставка'),
    )
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")

    event_type = CharField("Тип", max_length=1, choices=TYPE_CHOICES)

    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Событие буфера'
        verbose_name_plural = 'Трек: События буфера'

    def __str__(self):
        return '%s' % self.id


class VisitSelection(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")

    length = PositiveIntegerField("Длина выделения")

    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Событие выбора'
        verbose_name_plural = 'Трек: События выбора'

    def __str__(self):
        return '%s' % self.id


class VisitEditorSelection(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    length = PositiveIntegerField("Длина выделения")

    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Событие выбора в редакторе'
        verbose_name_plural = 'Трек: События выбора в редакторе'

    def __str__(self):
        return '%s' % self.id


class VisitClick(models.Model):
    BUTTON_CHOICES = (
        ('0', 'Левая'),
        ('1', 'Средняя'),
        ('2', 'Правая'),
        # IE 8-
        # 0 - left
        # 2 - right
        ('4', 'Средняя'), # IE 8-
    )
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    x_pos = PositiveSmallIntegerField("Сдвиг X")
    y_pos = PositiveSmallIntegerField("Сдвиг Y")
    button_type = CharField("Кнопка", max_length=1, choices=BUTTON_CHOICES)
    is_down = BooleanField("Нажата")
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Событие мыши'
        verbose_name_plural = 'Трек: События мыши'

    def __str__(self):
        return '%s' % self.id


class VisitElementClick(models.Model):
    visit_next = ForeignKey('user_app.VisitNext', verbose_name="Переход")
    
    element = ForeignKey(FrontPageElement, verbose_name='Элемент')
    
    time_shift = PositiveIntegerField("Время")
    track_number = PositiveIntegerField("Номер трека")

    class Meta:
        verbose_name = 'Трек: Нажатие на элемент'
        verbose_name_plural = 'Трек: Нажатия на элементы'

    def __str__(self):
        return '%s' % self.id





class UnwantedBehaviour(models.Model):
    visitor = ForeignKey(Visitor, verbose_name="Посетитель")
    page = ForeignKey('user_app.VisitPage', verbose_name="Страница")

    json_details = TextField("JSON")
    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Нежелательное поведение'
        verbose_name_plural = 'Нежелательные поведения'

    def __str__(self):
        return '%s' % self.pk



class Skill(models.Model):
    node = ForeignKey('knowledge.GraphNode', verbose_name="Нод", on_delete=models.CASCADE, null=True, blank=True)

    title = CharField("Название", max_length=50)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Тип навыка'
        verbose_name_plural = 'Типы навыков'

    def __str__(self):
        return self.title



class SkillCache(models.Model):
    skill = ForeignKey('user_app.Skill', verbose_name="Навык", on_delete=models.CASCADE, null=True, blank=True)
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE)

    level = PositiveSmallIntegerField("Уровень")
    value = PositiveIntegerField("Очков")

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Уровень навыка'
        verbose_name_plural = 'Уровни навыков'

    def __str__(self):
        return "{} {} {}".format(self.user.username, self.skill.title, self.level)



class TaskSkill(models.Model):
    task = ForeignKey('learn.Task', verbose_name="Задание")
    skill = ForeignKey('user_app.Skill', verbose_name="Навык")

    gives_experience = PositiveSmallIntegerField("Дает опыта")


    def __str__(self):
        return "{} {} {}".format(self.task.title, self.skill.title, self.gives_experience)


    class Meta:
        verbose_name = 'Опыт навыка'
        verbose_name_plural = 'Опыт навыков'



class SkillHistory(models.Model):
    EVENT_TYPES = (
        ('VV', 'Просмотрено видео'),
        ('TC', 'Выполнен тест'),
        ('SA', 'Набраны звезды'),
        ('MA', 'Выполнены доп. задания'),
    )
    skill = ForeignKey('user_app.Skill', verbose_name="Навык")
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE)

    content_type = ForeignKey(ContentType, verbose_name="Тип контента", null=True, blank=True)
    object_id = PositiveIntegerField("Связанный объект", null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    event_type = CharField("Событие", choices=EVENT_TYPES, max_length=2)
    points = PositiveIntegerField("Очков", default=0)
    created_at = DateTimeField("Создан", auto_now_add=True)


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Изменение навыка'
        verbose_name_plural = 'Изменения навыков'

    def __str__(self):
        return '%s' % self.pk


class Comment(models.Model): 
    user = ForeignKey(User, related_name='comments', verbose_name="Пользователь")
    author = ForeignKey(User, related_name='comments_by', verbose_name="Автор")
    text = TextField("Текст")
    created_at = DateTimeField("Создан", auto_now_add=True)
    

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'



class LoginTry(models.Model): 
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE, blank=True, null=True)
    login = CharField("Логин", max_length=255)
    is_successful = BooleanField("Успешен", default=False)
    browser_data = ForeignKey('user_app.BrowserData', verbose_name="Браузер")
    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Попытка входа'
        verbose_name_plural = 'Попытки входов'



class LoginTryVk(models.Model): 
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE, blank=True, null=True)
    vk_user_id = PositiveIntegerField("UID", default=0)
    code = CharField("Код", max_length=255, blank=True)
    access_token = CharField("Токен", max_length=255, blank=True)
    is_successful = BooleanField("Успешен", default=False)
    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Попытка входа VK'
        verbose_name_plural = 'Попытки входов VK'



class UseStat(models.Model):
    TYPE_CHOICES = (
        ('TED', 'удалил выполнение задания'),
        ('TER', 'вернул выполнение задания'),
        ('SAV', 'сохранил решение'),
        ('CF', 'изменил поля'),
    )
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE)
    content_type = ForeignKey(ContentType, verbose_name="Тип контента", null=True, blank=True)
    object_id = PositiveIntegerField("Связанный объект", null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    
    stat_type = CharField("Тип", max_length=4, choices=TYPE_CHOICES)
    json_string = TextField("JSON")
    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Транзакция данных'
        verbose_name_plural = 'Транзакции данных'

    def __str__(self):
        return "Пользователь {} {}".format(self.user, self.stat_type)


class Token(models.Model):
    key = CharField("Ключ", max_length=40, primary_key=True)
    user = ForeignKey(User, related_name='auth_tokens', on_delete=models.CASCADE, verbose_name="Пользователь")
    is_active = BooleanField("Активен", default=True)
    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        verbose_name = "Токен"
        verbose_name_plural = "Токены"

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return "{} {}".format(self.key, self.user)
