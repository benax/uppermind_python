from user_app import models
from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth import authenticate


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Country
        fields = (
            'pk', 
            'image', 
            'title', 
        )


class RegionSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Region
        fields = (
            'pk', 
            'title', 
        )


class AreaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Area
        fields = (
            'pk', 
            'title', 
        )


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.City
        fields = (
            'pk', 
            'title', 
        )


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Profile
        fields = (
            'pk', 
            'image', 
            'square_image', 
            'last_name', 
            'name', 
            'surname', 
            'sex', 
            'experience', 
            'level', 
            'rating',
        )

class FullProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Profile
        fields = (
            'pk', 
            'image', 
            'square_image', 
            'is_real_image', 
            'last_name', 
            'name', 
            'surname', 
            'birth_date', 
            'sex', 
            # 'mobile_number', 
            # 'is_mobile_confirmed', 
            # 'email', 
            # 'is_email_confirmed', 
            # 'vk_id', 
            # 'fb_id', 
            # 'ok_id', 
            # 'google_id', 
            'experience', 
            'level', 
            'coins', 
            'crystals', 
            'rubles', 
            'profile_occupancy', 
            'instagram',
            'skype',
            'rating',
            'vk_public',
            'fb_public',
        )


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('username', 'profile')


class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Transaction
        fields = (
            'pk', 
            'object_id', 
            'transaction_type', 
            'experience_points', 
            'coins', 
            'crystals', 
            'rubles', 
            'created_at', 
            'is_finished', 
        )


class AchievementSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Achievement
        fields = (
            'pk', 
            'image', 
            'title', 
            'desc', 
            'notes', 
        )


class AchievementLinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.AchievementLink
        fields = (
            'pk', 
            'created_at', 
        )


class VisitPageSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.VisitPage
        fields = (
            'pk', 
            'url', 
            'is_full', 
        )


class BrowserDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.BrowserData
        fields = (
            'pk', 
            'json_string', 
        )


class VisitorSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Visitor
        fields = (
            'pk', 
        )


class VisitSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Visit
        fields = (
            'pk', 
            'created_at', 
        )


class VisitNextSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.VisitNext
        fields = (
            'pk', 
            'created_at', 
        )


# class VisitTrackSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = models.VisitTrack
#         fields = (
#             'pk', 
#             'created_at', 
#         )


class UnwantedBehaviourSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.UnwantedBehaviour
        fields = (
            'pk', 
            'json_details', 
            'created_at', 
        )


class SkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Skill
        fields = (
            'pk', 
            'title', 
        )


class SkillCacheSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SkillCache
        fields = (
            'pk', 
            'level', 
            'value', 
        )


class TaskSkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TaskSkill
        fields = (
            'pk', 
            'gives_experience', 
        )


class SkillHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SkillHistory
        fields = (
            'pk', 
            'object_id', 
            'event_type', 
            'points', 
            'created_at', 
        )


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Comment
        fields = (
            'pk', 
            'text', 
            'created_at', 
        )



class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField(label="Логин")
    password = serializers.CharField(label="Пароль", style={'input_type': 'password'})

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = 'Аккаунт не активен.'
                    raise serializers.ValidationError(msg, code='authorization')
            else:
                msg = 'Невозможно войти с предоставленными данными.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Должен включать "Логин" и "Пароль".'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
