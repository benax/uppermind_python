from django.conf.urls import url, include
from rest_framework import routers
from user_app import api
from user_app import views


router = routers.DefaultRouter()
router.register(r'country', api.CountryViewSet)
router.register(r'region', api.RegionViewSet)
router.register(r'area', api.AreaViewSet)
router.register(r'city', api.CityViewSet)
router.register(r'profile', api.ProfileViewSet)
router.register(r'transaction', api.TransactionViewSet)
router.register(r'achievement', api.AchievementViewSet)
router.register(r'achievementlink', api.AchievementLinkViewSet)
router.register(r'visitpage', api.VisitPageViewSet)
router.register(r'browserdata', api.BrowserDataViewSet)
router.register(r'visitor', api.VisitorViewSet)
router.register(r'visit', api.VisitViewSet)
router.register(r'visitnext', api.VisitNextViewSet)
router.register(r'unwantedbehaviour', api.UnwantedBehaviourViewSet)
router.register(r'skill', api.SkillViewSet)
router.register(r'skillcache', api.SkillCacheViewSet)
router.register(r'taskskill', api.TaskSkillViewSet)
router.register(r'skillhistory', api.SkillHistoryViewSet)
router.register(r'comment', api.CommentViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    url(r'^api/v1/', include(router.urls)),

    url(r'^api/token_auth/', views.MyObtainAuthToken.as_view()),
    url(r'^api/logout/', views.api_logout),
    url(r'^api/initial/', views.api_initial),
    url(r'^api/visit_next/', views.api_visit_next),
    url(r'^api/my_profile/', views.api_my_profile),
    url(r'^api/get_notifications/', views.api_get_notifications),
    url(r'^api/get_new/', views.api_get_new),
    url(r'^api/visit_track/(?P<visit_pk>[0-9]+)', views.api_visit_track),
    url(r'^create_users/', views.create_users),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)

# urlpatterns += (
#     # urls for Profile
#     url(r'^profile/$', views.ProfileListView.as_view(), name='user_app_profile_list'),
#     url(r'^profile/create/$', views.ProfileCreateView.as_view(), name='user_app_profile_create'),
#     url(r'^profile/detail/(?P<pk>\S+)/$', views.ProfileDetailView.as_view(), name='user_app_profile_detail'),
#     url(r'^profile/update/(?P<pk>\S+)/$', views.ProfileUpdateView.as_view(), name='user_app_profile_update'),
# )

# urlpatterns += (
#     # urls for Comment
#     url(r'^comment/$', views.CommentListView.as_view(), name='user_app_comment_list'),
#     url(r'^comment/create/$', views.CommentCreateView.as_view(), name='user_app_comment_create'),
#     url(r'^comment/detail/(?P<pk>\S+)/$', views.CommentDetailView.as_view(), name='user_app_comment_detail'),
#     url(r'^comment/update/(?P<pk>\S+)/$', views.CommentUpdateView.as_view(), name='user_app_comment_update'),
# )

