from django import forms
from .models import Profile, Comment



class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'square_image', 'last_name', 'name', 'surname', 'birth_date', 'sex', 'mobile_number', 'email', 'country', 'city']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text', 'user']


