from django.contrib.auth.models import User
from django.db.models import *
from django.db import models as models


class Share(models.Model):
    SHARE_TYPES = (
        ('VK', 'ВКонтакте'),
        ('FB', 'Facebook'),
        ('OK', 'Одноклассники'),
        ('GP', 'Google+'),
    )
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE)
    visit = ForeignKey('user_app.Visit', verbose_name="Визит")
    share_type = CharField("Тип", choices=SHARE_TYPES, max_length=2)
    created_at = DateTimeField("Создан", auto_now_add=True)
    

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return '%s' % self.pk




class Game(models.Model): 
    title = CharField("Название", max_length=255)

