from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView, ListView, UpdateView, CreateView
from user_app.models import Country, Region, Area, City, Profile, Transaction, Achievement, AchievementLink, VisitPage, BrowserData, Visitor, Visit, VisitNext, UnwantedBehaviour, Skill, SkillCache, TaskSkill, SkillHistory, Comment, LoginTry, UseStat, Token, FrontPage, FrontPageVisit, VisitScroll, VisitResize, VisitMonitorChange, VisitKeyPress, VisitActive, VisitMouseMove, VisitBuffer, VisitSelection, VisitEditorSelection, VisitClick, VisitElementClick, Value
from user_app.forms import ProfileForm, CommentForm
from user_app.serializers import AuthTokenSerializer, FullProfileSerializer
from rest_framework import parsers, renderers
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from user_app.authentication import MyTokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
import rest_framework
from rest_framework.response import Response
from rest_framework.views import APIView
from learn.serializers import TaskConditionHistorySerializer, TaskExecutionForNotificationSerializer
import datetime
import json
from learn.models import Course, CourseExecution, TaskExecution, TaskConditionHistory, TaskCodeManualCheckNote
from django.contrib.auth.models import User, Group
from django.contrib.auth import logout
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType


F_VERSION = "2.1"

ADD_SCROLL = 0
ADD_RESIZE = 1
MONITOR_CHANGE = 2
KEY_PRESS = 3
SET_ACTIVE = 4
SET_MOUSE_MOVE = 5
SET_BUFFER = 6
SET_SELECTION = 7
SET_EDITOR_SELECTION = 8
CLICK = 9
ELEMENT_CLICK = 10



@login_required
def dashboard(request):
    return render(request, 'user_app/dashboard.html')


@login_required
def profile(request):
    item = request.user.profile
    return render(request, 'user_app/profile.html', {'item': item})


@login_required
def profile_edit(request):
    item = request.user.profile
    form = ProfileForm(request.POST or None, instance=item)
    return render(request, 'user_app/profile_edit.html', {'item': item, 'form': form})



def get_notifications_count(request):
    if request.user and request.user.is_authenticated():
        summ = 0

        # achieve TaskCondition
        summ += TaskConditionHistory.objects.filter(created_at__gt=request.user.profile.last_notification_read, task_exec__user=request.user, task_exec__is_active=True, is_correct=True).count()

        # TaskExecution was checked
        summ += TaskExecution.objects.filter(checked_at__gt=request.user.profile.last_notification_read, user=request.user).count()

        # money backs
        te_content_type = ContentType.objects.get_for_model(TaskExecution)
        summ += Transaction.objects.filter(created_at__gt=request.user.profile.last_notification_read, user=request.user, content_type__pk=te_content_type.id, transaction_type='MB').count()
        
        return summ
    return 0


class ProfileListView(ListView):
    model = Profile


class ProfileCreateView(CreateView):
    model = Profile
    form_class = ProfileForm


class ProfileDetailView(DetailView):
    model = Profile


class ProfileUpdateView(UpdateView):
    model = Profile
    form_class = ProfileForm


class CommentListView(ListView):
    model = Comment


class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentForm


class CommentDetailView(DetailView):
    model = Comment


class CommentUpdateView(UpdateView):
    model = Comment
    form_class = CommentForm


def get_browser_data(request):
    browser_data = {
        'HTTP_USER_AGENT': request.META.get('HTTP_USER_AGENT'),
        'HTTP_ACCEPT': request.META.get('HTTP_ACCEPT'),
        'HTTP_ACCEPT_LANGUAGE': request.META.get('HTTP_ACCEPT_LANGUAGE'),
        'HTTP_ACCEPT_ENCODING': request.META.get('HTTP_ACCEPT_ENCODING'),
        'HTTP_X_FORWARDED_FOR': request.META.get('HTTP_X_FORWARDED_FOR'),
        'REMOTE_ADDR': request.META.get('REMOTE_ADDR'),
        'guid': request.data.get('guid'),
        'luid': request.data.get('luid'),
    }
    entities = list(Value.objects.filter(value__in=set([x for x in browser_data.values() if x])))
    pairs = dict(
        HTTP_USER_AGENT='user_agent_val',
        HTTP_ACCEPT='http_accept_val',
        HTTP_ACCEPT_LANGUAGE='accept_lang_val',
        HTTP_ACCEPT_ENCODING='accept_encoding_val',
        HTTP_X_FORWARDED_FOR='x_forwarded_val',
        REMOTE_ADDR='remote_addr_val',
        guid='guid_val',
        luid='luid_val',
    )
    new_br_data = dict()
    for k, v in browser_data.items():
        if v:
            has_value = False
            # print(len(entities))
            for item in entities:
                if item.value == v:
                    new_br_data.update({ pairs[k]: item })
                    has_value = True
                    break
            if not has_value:
                val, created = Value.objects.get_or_create(value=v)
                new_br_data.update({ pairs[k]: val })
                entities.append(val)
        else:
            new_br_data.update({ pairs[k]: None })

    return BrowserData.objects.create(**new_br_data)


class MyObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer


    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            user = None
        login_try = LoginTry.objects.create(browser_data=get_browser_data(request), login=username, user=user)
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']
        token = Token.objects.create(user=user)
        
        login_try.is_successful = True
        login_try.save()
        return Response({
            'token': token.key, 
            'user': {
                'username': user.username, 
                'name': user.profile.name, 
                'last_name': user.profile.last_name, 
                'surname': user.profile.surname, 
                'coins': user.profile.coins, 
                'crystals': user.profile.crystals, 
                'rubles': user.profile.rubles, 
                'notifications_count': get_notifications_count(request)
            },
        })


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, ))
def api_logout(request):
    t_obj = MyTokenAuthentication()
    try:
        t_user, token = t_obj.authenticate(request)
        token.is_active = False
        token.save()
    except:
        save_log("api_logout", "logout get token error")
    logout(request)
    return Response({'detail': "Ok"})



@api_view(['POST'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_initial(request):
    user = None
    if request.data.get("url") is None:
        msg = "'url' is required"
        save_log("api_initial", msg)
        msg = "Требуется url"
        return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)

    visitor_data = dict(browser_data=get_browser_data(request))

    if request.user and request.user.is_authenticated():
        visitor_data['user'] = request.user

        user = {
            'username': request.user.username,
            'name': request.user.profile.name,
            'last_name': request.user.profile.last_name,
            'surname': request.user.profile.surname,
            'coins': request.user.profile.coins,
            'crystals': request.user.profile.crystals,
            'rubles': request.user.profile.rubles,
            'notifications_count': get_notifications_count(request)
        }

    visitor, created = Visitor.objects.get_or_create(**visitor_data)
    
    visit = Visit()
    visit.visitor = visitor
    if request.data.get('luid'):
        visit.luid_val, created = Value.objects.get_or_create(value=request.data.get('luid'))
    visit.start_time = request.data.get('start_time')
    visit.time_shift = request.data.get('time_shift')
    
    full_url = request.data.get("url", "/no_info")
    url = full_url.split('#')[0].split('?')[0]
    # print(url)
    page, created = VisitPage.objects.get_or_create(url=url)
    visit.page = page

    if full_url.count('?') > 0 or full_url.count('#') > 0:
        full_page, created = VisitPage.objects.get_or_create(url=full_url, is_full=True)
        visit.full_page = full_page
    else:
        visit.full_page = page

    visit.save()
    return Response({
        'user': user,
        'visit_id': visit.id,
        'f_version': F_VERSION,
    })


@api_view(['POST'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_visit_next(request):
    user = None
    visit_id_raw = request.data.get("visit_id", "")
    if not visit_id_raw.isdigit():
        msg = "Valid 'visit_id' is required"
        save_log("api_visit_next", msg + " - " + str(visit_id_raw))
        msg = "Требуется валидный идентификатор"
        return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)
    if request.data.get("url") is None:
        msg = "'url' is required"
        save_log("api_visit_next", msg)
        msg = "Требуется url"
        return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)

    try:
        visit = Visit.objects.get(pk=int(visit_id_raw))
    except Visit.DoesNotExist:
        save_log("api_visit_next", "HTTP_404_NOT_FOUND in " + str(visit_id_raw))
        return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

    if visit.visitor.user and visit.visitor.user != request.user:
        msg = "No permissions"
        save_log("api_visit_next", msg + " in " + str(task_execution.pk))
        msg = "Нет доступа"
        return Response({"detail": msg}, status=rest_framework.status.HTTP_403_FORBIDDEN)

    url = request.data.get("url").split('?')[0]

    visit_next = VisitNext()
    visit_next.visit = visit

    page, created = VisitPage.objects.get_or_create(url=url)
    visit_next.page = page
    visit_next.save()
    return Response({'visit_next_id': visit_next.id})



@api_view(['POST'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_visit_track(request, visit_pk):
    try:
        visit = Visit.objects.get(pk=visit_pk)
    except Visit.DoesNotExist:
        save_log("api_visit_track", "HTTP_404_NOT_FOUND in " + str(visit_pk))
        return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

    # if visit.visitor.user and visit.visitor.user != request.user:
    #     return Response({"detail": "No user permissions"}, status=rest_framework.status.HTTP_403_FORBIDDEN)

    if visit.luid_val.value != request.data.get('luid', 'no_track'):
        msg = "No permissions"
        save_log("api_visit_track", msg + " in " + str(visit.pk) + " with luid " + str(request.data.get('luid', 'no_track')))
        msg = "Нет доступа"
        return Response({"detail": msg}, status=rest_framework.status.HTTP_403_FORBIDDEN)

    new_data = dict(
        visit_id=request.data.get('visit_id'),
        start_time=request.data.get('start_time'),
        visits_next=list(),
    )

    # for k, v in request.data.items():
    #     if k == 'visits_next':
    #         print(k, ':')
    #         for item in v:
    #             print(item)
    #             print()
    #     else:
    #         print(k, ':', v)

    #     print()
    # print()

    for i in range(len(request.data.get('visits_next', list()))):
        real_count = 0
        count = len(request.data['visits_next'][i][5])
        item = request.data['visits_next'][i].copy()

        if item[0] == None:
            visit_next = VisitNext()
            visit_next.visit = visit
            
            page_name = item[4][1].get('name', "_no_page")
            params = item[4][1].get('params', dict())
            pk = params.get('pk')
            if pk:
                del params['pk']

            front_page, created = FrontPage.objects.get_or_create(name=page_name)
            
            page_visit = FrontPageVisit()
            page_visit.page = front_page
            page_visit.page_pk = pk
            if params:
                page_visit.json_string = json.dumps(params)
            page_visit.save()
        
            visit_next.page_visit = page_visit
            visit_next.index = 1
            visit_next.history_length = item[4][2]
            visit_next.save()
        else:
            visit_next = VisitNext.objects.get(pk=item[0])
            visit_next.index += 1


        for j in range(len(item[5])):
            if item[5][j][1] >= visit_next.offset:
                if len(item[5][j]) < 3:
                        raise Exception("Not enought data for add")
                time_shift = item[5][j][2]

                if item[5][j][1] == ADD_SCROLL:
                    if len(item[5][j]) < 4:
                        raise Exception("Not enought data for scroll data")
                    y_pos = item[5][j][3]
                    VisitScroll.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, y_pos=y_pos)
                elif item[5][j][1] == ADD_RESIZE:
                    if len(item[5][j]) < 5:
                        raise Exception("Not enought data for resize data")
                    w = item[5][j][3]
                    h = item[5][j][4]
                    VisitResize.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, w=w, h=h)
                elif item[5][j][1] == MONITOR_CHANGE:
                    if len(item[5][j]) < 7:
                        raise Exception("Not enought data for monitor data")
                    sw = item[5][j][3]
                    sh = item[5][j][4]
                    asw = item[5][j][5]
                    ash = item[5][j][6]
                    VisitMonitorChange.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, sw=sw, sh=sh, asw=asw, ash=ash)
                elif item[5][j][1] == KEY_PRESS:
                    if len(item[5][j]) < 7:
                        raise Exception("Not enought data for key data")
                    ctrl_hold = item[5][j][3] == 1
                    shift_hold = item[5][j][4] == 1
                    alt_hold = item[5][j][5] == 1
                    key_code = item[5][j][6]
                    VisitKeyPress.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, ctrl_hold=ctrl_hold, shift_hold=shift_hold, alt_hold=alt_hold, key_code=key_code)
                elif item[5][j][1] == SET_ACTIVE:
                    if len(item[5][j]) < 5:
                        raise Exception("Not enought data for active data")
                    # TODO: check data
                    is_active = item[5][j][3] == 1
                    event_type = str(item[5][j][4])
                    VisitActive.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, event_type=event_type, is_active=is_active)
                elif item[5][j][1] == SET_MOUSE_MOVE:
                    VisitMouseMove.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index)
                elif item[5][j][1] == SET_BUFFER:
                    if len(item[5][j]) < 4:
                        raise Exception("Not enought data for buffer data")
                    # TODO: check data
                    event_type = str(item[5][j][3])
                    VisitBuffer.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, event_type=event_type)
                elif item[5][j][1] == SET_SELECTION:
                    if len(item[5][j]) < 4:
                        raise Exception("Not enought data for selection data")
                    length = item[5][j][3]
                    VisitSelection.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, length=length)
                elif item[5][j][1] == SET_EDITOR_SELECTION:
                    if len(item[5][j]) < 4:
                        raise Exception("Not enought data for editor selection data")
                    length = item[5][j][3]
                    VisitSelection.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, length=length)
                    VisitEditorSelection.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, )
                elif item[5][j][1] == CLICK:
                    if len(item[5][j]) < 7:
                        raise Exception("Not enought data for click data")
                    x_pos = item[5][j][3]
                    y_pos = item[5][j][4]
                    # TODO: check data
                    button_type = str(item[5][j][5])
                    is_down = item[5][j][6] == 1
                    VisitClick.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, x_pos=x_pos, y_pos=y_pos, button_type=button_type, is_down=is_down)
                elif item[5][j][1] == ELEMENT_CLICK:
                    if len(item[5][j]) < 4:
                        raise Exception("Not enought data for element data")
                    e_id = item[5][j][3]
                    element = FrontPageElement.objects.get(element_id=e_id)
                    VisitElementClick.objects.create(visit_next=visit_next, time_shift=time_shift, track_number=visit_next.index, element=element)
                else:
                    save_log("api_visit_track", "Invalid event_type " + str(item[5][j][1]))
                    raise Exception("Invalid event type")
                real_count += 1

        item[0] = visit_next.pk
        #TODO: check
        if not isinstance(item[2], int) or (item[2] < 0 or item[2] > 2):
            save_log("api_visit_track", "Invalid visits_next status " + str(item[2]))
        visit_next.status = str(item[2])

        visit_next.offset += real_count
        visit_next.save()

        # print("count:", count, "real_count:", real_count)

        new_data['visits_next'].append(dict(
            id=item[0],
            index=item[1],
            count=count,
            real_count=real_count,
            offset=visit_next.offset,
            is_finished=item[2] > 0,
        ))

    return Response(new_data)



@api_view(['GET', 'POST'])
def create_users(request):
    users_list = [
        {
            'last_name': "Большова", 'name': "Юлия", 'surname': "Александровна", 'sex': "F",
            'username': "asdfghjkl", 
            "password": "7gdnoy",
            'in_group': True,
        },
        {
            'last_name': "Деменев", 'name': "Кирилл", 'surname': "Олегович", 'sex': "M",
            'username': "demenev.k",
            "password": "92knfn",
            'in_group': True,
        },
        {
            'last_name': "Дорибидонтов", 'name': "Юрий", 'surname': "Александрович", 'sex': "M",
            'username': "doribidontovyura", 
            "password": "0ig64x",
            'in_group': True,
        },
        {
            'last_name': "Карманов", 'name': "Сергей", 'surname': "Сергеевич", 'sex': "M",
            'username': "serega404", 
            "password": "pgk0rg",
            'in_group': True,
        },
        {
            'last_name': "Косатенко", 'name': "Александр", 'surname': "Адреевич", 'sex': "M",
            'username': "kosatenko.a",
            "password": "hnv7ez",
            'in_group': True,
        },
        {
            'last_name': "Краскова", 'name': "Евангелина", 'surname': "Александровна", 'sex': "F",
            'username': "kraskova.e",
            "password": "bdrmvg",
            'in_group': True,
        },
        {
            'last_name': "Курдиманов", 'name': "Максим", 'surname': "Егорович", 'sex': "M",
            'username': "kurdimanov.m",
            "password": "odqtlq",
            'in_group': True,
        },
        {
            'last_name': "Лезина", 'name': "Анастасия", 'surname': "Давидовна", 'sex': "F",
            'username': "lezinastya", 
            "password": "gke4og",
            'in_group': True,
        },
        {
            'last_name': "Лобач", 'name': "Богдан", 'surname': "Ярославович", 'sex': "M",
            'username': "Bach", 
            "password": "fxmpx3",
            'in_group': True,
        },
        {
            'last_name': "Люпа", 'name': "Ростислав", 'surname': "Александрович", 'sex': "M",
            'username': "u_rl_aka_sm", 
            "password": "flm7ro",
            'in_group': True,
        },
        {
            'last_name': "Мальцев", 'name': "Антон", 'surname': "Алексеевич", 'sex': "M",
            'username': "djack", 
            "password": "a7585954565152535",
            'in_group': True,
        },
        {
            'last_name': "Нацаков", 'name': "Иван", 'surname': "Владимирович", 'sex': "M",
            'username': "sloupok____228", 
            "password": "ix7kb8",
            'in_group': True,
        },
        {
            'last_name': "Пономарев", 'name': "Михаил", 'surname': "Михайлович", 'sex': "M",
            'username': "ponomarev.m",
            "password": "etvwyq",
            'in_group': True,
        },
        {
            'last_name': "Савельева", 'name': "Екатерина", 'surname': "Владиславовна", 'sex': "F",
            'username': "kss.sa", 
            "password": "2xpm6j",
            'in_group': True,
        },
        {
            'last_name': "Семченко", 'name': "Арсений", 'surname': "Геннадьевич", 'sex': "M",
            'username': "semchenko_senya7", 
            "password": "z2e6rg",
            'in_group': True,
        },
        {
            'last_name': "Сивокоз", 'name': "Артём", 'surname': "Владиславович", 'sex': "M",
            'username': "temik2005", 
            "password": "qj8n19",
            'in_group': True,
        },
        {
            'last_name': "Синица", 'name': "Алеся", 'surname': "Андреевна", 'sex': "F",
            'username': "sinitsa.a",
            "password": "ej0mck",
            'in_group': True,
        },
        {
            'last_name': "Сипиёв", 'name': "Артём", 'surname': "Владимирович", 'sex': "M",
            'username': "nubekiller24", 
            "password": "3xqq7e",
            'in_group': True,
        },
        {
            'last_name': "Сотников", 'name': "Кирилл", 'surname': "Денисович", 'sex': "M",
            'username': "988_ZzZ_ZzZ_889", 
            "password": "xp2m6j",
            'in_group': True,
        },
        {
            'last_name': "Хапёрский", 'name': "Егор", 'surname': "Сергеевич", 'sex': "M",
            'username': "qwert1324", 
            "password": "uc7um2",
            'in_group': True,
        },
        {
            'last_name': "Чичерин", 'name': "Никита", 'surname': "Сергеевич", 'sex': "M",
            'username': "chicherin.n",
            "password": "gzw1ou",
            'in_group': True,
        },

        {
            'last_name': "Тестовый", 'name': "Пользователь", 'surname': "", 'sex': "M",
            'username': "test_user",
            "password": "user1234",
            'in_group': True,
        },
        {
            'last_name': "Дмитренко", 'name': "Никита", 'surname': "", 'sex': "M",
            'username': "Adminvseyrussi",
            "password": "gzеаou",
        },
        {
            'last_name': "Яковенко", 'name': "Сергей", 'surname': "", 'sex': "M",
            'username': "Geespee",
            "password": "ea843sou",
        }, 
        {
            'last_name': "Чубов", 'name': "Антон", 'surname': "", 'sex': "M",
            'username': "insanecake",
            "password": "anton0000",
        },
        {
            'last_name': "Кондратов", 'name': "Вадим", 'surname': "", 'sex': "M",
            'username': "prabwa",
            "password": "ea83so7u",
        },
        {
            'last_name': "Кочубей", 'name': "Даниил", 'surname': "", 'sex': "M",
            'username': "kochubey",
            "password": "ea84so7u",
        },
        {
            'last_name': "Кувардин", 'name': "Артём", 'surname': "Вадимович", 'sex': "M",
            'username': "Honey_eclair",
            "password": "еw12ou",
        },
    ]

    if request.GET.get('method') == "POST":
        cpp_course = Course.objects.get(title="Основы C++ (осень 17-18)")
        group = Group.objects.get(name="CPP_17_18_1")
        
        for x in users_list:
            new_user = User.objects.create_user(x['username'], '', x['password'])
            new_user.last_name = x['last_name']
            new_user.save()
            new_user.profile.last_name = x['last_name']
            new_user.profile.name = x['name']
            new_user.profile.surname = x['surname']
            new_user.profile.sex = x['sex']
            new_user.profile.save()

            if x.get('in_group', False):
                new_user.groups.add(group)

            course_execution = CourseExecution()
            course_execution.user = new_user
            course_execution.course = cpp_course
            course_execution.save()
            course_execution.started_at = datetime.datetime(2017, 10, 9)
            course_execution.save()

        user = User.objects.get(username="loki912")
        course_execution = CourseExecution()
        course_execution.user = user
        course_execution.course = cpp_course
        course_execution.save()
        course_execution.started_at = datetime.datetime(2017, 10, 9)
        course_execution.save()

        return Response("ok")


    return Response(users_list)



@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_my_profile(request):
    serializer = FullProfileSerializer(request.user.profile)
    return Response({'profile': serializer.data})



@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_get_new(request):
    profile_update = None
    notifications_count = 0
    if request.user and request.user.is_authenticated(): 
        notifications_count = get_notifications_count(request)
        if request.user.profile.updated_at > timezone.now() - datetime.timedelta(seconds=5):
            profile_update = {
                'username': request.user.username,
                'name': request.user.profile.name,
                'last_name': request.user.profile.last_name,
                'surname': request.user.profile.surname,
                'coins': request.user.profile.coins,
                'crystals': request.user.profile.crystals,
                'rubles': request.user.profile.rubles,
            }

    return Response({'profile_update': profile_update, 'notifications_count': notifications_count, 'f_version': F_VERSION})



@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_get_notifications(request):
    notifications_list = list()

    if request.user and request.user.is_authenticated():
        # correct contitions
        for cond_his_obj in TaskConditionHistory.objects.filter(created_at__gt=request.user.profile.last_notification_read, task_exec__user=request.user, task_exec__is_active=True, is_correct=True):
            notifications_list.append(dict(type="condition", obj=TaskConditionHistorySerializer(cond_his_obj).data, event_time=cond_his_obj.created_at, data=dict()))

        # checked TaskExecutions by teacher
        for task_exec_check_obj in TaskExecution.objects.filter(checked_at__gt=request.user.profile.last_notification_read, user=request.user):
            notifications_list.append(dict(type="task_check", obj=TaskExecutionForNotificationSerializer(task_exec_check_obj).data, event_time=task_exec_check_obj.checked_at, data=dict()))

        # money backs
        te_content_type = ContentType.objects.get_for_model(TaskExecution)
        for trans_obj in Transaction.objects.filter(created_at__gt=request.user.profile.last_notification_read, user=request.user, transaction_type='MB', content_type__pk=te_content_type.id):
            notifications_list.append(dict(type="money_back_te", obj=TaskExecutionForNotificationSerializer(trans_obj.content_object).data, event_time=trans_obj.created_at, data=dict(coins=trans_obj.coins, crystals=trans_obj.crystals, rubles=trans_obj.rubles)))

        request.user.profile.last_notification_read = timezone.now()
        request.user.profile.save()

    return Response({'notifications_list': notifications_list})
