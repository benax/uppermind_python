from django.contrib import admin
from django import forms
from user_app.models import Country, Region, Area, City, Profile, Transaction, Achievement, AchievementLink, VisitPage, BrowserData, Visitor, Visit, VisitNext, UnwantedBehaviour, Skill, SkillCache, TaskSkill, SkillHistory, Comment, LoginTry, Token, AchievementGroup, FrontPage, FrontPageVisit, FrontPageElement, VisitScroll, VisitResize, VisitMonitorChange, VisitKeyPress, VisitActive, VisitMouseMove, VisitBuffer, VisitSelection, VisitEditorSelection, VisitClick, VisitElementClick, UseStat



class CountryAdminForm(forms.ModelForm):

    class Meta:
        model = Country
        fields = '__all__'


class CountryAdmin(admin.ModelAdmin):
    form = CountryAdminForm
    list_display = ['image', 'title']
    #readonly_fields = ['image', 'title']

admin.site.register(Country, CountryAdmin)


class RegionAdminForm(forms.ModelForm):

    class Meta:
        model = Region
        fields = '__all__'


class RegionAdmin(admin.ModelAdmin):
    form = RegionAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(Region, RegionAdmin)


class AreaAdminForm(forms.ModelForm):

    class Meta:
        model = Area
        fields = '__all__'


class AreaAdmin(admin.ModelAdmin):
    form = AreaAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(Area, AreaAdmin)


class CityAdminForm(forms.ModelForm):

    class Meta:
        model = City
        fields = '__all__'


class CityAdmin(admin.ModelAdmin):
    form = CityAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(City, CityAdmin)



def profile_recalc_money(modeladmin, request, queryset):
    for x in queryset:
        x.recalc_money()
profile_recalc_money.short_description = "Пересчитать валюту"


class ProfileAdminForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = '__all__'


class ProfileAdmin(admin.ModelAdmin):
    form = ProfileAdminForm
    list_display = ['id', 'user_username', 'last_name', 'name', 'surname', 'sex', 'level', 'coins', 'crystals', 'rubles', 'updated_at', 'rating', 'last_notification_read']
    readonly_fields = ['experience', 'level', 'coins', 'crystals', 'rubles', 'profile_occupancy', 'last_notification_read', 'updated_at']
    list_filter = ['sex', 'level', 'is_mobile_confirmed', 'is_email_confirmed']
    actions = [profile_recalc_money]

    def user_username(self, obj):
        return obj.user.username
    user_username.short_description = 'Логин'

admin.site.register(Profile, ProfileAdmin)


class TransactionAdminForm(forms.ModelForm):

    class Meta:
        model = Transaction
        fields = '__all__'


class TransactionAdmin(admin.ModelAdmin):
    form = TransactionAdminForm
    list_display = ['id', 'user', 'content_type', 'object_id', 'transaction_type', 'experience_points', 'coins', 'crystals', 'rubles', 'created_at', 'is_finished']
    list_filter = ['transaction_type', 'content_type', 'is_finished']
    readonly_fields = ['created_at']
    # autocomplete_lookup_fields = {
    #     'content_object': [['content_type', 'object_id']],
    # }
    #readonly_fields = ['object_id', 'content_type']

    def save_model(self, request, obj, form, change):
        super(TransactionAdmin, self).save_model(request, obj, form, change)
        if obj.user:
            obj.user.profile.recalc_money()


admin.site.register(Transaction, TransactionAdmin)



class AchievementGroupAdminForm(forms.ModelForm):

    class Meta:
        model = AchievementGroup
        fields = '__all__'


class AchievementGroupAdmin(admin.ModelAdmin):
    form = AchievementGroupAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(AchievementGroup, AchievementGroupAdmin)



class AchievementAdminForm(forms.ModelForm):

    class Meta:
        model = Achievement
        fields = '__all__'


class AchievementAdmin(admin.ModelAdmin):
    form = AchievementAdminForm
    list_display = ['image', 'title', 'desc', 'notes', 'uid']
    #readonly_fields = ['image', 'title', 'desc', 'notes', 'uid']

admin.site.register(Achievement, AchievementAdmin)


class AchievementLinkAdminForm(forms.ModelForm):

    class Meta:
        model = AchievementLink
        fields = '__all__'


class AchievementLinkAdmin(admin.ModelAdmin):
    form = AchievementLinkAdminForm
    list_display = ['created_at']
    #readonly_fields = ['created_at']

admin.site.register(AchievementLink, AchievementLinkAdmin)


class VisitPageAdminForm(forms.ModelForm):

    class Meta:
        model = VisitPage
        fields = '__all__'


class VisitPageAdmin(admin.ModelAdmin):
    form = VisitPageAdminForm
    list_display = ['url', 'is_full']
    #readonly_fields = ['url', 'is_full']

admin.site.register(VisitPage, VisitPageAdmin)


class BrowserDataAdminForm(forms.ModelForm):

    class Meta:
        model = BrowserData
        fields = '__all__'


class BrowserDataAdmin(admin.ModelAdmin):
    form = BrowserDataAdminForm
    list_display = ['user_agent_val', 'remote_addr_val', 'guid_val']
    #readonly_fields = ['user_agent_val']

admin.site.register(BrowserData, BrowserDataAdmin)



class VisitorAdminForm(forms.ModelForm):

    class Meta:
        model = Visitor
        fields = '__all__'


class VisitorAdmin(admin.ModelAdmin):
    form = VisitorAdminForm
    list_display = ['id', 'user']


admin.site.register(Visitor, VisitorAdmin)


class VisitAdminForm(forms.ModelForm):

    class Meta:
        model = Visit
        fields = '__all__'


class VisitAdmin(admin.ModelAdmin):
    form = VisitAdminForm
    list_display = ['id', 'visitor', 'page', 'created_at']
    #readonly_fields = ['created_at']

admin.site.register(Visit, VisitAdmin)


class VisitNextAdminForm(forms.ModelForm):

    class Meta:
        model = VisitNext
        fields = '__all__'


class VisitNextAdmin(admin.ModelAdmin):
    form = VisitNextAdminForm
    list_display = ['visit', 'page_visit', 'status', 'offset', 'history_length', 'created_at']
    #readonly_fields = ['created_at']

admin.site.register(VisitNext, VisitNextAdmin)


# class VisitTrackAdminForm(forms.ModelForm):

#     class Meta:
#         model = VisitTrack
#         fields = '__all__'


# class VisitTrackAdmin(admin.ModelAdmin):
#     form = VisitTrackAdminForm
#     list_display = ['visit_next', 'created_at']
#     #readonly_fields = ['created_at']

# admin.site.register(VisitTrack, VisitTrackAdmin)



class UnwantedBehaviourAdminForm(forms.ModelForm):

    class Meta:
        model = UnwantedBehaviour
        fields = '__all__'


class UnwantedBehaviourAdmin(admin.ModelAdmin):
    form = UnwantedBehaviourAdminForm
    list_display = ['json_details', 'created_at']
    #readonly_fields = ['json_details', 'created_at']

admin.site.register(UnwantedBehaviour, UnwantedBehaviourAdmin)


class SkillAdminForm(forms.ModelForm):

    class Meta:
        model = Skill
        fields = '__all__'


class SkillAdmin(admin.ModelAdmin):
    form = SkillAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(Skill, SkillAdmin)


class SkillCacheAdminForm(forms.ModelForm):

    class Meta:
        model = SkillCache
        fields = '__all__'


class SkillCacheAdmin(admin.ModelAdmin):
    form = SkillCacheAdminForm
    list_display = ['level', 'value']
    #readonly_fields = ['level', 'value']

admin.site.register(SkillCache, SkillCacheAdmin)


class TaskSkillAdminForm(forms.ModelForm):

    class Meta:
        model = TaskSkill
        fields = '__all__'


class TaskSkillAdmin(admin.ModelAdmin):
    form = TaskSkillAdminForm
    list_display = ['gives_experience']
    #readonly_fields = ['gives_experience']

admin.site.register(TaskSkill, TaskSkillAdmin)


class SkillHistoryAdminForm(forms.ModelForm):

    class Meta:
        model = SkillHistory
        fields = '__all__'


class SkillHistoryAdmin(admin.ModelAdmin):
    form = SkillHistoryAdminForm
    list_display = ['object_id', 'event_type', 'points', 'created_at']
    #readonly_fields = ['object_id', 'event_type', 'points', 'created_at']

admin.site.register(SkillHistory, SkillHistoryAdmin)


class CommentAdminForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = '__all__'


class CommentAdmin(admin.ModelAdmin):
    form = CommentAdminForm
    list_display = ['text', 'created_at']
    #readonly_fields = ['text', 'created_at']

admin.site.register(Comment, CommentAdmin)



class LoginTryAdminForm(forms.ModelForm):

    class Meta:
        model = LoginTry
        fields = '__all__'


class LoginTryAdmin(admin.ModelAdmin):
    form = LoginTryAdminForm
    list_display = ['login', 'user', 'is_successful', 'created_at']
    readonly_fields = ['login', 'user', 'is_successful', 'created_at']

admin.site.register(LoginTry, LoginTryAdmin)



class TokenAdminForm(forms.ModelForm):

    class Meta:
        model = Token
        fields = '__all__'


class TokenAdmin(admin.ModelAdmin):
    form = TokenAdminForm
    list_display = ['user', 'key', 'created_at', 'is_active']
    #readonly_fields = ['user', 'key', 'created_at', 'is_active']

admin.site.register(Token, TokenAdmin)


class FrontPageAdminForm(forms.ModelForm):

    class Meta:
        model = FrontPage
        fields = '__all__'


class FrontPageAdmin(admin.ModelAdmin):
    form = FrontPageAdminForm
    list_display = ['name']
    #readonly_fields = ['name']

admin.site.register(FrontPage, FrontPageAdmin)




class FrontPageVisitAdminForm(forms.ModelForm):

    class Meta:
        model = FrontPageVisit
        fields = '__all__'


class FrontPageVisitAdmin(admin.ModelAdmin):
    form = FrontPageVisitAdminForm
    list_display = ['page', 'page_pk']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(FrontPageVisit, FrontPageVisitAdmin)




class FrontPageElementAdminForm(forms.ModelForm):

    class Meta:
        model = FrontPageElement
        fields = '__all__'


class FrontPageElementAdmin(admin.ModelAdmin):
    form = FrontPageElementAdminForm
    list_display = ['page', 'element_id', 'desc']
    #readonly_fields = ['page', 'element_id', 'desc']

admin.site.register(FrontPageElement, FrontPageElementAdmin)



class VisitScrollAdminForm(forms.ModelForm):

    class Meta:
        model = VisitScroll
        fields = '__all__'


class VisitScrollAdmin(admin.ModelAdmin):
    form = VisitScrollAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitScroll, VisitScrollAdmin)


class VisitResizeAdminForm(forms.ModelForm):

    class Meta:
        model = VisitResize
        fields = '__all__'


class VisitResizeAdmin(admin.ModelAdmin):
    form = VisitResizeAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitResize, VisitResizeAdmin)


class VisitMonitorChangeAdminForm(forms.ModelForm):

    class Meta:
        model = VisitMonitorChange
        fields = '__all__'


class VisitMonitorChangeAdmin(admin.ModelAdmin):
    form = VisitMonitorChangeAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitMonitorChange, VisitMonitorChangeAdmin)


class VisitKeyPressAdminForm(forms.ModelForm):

    class Meta:
        model = VisitKeyPress
        fields = '__all__'


class VisitKeyPressAdmin(admin.ModelAdmin):
    form = VisitKeyPressAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitKeyPress, VisitKeyPressAdmin)


class VisitActiveAdminForm(forms.ModelForm):

    class Meta:
        model = VisitActive
        fields = '__all__'


class VisitActiveAdmin(admin.ModelAdmin):
    form = VisitActiveAdminForm
    list_display = ['id', 'is_active', 'event_type', 'time_shift', 'track_number']
    list_filter = ['is_active', 'event_type']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitActive, VisitActiveAdmin)


class VisitMouseMoveAdminForm(forms.ModelForm):

    class Meta:
        model = VisitMouseMove
        fields = '__all__'


class VisitMouseMoveAdmin(admin.ModelAdmin):
    form = VisitMouseMoveAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitMouseMove, VisitMouseMoveAdmin)


class VisitBufferAdminForm(forms.ModelForm):

    class Meta:
        model = VisitBuffer
        fields = '__all__'


class VisitBufferAdmin(admin.ModelAdmin):
    form = VisitBufferAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitBuffer, VisitBufferAdmin)


class VisitSelectionAdminForm(forms.ModelForm):

    class Meta:
        model = VisitSelection
        fields = '__all__'


class VisitSelectionAdmin(admin.ModelAdmin):
    form = VisitSelectionAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitSelection, VisitSelectionAdmin)


class VisitEditorSelectionAdminForm(forms.ModelForm):

    class Meta:
        model = VisitEditorSelection
        fields = '__all__'


class VisitEditorSelectionAdmin(admin.ModelAdmin):
    form = VisitEditorSelectionAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitEditorSelection, VisitEditorSelectionAdmin)


class VisitClickAdminForm(forms.ModelForm):

    class Meta:
        model = VisitClick
        fields = '__all__'


class VisitClickAdmin(admin.ModelAdmin):
    form = VisitClickAdminForm
    list_display = ['id', 'time_shift', 'track_number', 'button_type', 'is_down']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitClick, VisitClickAdmin)


class VisitElementClickAdminForm(forms.ModelForm):

    class Meta:
        model = VisitElementClick
        fields = '__all__'


class VisitElementClickAdmin(admin.ModelAdmin):
    form = VisitElementClickAdminForm
    list_display = ['id', 'time_shift', 'track_number']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(VisitElementClick, VisitElementClickAdmin)



class UseStatAdminForm(forms.ModelForm):

    class Meta:
        model = UseStat
        fields = '__all__'


class UseStatAdmin(admin.ModelAdmin):
    form = UseStatAdminForm
    list_display = ['id', 'user', 'stat_type']
    #readonly_fields = ['page', 'page_pk']

admin.site.register(UseStat, UseStatAdmin)
