"""django_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from main import views as main_views
from user_app import views as user_app_views
from learn import views as learn_views


urlpatterns = [
    url(r'^$', user_app_views.dashboard, name='dashboard'),
    url(r'^profile/$', user_app_views.profile, name='profile'),
    url(r'^profile_edit/$', user_app_views.profile_edit, name='profile_edit'),
    
    url(r'^courses/$', learn_views.courses_list, name='courses_list'),
    url(r'^courses/(?P<course_id>\d+)$', learn_views.course_page, name='course_page'),
    url(r'^courses/(?P<course_id>\d+)/start$', learn_views.start_course, name='start_course'),
    
    url(r'^lessons/(?P<lesson_id>\d+)$', learn_views.lesson_page, name='lesson_page'),
    
    url(r'^tasks/(?P<task_id>\d+)/start$', learn_views.start_task, name='start_task'),
    url(r'^tasks/(?P<task_execution_id>\d+)$', learn_views.task_execute, name='task_execute'),
    url(r'^tasks/(?P<task_execution_id>\d+)/check$', learn_views.task_check, name='task_check'),

    url(r'^main/', include('main.urls')),
    url(r'^user_app/', include('user_app.urls')),
    url(r'^knowledge/', include('knowledge.urls')),
    url(r'^learn/', include('learn.urls')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^my_admin/', admin.site.urls),
    # TODO: honeypot
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls))
    ]