from django.conf.urls import url, include
from rest_framework import routers
from knowledge import api
from knowledge import views

router = routers.DefaultRouter()
router.register(r'graphnodetype', api.GraphNodeTypeViewSet)
router.register(r'graphnode', api.GraphNodeViewSet)
router.register(r'graphlinktype', api.GraphLinkTypeViewSet)
router.register(r'graphpropertykey', api.GraphPropertyKeyViewSet)
router.register(r'graphpropertyvalue', api.GraphPropertyValueViewSet)
router.register(r'graphnodeproperty', api.GraphNodePropertyViewSet)
router.register(r'graphlink', api.GraphLinkViewSet)
router.register(r'graphlinkproperty', api.GraphLinkPropertyViewSet)
router.register(r'basecategory', api.BaseCategoryViewSet)
router.register(r'basepost', api.BasePostViewSet)
router.register(r'basepostblock', api.BasePostBlockViewSet)
router.register(r'basepostlinks', api.BasePostLinksViewSet)
router.register(r'basegraphlinks', api.BaseGraphLinksViewSet)
router.register(r'userlink', api.UserLinkViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    url(r'^api/v1/', include(router.urls)),
)
