from knowledge import models

from rest_framework import serializers


class GraphNodeTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphNodeType
        fields = (
            'pk', 
            'title', 
        )


class GraphNodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphNode
        fields = (
            'pk', 
            'title', 
            'created_at', 
        )


class GraphLinkTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphLinkType
        fields = (
            'pk', 
            'title', 
        )


class GraphPropertyKeySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphPropertyKey
        fields = (
            'pk', 
            'value', 
        )


class GraphPropertyValueSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphPropertyValue
        fields = (
            'pk', 
            'value', 
        )


class GraphNodePropertySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphNodeProperty
        fields = (
            'pk', 
            'created_at', 
        )


class GraphLinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphLink
        fields = (
            'pk', 
            'created_at', 
        )


class GraphLinkPropertySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.GraphLinkProperty
        fields = (
            'pk', 
            'created_at', 
        )


class BaseCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.BaseCategory
        fields = (
            'pk', 
            'title', 
            'created_at', 
        )


class BasePostSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.BasePost
        fields = (
            'pk', 
            'title', 
            'short_info', 
            'created_at', 
        )


class BasePostBlockSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.BasePostBlock
        fields = (
            'pk', 
            'block_type', 
            'html', 
        )


class BasePostLinksSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.BasePostLinks
        fields = (
            'pk', 
            'created_at', 
        )


class BaseGraphLinksSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.BaseGraphLinks
        fields = (
            'pk', 
            'created_at', 
        )


class UserLinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.UserLink
        fields = (
            'pk', 
            'created_at', 
        )


