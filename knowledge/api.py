from knowledge import models
from knowledge import serializers
from rest_framework import viewsets, permissions


class GraphNodeTypeViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphNodeType class"""

    queryset = models.GraphNodeType.objects.all()
    serializer_class = serializers.GraphNodeTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class GraphNodeViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphNode class"""

    queryset = models.GraphNode.objects.all()
    serializer_class = serializers.GraphNodeSerializer
    permission_classes = [permissions.IsAuthenticated]


class GraphLinkTypeViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphLinkType class"""

    queryset = models.GraphLinkType.objects.all()
    serializer_class = serializers.GraphLinkTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class GraphPropertyKeyViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphPropertyKey class"""

    queryset = models.GraphPropertyKey.objects.all()
    serializer_class = serializers.GraphPropertyKeySerializer
    permission_classes = [permissions.IsAuthenticated]


class GraphPropertyValueViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphPropertyValue class"""

    queryset = models.GraphPropertyValue.objects.all()
    serializer_class = serializers.GraphPropertyValueSerializer
    permission_classes = [permissions.IsAuthenticated]


class GraphNodePropertyViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphNodeProperty class"""

    queryset = models.GraphNodeProperty.objects.all()
    serializer_class = serializers.GraphNodePropertySerializer
    permission_classes = [permissions.IsAuthenticated]


class GraphLinkViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphLink class"""

    queryset = models.GraphLink.objects.all()
    serializer_class = serializers.GraphLinkSerializer
    permission_classes = [permissions.IsAuthenticated]


class GraphLinkPropertyViewSet(viewsets.ModelViewSet):
    """ViewSet for the GraphLinkProperty class"""

    queryset = models.GraphLinkProperty.objects.all()
    serializer_class = serializers.GraphLinkPropertySerializer
    permission_classes = [permissions.IsAuthenticated]


class BaseCategoryViewSet(viewsets.ModelViewSet):
    """ViewSet for the BaseCategory class"""

    queryset = models.BaseCategory.objects.all()
    serializer_class = serializers.BaseCategorySerializer
    permission_classes = [permissions.IsAuthenticated]


class BasePostViewSet(viewsets.ModelViewSet):
    """ViewSet for the BasePost class"""

    queryset = models.BasePost.objects.all()
    serializer_class = serializers.BasePostSerializer
    permission_classes = [permissions.IsAuthenticated]


class BasePostBlockViewSet(viewsets.ModelViewSet):
    """ViewSet for the BasePostBlock class"""

    queryset = models.BasePostBlock.objects.all()
    serializer_class = serializers.BasePostBlockSerializer
    permission_classes = [permissions.IsAuthenticated]


class BasePostLinksViewSet(viewsets.ModelViewSet):
    """ViewSet for the BasePostLinks class"""

    queryset = models.BasePostLinks.objects.all()
    serializer_class = serializers.BasePostLinksSerializer
    permission_classes = [permissions.IsAuthenticated]


class BaseGraphLinksViewSet(viewsets.ModelViewSet):
    """ViewSet for the BaseGraphLinks class"""

    queryset = models.BaseGraphLinks.objects.all()
    serializer_class = serializers.BaseGraphLinksSerializer
    permission_classes = [permissions.IsAuthenticated]


class UserLinkViewSet(viewsets.ModelViewSet):
    """ViewSet for the UserLink class"""

    queryset = models.UserLink.objects.all()
    serializer_class = serializers.UserLinkSerializer
    permission_classes = [permissions.IsAuthenticated]


