from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db.models import *
from django.db import models as models
from django.contrib.auth.models import User



class GraphNodeType(models.Model):
    title = CharField("Название", max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тип нода'
        verbose_name_plural = 'Типы нодов'



class GraphNode(models.Model):
    node_type = ForeignKey('knowledge.GraphNodeType', verbose_name="Тип нода")

    title = CharField("Название", max_length=50)
    created_at = DateTimeField("Создан", auto_now_add=True)


    def __str__(self):
        return self.title


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Нод'
        verbose_name_plural = 'Ноды'




class GraphLinkType(models.Model):
    title = CharField("Название", max_length=255)


    def __str__(self):
        return self.title


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Тип связи'
        verbose_name_plural = 'Типы связей'



class GraphPropertyKey(models.Model):
    value = CharField("Значение", max_length=255)


    def __str__(self):
        return self.value


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Ключ свойства'
        verbose_name_plural = 'Ключи свойств'



class GraphPropertyValue(models.Model):
    value = TextField("Значение")


    def __str__(self):
        return self.value


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Значение свойства'
        verbose_name_plural = 'Значения свойств'



class GraphNodeProperty(models.Model):
    node = ForeignKey(GraphNode, verbose_name="Нод", related_name='properties')
    prop_key = ForeignKey(GraphPropertyKey, verbose_name="Ключ")
    prop_obj = ForeignKey(GraphNode, verbose_name="Объект значения", related_name='as_properties', blank=True, null=True)
    prop_val = ForeignKey(GraphPropertyValue, verbose_name="Значение", blank=True, null=True)
    created_at = DateTimeField("Создан", auto_now_add=True)


    def __str__(self):
        if self.prop_obj:
            return "{} {}".format(self.prop_key, self.prop_obj)
        return "{} {}".format(self.prop_key, self.prop_val)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Свойство нода'
        verbose_name_plural = 'Свойства нодов'





class GraphLink(models.Model):
    from_node = ForeignKey(GraphNode, verbose_name="От нода", related_name='from_links')
    link_type = ForeignKey(GraphLinkType, verbose_name="Тип связи")
    to_node = ForeignKey(GraphNode, verbose_name="К ноду", related_name='to_links')
    created_at = DateTimeField("Создан", auto_now_add=True)


    def __str__(self):
        return "{} {} {}".format(self.from_node, self.link_type, self.to_node)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Связь нодов'
        verbose_name_plural = 'Связи нодов'



class GraphLinkProperty(models.Model):
    link = ForeignKey(GraphLink, verbose_name="Связь")
    prop_key = ForeignKey(GraphPropertyKey, verbose_name="Ключ")
    prop_obj = ForeignKey(GraphNode, verbose_name="Объект значения")
    prop_val = ForeignKey(GraphPropertyValue, verbose_name="Значение")
    created_at = DateTimeField("Создан", auto_now_add=True)


    def __str__(self):
        if self.prop_obj:
            return "{} {}".format(self.prop_key, self.prop_obj)
        return "{} {}".format(self.prop_key, self.prop_val)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Свойство связи'
        verbose_name_plural = 'Свойства связей'



class BaseCategory(models.Model):
    parent = ForeignKey('self', verbose_name="Родитель", blank=True, null=True)
    node = ForeignKey(GraphNode, verbose_name="Нод")
    title = CharField("Название", max_length=50)
    created_at = DateTimeField("Создан", auto_now_add=True)


    def __str__(self):
        return self.title


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Категория поста'
        verbose_name_plural = 'Категории постов'



class BasePost(models.Model):
    node = ForeignKey(GraphNode, verbose_name="Нод")
    title = CharField("Название", max_length=50)
    short_info = TextField("Краткое описание")
    created_at = DateTimeField("Создан", auto_now_add=True)


    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'



class BasePostBlock(models.Model):
    BLOCK_TYPES = (
        ('HTML', 'HTML'),
        ('PYTHON', 'Код Python'),
        ('CPP', 'Код C++'),
        ('CSHARP', 'Код C#'),
    )
    post = ForeignKey(BasePost, verbose_name="Статья")
    node = ForeignKey(GraphNode, verbose_name="Нод")
    block_type = CharField("Тип", max_length=20, choices=BLOCK_TYPES)
    html = TextField("Содержание")


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Блок поста'
        verbose_name_plural = 'Блоки постов'



class BasePostLinks(models.Model):
    from_post = ForeignKey(BasePost, verbose_name="От статьи", related_name='from_links')
    from_block = ForeignKey(BasePostBlock, verbose_name="От блока", related_name='from_links')

    to_post = ForeignKey(BasePost, verbose_name="К статье", related_name='to_links')
    to_block = ForeignKey(BasePostBlock, verbose_name="К блоку", related_name='to_links')
    
    created_at = DateTimeField("Создан", auto_now_add=True)



    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Связь постов'
        verbose_name_plural = 'Связи постов'



class BaseGraphLinks(models.Model):
    from_post = ForeignKey(BasePost, verbose_name="От статьи")
    block = ForeignKey(BasePostBlock, verbose_name="Блок")
    node = ForeignKey(GraphNode, verbose_name="Нод")
    created_at = DateTimeField("Создан", auto_now_add=True)



    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Привязка блока к ноду'
        verbose_name_plural = 'Привязки блоков к нодам'



class UserLink(models.Model):
    user = ForeignKey(User, verbose_name="Пользователь", related_name="links_to_nodes")
    node = ForeignKey(GraphNode, verbose_name="Нод")
    link_type = ForeignKey(GraphLinkType, verbose_name="Тип связи")
    created_at = DateTimeField("Создан", auto_now_add=True)



    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Связь пользователя с нодом'
        verbose_name_plural = 'Связи пользователей с нодами'

