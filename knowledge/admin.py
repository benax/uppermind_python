from django.contrib import admin
from django import forms
from .models import GraphNodeType, GraphNode, GraphLinkType, GraphPropertyKey, GraphPropertyValue, GraphNodeProperty, GraphLink, GraphLinkProperty, BaseCategory, BasePost, BasePostBlock, BasePostLinks, BaseGraphLinks, UserLink

class PropertyValueAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PropertyValueAdminForm, self).__init__(*args, **kwargs)
        self.fields['value'].widget = admin.widgets.AdminTextInputWidget()



class GraphNodeTypeAdminForm(forms.ModelForm):

    class Meta:
        model = GraphNodeType
        fields = '__all__'


class GraphNodeTypeAdmin(admin.ModelAdmin):
    form = GraphNodeTypeAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(GraphNodeType, GraphNodeTypeAdmin)





class PropertyNodeInline(admin.TabularInline):
    model = GraphNodeProperty
    fk_name = 'node'
    extra = 1
    readonly_fields = ('created_at', )
    exclude = ('link', )

class LinkFromInline(admin.TabularInline):
    model = GraphLink
    fk_name = 'from_node'
    extra = 1
    readonly_fields = ('created_at', )
    fields = ('link_type', 'to_node')

class LinkToInline(admin.TabularInline):
    model = GraphLink
    fk_name = 'to_node'
    extra = 1
    readonly_fields = ('created_at', )
    fields = ('from_node', 'link_type')



class GraphNodeAdminForm(forms.ModelForm):

    class Meta:
        model = GraphNode
        fields = '__all__'


class GraphNodeAdmin(admin.ModelAdmin):
    form = GraphNodeAdminForm
    inlines = [
        PropertyNodeInline,
        LinkFromInline,
        LinkToInline,
    ]
    list_display = ['title', 'created_at']
    #readonly_fields = ['title', 'created_at']

admin.site.register(GraphNode, GraphNodeAdmin)


class GraphLinkTypeAdminForm(forms.ModelForm):

    class Meta:
        model = GraphLinkType
        fields = '__all__'


class GraphLinkTypeAdmin(admin.ModelAdmin):
    form = GraphLinkTypeAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(GraphLinkType, GraphLinkTypeAdmin)


class GraphPropertyKeyAdminForm(forms.ModelForm):

    class Meta:
        model = GraphPropertyKey
        fields = '__all__'


class GraphPropertyKeyAdmin(admin.ModelAdmin):
    form = GraphPropertyKeyAdminForm
    list_display = ['value']
    #readonly_fields = ['value']

admin.site.register(GraphPropertyKey, GraphPropertyKeyAdmin)


class GraphPropertyValueAdmin(admin.ModelAdmin):
    form = PropertyValueAdminForm
    list_display = ['value']
    #readonly_fields = ['value']

admin.site.register(GraphPropertyValue, GraphPropertyValueAdmin)


class GraphNodePropertyAdminForm(forms.ModelForm):

    class Meta:
        model = GraphNodeProperty
        fields = '__all__'


class GraphNodePropertyAdmin(admin.ModelAdmin):
    form = GraphNodePropertyAdminForm
    list_display = ['created_at']
    #readonly_fields = ['created_at']

admin.site.register(GraphNodeProperty, GraphNodePropertyAdmin)


class GraphLinkAdminForm(forms.ModelForm):

    class Meta:
        model = GraphLink
        fields = '__all__'


class GraphLinkAdmin(admin.ModelAdmin):
    form = GraphLinkAdminForm
    list_display = ['id', 'from_node', 'link_type', 'to_node', 'created_at']
    #readonly_fields = ['created_at']

admin.site.register(GraphLink, GraphLinkAdmin)


class GraphLinkPropertyAdminForm(forms.ModelForm):

    class Meta:
        model = GraphLinkProperty
        fields = '__all__'


class GraphLinkPropertyAdmin(admin.ModelAdmin):
    form = GraphLinkPropertyAdminForm
    list_display = ['created_at']
    #readonly_fields = ['created_at']

admin.site.register(GraphLinkProperty, GraphLinkPropertyAdmin)


class BaseCategoryAdminForm(forms.ModelForm):

    class Meta:
        model = BaseCategory
        fields = '__all__'


class BaseCategoryAdmin(admin.ModelAdmin):
    form = BaseCategoryAdminForm
    list_display = ['title', 'created_at']
    #readonly_fields = ['title', 'created_at']

admin.site.register(BaseCategory, BaseCategoryAdmin)


class BasePostAdminForm(forms.ModelForm):

    class Meta:
        model = BasePost
        fields = '__all__'


class BasePostAdmin(admin.ModelAdmin):
    form = BasePostAdminForm
    list_display = ['title', 'short_info', 'created_at']
    #readonly_fields = ['title', 'short_info', 'created_at']

admin.site.register(BasePost, BasePostAdmin)


class BasePostBlockAdminForm(forms.ModelForm):

    class Meta:
        model = BasePostBlock
        fields = '__all__'


class BasePostBlockAdmin(admin.ModelAdmin):
    form = BasePostBlockAdminForm
    list_display = ['block_type', 'html']
    #readonly_fields = ['block_type', 'html']

admin.site.register(BasePostBlock, BasePostBlockAdmin)


class BasePostLinksAdminForm(forms.ModelForm):

    class Meta:
        model = BasePostLinks
        fields = '__all__'


class BasePostLinksAdmin(admin.ModelAdmin):
    form = BasePostLinksAdminForm
    list_display = ['created_at']
    #readonly_fields = ['created_at']

admin.site.register(BasePostLinks, BasePostLinksAdmin)


class BaseGraphLinksAdminForm(forms.ModelForm):

    class Meta:
        model = BaseGraphLinks
        fields = '__all__'


class BaseGraphLinksAdmin(admin.ModelAdmin):
    form = BaseGraphLinksAdminForm
    list_display = ['created_at']
    #readonly_fields = ['created_at']

admin.site.register(BaseGraphLinks, BaseGraphLinksAdmin)


class UserLinkAdminForm(forms.ModelForm):

    class Meta:
        model = UserLink
        fields = '__all__'


class UserLinkAdmin(admin.ModelAdmin):
    form = UserLinkAdminForm
    list_display = ['created_at']
    #readonly_fields = ['created_at']

admin.site.register(UserLink, UserLinkAdmin)


