import unittest
from django.core.urlresolvers import reverse
from django.test import Client
from .models import GraphNodeType, GraphNode, GraphLinkType, GraphPropertyKey, GraphPropertyValue, GraphNodeProperty, GraphLink, GraphLinkProperty, BaseCategory, BasePost, BasePostBlock, BasePostLinks, BaseGraphLinks, UserLink
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_graphnodetype(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    return GraphNodeType.objects.create(**defaults)


def create_graphnode(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    if "node_type" not in defaults:
        defaults["node_type"] = create_'knowledge_graphnodetype'()
    return GraphNode.objects.create(**defaults)


def create_graphlinktype(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    return GraphLinkType.objects.create(**defaults)


def create_graphpropertykey(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return GraphPropertyKey.objects.create(**defaults)


def create_graphpropertyvalue(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return GraphPropertyValue.objects.create(**defaults)


def create_graphnodeproperty(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "node" not in defaults:
        defaults["node"] = create_graphnode()
    if "prop_key" not in defaults:
        defaults["prop_key"] = create_graphpropertykey()
    if "prop_obj" not in defaults:
        defaults["prop_obj"] = create_graphnode()
    if "prop_val" not in defaults:
        defaults["prop_val"] = create_graphpropertyvalue()
    return GraphNodeProperty.objects.create(**defaults)


def create_graphlink(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "from_node" not in defaults:
        defaults["from_node"] = create_graphnode()
    if "to_node" not in defaults:
        defaults["to_node"] = create_graphnode()
    if "link_type" not in defaults:
        defaults["link_type"] = create_graphlinktype()
    return GraphLink.objects.create(**defaults)


def create_graphlinkproperty(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "link" not in defaults:
        defaults["link"] = create_graphlink()
    if "prop_key" not in defaults:
        defaults["prop_key"] = create_graphpropertykey()
    if "prop_obj" not in defaults:
        defaults["prop_obj"] = create_graphnode()
    if "prop_val" not in defaults:
        defaults["prop_val"] = create_graphpropertyvalue()
    return GraphLinkProperty.objects.create(**defaults)


def create_basecategory(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    if "parent" not in defaults:
        defaults["parent"] = create_'self'()
    if "node" not in defaults:
        defaults["node"] = create_graphnode()
    return BaseCategory.objects.create(**defaults)


def create_basepost(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults["short_info"] = "short_info"
    defaults.update(**kwargs)
    if "node" not in defaults:
        defaults["node"] = create_graphnode()
    return BasePost.objects.create(**defaults)


def create_basepostblock(**kwargs):
    defaults = {}
    defaults["block_type"] = "block_type"
    defaults["html"] = "html"
    defaults.update(**kwargs)
    if "post" not in defaults:
        defaults["post"] = create_basepost()
    if "node" not in defaults:
        defaults["node"] = create_graphnode()
    return BasePostBlock.objects.create(**defaults)


def create_basepostlinks(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "from_post" not in defaults:
        defaults["from_post"] = create_basepost()
    if "from_block" not in defaults:
        defaults["from_block"] = create_basepostblock()
    if "to_post" not in defaults:
        defaults["to_post"] = create_basepost()
    if "to_block" not in defaults:
        defaults["to_block"] = create_basepostblock()
    return BasePostLinks.objects.create(**defaults)


def create_basegraphlinks(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "from_post" not in defaults:
        defaults["from_post"] = create_basepost()
    if "block" not in defaults:
        defaults["block"] = create_basepostblock()
    if "node" not in defaults:
        defaults["node"] = create_graphnode()
    return BaseGraphLinks.objects.create(**defaults)


def create_userlink(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "user" not in defaults:
        defaults["user"] = create_user()
    if "node" not in defaults:
        defaults["node"] = create_graphnode()
    if "link_type" not in defaults:
        defaults["link_type"] = create_graphlinktype()
    return UserLink.objects.create(**defaults)


class GraphNodeTypeViewTest(unittest.TestCase):
    '''
    Tests for GraphNodeType
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphnodetype(self):
        url = reverse('knowledge_graphnodetype_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphnodetype(self):
        url = reverse('knowledge_graphnodetype_create')
        data = {
            "title": "title",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphnodetype(self):
        graphnodetype = create_graphnodetype()
        url = reverse('knowledge_graphnodetype_detail', args=[graphnodetype.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphnodetype(self):
        graphnodetype = create_graphnodetype()
        data = {
            "title": "title",
        }
        url = reverse('knowledge_graphnodetype_update', args=[graphnodetype.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GraphNodeViewTest(unittest.TestCase):
    '''
    Tests for GraphNode
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphnode(self):
        url = reverse('knowledge_graphnode_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphnode(self):
        url = reverse('knowledge_graphnode_create')
        data = {
            "title": "title",
            "node_type": create_'knowledge_graphnodetype'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphnode(self):
        graphnode = create_graphnode()
        url = reverse('knowledge_graphnode_detail', args=[graphnode.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphnode(self):
        graphnode = create_graphnode()
        data = {
            "title": "title",
            "node_type": create_'knowledge_graphnodetype'().pk,
        }
        url = reverse('knowledge_graphnode_update', args=[graphnode.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GraphLinkTypeViewTest(unittest.TestCase):
    '''
    Tests for GraphLinkType
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphlinktype(self):
        url = reverse('knowledge_graphlinktype_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphlinktype(self):
        url = reverse('knowledge_graphlinktype_create')
        data = {
            "title": "title",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphlinktype(self):
        graphlinktype = create_graphlinktype()
        url = reverse('knowledge_graphlinktype_detail', args=[graphlinktype.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphlinktype(self):
        graphlinktype = create_graphlinktype()
        data = {
            "title": "title",
        }
        url = reverse('knowledge_graphlinktype_update', args=[graphlinktype.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GraphPropertyKeyViewTest(unittest.TestCase):
    '''
    Tests for GraphPropertyKey
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphpropertykey(self):
        url = reverse('knowledge_graphpropertykey_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphpropertykey(self):
        url = reverse('knowledge_graphpropertykey_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphpropertykey(self):
        graphpropertykey = create_graphpropertykey()
        url = reverse('knowledge_graphpropertykey_detail', args=[graphpropertykey.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphpropertykey(self):
        graphpropertykey = create_graphpropertykey()
        data = {
            "value": "value",
        }
        url = reverse('knowledge_graphpropertykey_update', args=[graphpropertykey.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GraphPropertyValueViewTest(unittest.TestCase):
    '''
    Tests for GraphPropertyValue
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphpropertyvalue(self):
        url = reverse('knowledge_graphpropertyvalue_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphpropertyvalue(self):
        url = reverse('knowledge_graphpropertyvalue_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphpropertyvalue(self):
        graphpropertyvalue = create_graphpropertyvalue()
        url = reverse('knowledge_graphpropertyvalue_detail', args=[graphpropertyvalue.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphpropertyvalue(self):
        graphpropertyvalue = create_graphpropertyvalue()
        data = {
            "value": "value",
        }
        url = reverse('knowledge_graphpropertyvalue_update', args=[graphpropertyvalue.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GraphNodePropertyViewTest(unittest.TestCase):
    '''
    Tests for GraphNodeProperty
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphnodeproperty(self):
        url = reverse('knowledge_graphnodeproperty_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphnodeproperty(self):
        url = reverse('knowledge_graphnodeproperty_create')
        data = {
            "node": create_graphnode().pk,
            "prop_key": create_graphpropertykey().pk,
            "prop_obj": create_graphnode().pk,
            "prop_val": create_graphpropertyvalue().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphnodeproperty(self):
        graphnodeproperty = create_graphnodeproperty()
        url = reverse('knowledge_graphnodeproperty_detail', args=[graphnodeproperty.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphnodeproperty(self):
        graphnodeproperty = create_graphnodeproperty()
        data = {
            "node": create_graphnode().pk,
            "prop_key": create_graphpropertykey().pk,
            "prop_obj": create_graphnode().pk,
            "prop_val": create_graphpropertyvalue().pk,
        }
        url = reverse('knowledge_graphnodeproperty_update', args=[graphnodeproperty.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GraphLinkViewTest(unittest.TestCase):
    '''
    Tests for GraphLink
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphlink(self):
        url = reverse('knowledge_graphlink_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphlink(self):
        url = reverse('knowledge_graphlink_create')
        data = {
            "from_node": create_graphnode().pk,
            "to_node": create_graphnode().pk,
            "link_type": create_graphlinktype().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphlink(self):
        graphlink = create_graphlink()
        url = reverse('knowledge_graphlink_detail', args=[graphlink.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphlink(self):
        graphlink = create_graphlink()
        data = {
            "from_node": create_graphnode().pk,
            "to_node": create_graphnode().pk,
            "link_type": create_graphlinktype().pk,
        }
        url = reverse('knowledge_graphlink_update', args=[graphlink.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GraphLinkPropertyViewTest(unittest.TestCase):
    '''
    Tests for GraphLinkProperty
    '''
    def setUp(self):
        self.client = Client()

    def test_list_graphlinkproperty(self):
        url = reverse('knowledge_graphlinkproperty_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_graphlinkproperty(self):
        url = reverse('knowledge_graphlinkproperty_create')
        data = {
            "link": create_graphlink().pk,
            "prop_key": create_graphpropertykey().pk,
            "prop_obj": create_graphnode().pk,
            "prop_val": create_graphpropertyvalue().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_graphlinkproperty(self):
        graphlinkproperty = create_graphlinkproperty()
        url = reverse('knowledge_graphlinkproperty_detail', args=[graphlinkproperty.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_graphlinkproperty(self):
        graphlinkproperty = create_graphlinkproperty()
        data = {
            "link": create_graphlink().pk,
            "prop_key": create_graphpropertykey().pk,
            "prop_obj": create_graphnode().pk,
            "prop_val": create_graphpropertyvalue().pk,
        }
        url = reverse('knowledge_graphlinkproperty_update', args=[graphlinkproperty.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class BaseCategoryViewTest(unittest.TestCase):
    '''
    Tests for BaseCategory
    '''
    def setUp(self):
        self.client = Client()

    def test_list_basecategory(self):
        url = reverse('knowledge_basecategory_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_basecategory(self):
        url = reverse('knowledge_basecategory_create')
        data = {
            "title": "title",
            "parent": create_'self'().pk,
            "node": create_graphnode().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_basecategory(self):
        basecategory = create_basecategory()
        url = reverse('knowledge_basecategory_detail', args=[basecategory.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_basecategory(self):
        basecategory = create_basecategory()
        data = {
            "title": "title",
            "parent": create_'self'().pk,
            "node": create_graphnode().pk,
        }
        url = reverse('knowledge_basecategory_update', args=[basecategory.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class BasePostViewTest(unittest.TestCase):
    '''
    Tests for BasePost
    '''
    def setUp(self):
        self.client = Client()

    def test_list_basepost(self):
        url = reverse('knowledge_basepost_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_basepost(self):
        url = reverse('knowledge_basepost_create')
        data = {
            "title": "title",
            "short_info": "short_info",
            "node": create_graphnode().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_basepost(self):
        basepost = create_basepost()
        url = reverse('knowledge_basepost_detail', args=[basepost.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_basepost(self):
        basepost = create_basepost()
        data = {
            "title": "title",
            "short_info": "short_info",
            "node": create_graphnode().pk,
        }
        url = reverse('knowledge_basepost_update', args=[basepost.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class BasePostBlockViewTest(unittest.TestCase):
    '''
    Tests for BasePostBlock
    '''
    def setUp(self):
        self.client = Client()

    def test_list_basepostblock(self):
        url = reverse('knowledge_basepostblock_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_basepostblock(self):
        url = reverse('knowledge_basepostblock_create')
        data = {
            "block_type": "block_type",
            "html": "html",
            "post": create_basepost().pk,
            "node": create_graphnode().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_basepostblock(self):
        basepostblock = create_basepostblock()
        url = reverse('knowledge_basepostblock_detail', args=[basepostblock.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_basepostblock(self):
        basepostblock = create_basepostblock()
        data = {
            "block_type": "block_type",
            "html": "html",
            "post": create_basepost().pk,
            "node": create_graphnode().pk,
        }
        url = reverse('knowledge_basepostblock_update', args=[basepostblock.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class BasePostLinksViewTest(unittest.TestCase):
    '''
    Tests for BasePostLinks
    '''
    def setUp(self):
        self.client = Client()

    def test_list_basepostlinks(self):
        url = reverse('knowledge_basepostlinks_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_basepostlinks(self):
        url = reverse('knowledge_basepostlinks_create')
        data = {
            "from_post": create_basepost().pk,
            "from_block": create_basepostblock().pk,
            "to_post": create_basepost().pk,
            "to_block": create_basepostblock().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_basepostlinks(self):
        basepostlinks = create_basepostlinks()
        url = reverse('knowledge_basepostlinks_detail', args=[basepostlinks.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_basepostlinks(self):
        basepostlinks = create_basepostlinks()
        data = {
            "from_post": create_basepost().pk,
            "from_block": create_basepostblock().pk,
            "to_post": create_basepost().pk,
            "to_block": create_basepostblock().pk,
        }
        url = reverse('knowledge_basepostlinks_update', args=[basepostlinks.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class BaseGraphLinksViewTest(unittest.TestCase):
    '''
    Tests for BaseGraphLinks
    '''
    def setUp(self):
        self.client = Client()

    def test_list_basegraphlinks(self):
        url = reverse('knowledge_basegraphlinks_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_basegraphlinks(self):
        url = reverse('knowledge_basegraphlinks_create')
        data = {
            "from_post": create_basepost().pk,
            "block": create_basepostblock().pk,
            "node": create_graphnode().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_basegraphlinks(self):
        basegraphlinks = create_basegraphlinks()
        url = reverse('knowledge_basegraphlinks_detail', args=[basegraphlinks.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_basegraphlinks(self):
        basegraphlinks = create_basegraphlinks()
        data = {
            "from_post": create_basepost().pk,
            "block": create_basepostblock().pk,
            "node": create_graphnode().pk,
        }
        url = reverse('knowledge_basegraphlinks_update', args=[basegraphlinks.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class UserLinkViewTest(unittest.TestCase):
    '''
    Tests for UserLink
    '''
    def setUp(self):
        self.client = Client()

    def test_list_userlink(self):
        url = reverse('knowledge_userlink_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_userlink(self):
        url = reverse('knowledge_userlink_create')
        data = {
            "user": create_user().pk,
            "node": create_graphnode().pk,
            "link_type": create_graphlinktype().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_userlink(self):
        userlink = create_userlink()
        url = reverse('knowledge_userlink_detail', args=[userlink.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_userlink(self):
        userlink = create_userlink()
        data = {
            "user": create_user().pk,
            "node": create_graphnode().pk,
            "link_type": create_graphlinktype().pk,
        }
        url = reverse('knowledge_userlink_update', args=[userlink.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


