from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import GraphNodeType, GraphNode, GraphLinkType, GraphPropertyKey, GraphPropertyValue, GraphNodeProperty, GraphLink, GraphLinkProperty, BaseCategory, BasePost, BasePostBlock, BasePostLinks, BaseGraphLinks, UserLink

