from django.shortcuts import render
from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import Version, SupportChat, SearchHistory


def home(request):
    return render(request, 'home.html')

