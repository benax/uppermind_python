from django.contrib import admin
from django import forms
from .models import Version, SupportChat, SearchHistory

class VersionAdminForm(forms.ModelForm):

    class Meta:
        model = Version
        fields = '__all__'


class VersionAdmin(admin.ModelAdmin):
    form = VersionAdminForm
    list_display = ['version', 'desc', 'private_desc', 'released_at']
    #readonly_fields = ['version', 'desc', 'private_desc', 'released_at']

admin.site.register(Version, VersionAdmin)


class SupportChatAdminForm(forms.ModelForm):

    class Meta:
        model = SupportChat
        fields = '__all__'


class SupportChatAdmin(admin.ModelAdmin):
    form = SupportChatAdminForm
    list_display = ['to_support', 'message', 'created_at']
    #readonly_fields = ['to_support', 'message', 'created_at']

admin.site.register(SupportChat, SupportChatAdmin)


class SearchHistoryAdminForm(forms.ModelForm):

    class Meta:
        model = SearchHistory
        fields = '__all__'


class SearchHistoryAdmin(admin.ModelAdmin):
    form = SearchHistoryAdminForm
    list_display = ['query', 'created_at']
    #readonly_fields = ['query', 'created_at']

admin.site.register(SearchHistory, SearchHistoryAdmin)


