from django.conf.urls import url, include
from rest_framework import routers
from main import api
from main import views

router = routers.DefaultRouter()
router.register(r'version', api.VersionViewSet)
router.register(r'supportchat', api.SupportChatViewSet)
router.register(r'searchhistory', api.SearchHistoryViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    url(r'^api/v1/', include(router.urls)),
)
