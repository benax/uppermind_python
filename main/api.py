from main import models
from main import serializers
from rest_framework import viewsets, permissions


class VersionViewSet(viewsets.ModelViewSet):
    """ViewSet for the Version class"""

    queryset = models.Version.objects.all()
    serializer_class = serializers.VersionSerializer
    permission_classes = [permissions.IsAuthenticated]


class SupportChatViewSet(viewsets.ModelViewSet):
    """ViewSet for the SupportChat class"""

    queryset = models.SupportChat.objects.all()
    serializer_class = serializers.SupportChatSerializer
    permission_classes = [permissions.IsAuthenticated]


class SearchHistoryViewSet(viewsets.ModelViewSet):
    """ViewSet for the SearchHistory class"""

    queryset = models.SearchHistory.objects.all()
    serializer_class = serializers.SearchHistorySerializer
    permission_classes = [permissions.IsAuthenticated]


