import unittest
from django.core.urlresolvers import reverse
from django.test import Client
from .models import Version, SupportChat, SearchHistory
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_version(**kwargs):
    defaults = {}
    defaults["version"] = "version"
    defaults["desc"] = "desc"
    defaults["private_desc"] = "private_desc"
    defaults["released_at"] = "released_at"
    defaults.update(**kwargs)
    return Version.objects.create(**defaults)


def create_supportchat(**kwargs):
    defaults = {}
    defaults["to_support"] = "to_support"
    defaults["message"] = "message"
    defaults.update(**kwargs)
    if "visitor" not in defaults:
        defaults["visitor"] = create_'user_app_visitor'()
    return SupportChat.objects.create(**defaults)


def create_searchhistory(**kwargs):
    defaults = {}
    defaults["query"] = "query"
    defaults.update(**kwargs)
    if "visitor" not in defaults:
        defaults["visitor"] = create_'user_app_visitor'()
    if "page" not in defaults:
        defaults["page"] = create_'user_app_visitpage'()
    return SearchHistory.objects.create(**defaults)


class VersionViewTest(unittest.TestCase):
    '''
    Tests for Version
    '''
    def setUp(self):
        self.client = Client()

    def test_list_version(self):
        url = reverse('main_version_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_version(self):
        url = reverse('main_version_create')
        data = {
            "version": "version",
            "desc": "desc",
            "private_desc": "private_desc",
            "released_at": "released_at",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_version(self):
        version = create_version()
        url = reverse('main_version_detail', args=[version.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_version(self):
        version = create_version()
        data = {
            "version": "version",
            "desc": "desc",
            "private_desc": "private_desc",
            "released_at": "released_at",
        }
        url = reverse('main_version_update', args=[version.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SupportChatViewTest(unittest.TestCase):
    '''
    Tests for SupportChat
    '''
    def setUp(self):
        self.client = Client()

    def test_list_supportchat(self):
        url = reverse('main_supportchat_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_supportchat(self):
        url = reverse('main_supportchat_create')
        data = {
            "to_support": "to_support",
            "message": "message",
            "visitor": create_'user_app_visitor'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_supportchat(self):
        supportchat = create_supportchat()
        url = reverse('main_supportchat_detail', args=[supportchat.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_supportchat(self):
        supportchat = create_supportchat()
        data = {
            "to_support": "to_support",
            "message": "message",
            "visitor": create_'user_app_visitor'().pk,
        }
        url = reverse('main_supportchat_update', args=[supportchat.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SearchHistoryViewTest(unittest.TestCase):
    '''
    Tests for SearchHistory
    '''
    def setUp(self):
        self.client = Client()

    def test_list_searchhistory(self):
        url = reverse('main_searchhistory_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_searchhistory(self):
        url = reverse('main_searchhistory_create')
        data = {
            "query": "query",
            "visitor": create_'user_app_visitor'().pk,
            "page": create_'user_app_visitpage'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_searchhistory(self):
        searchhistory = create_searchhistory()
        url = reverse('main_searchhistory_detail', args=[searchhistory.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_searchhistory(self):
        searchhistory = create_searchhistory()
        data = {
            "query": "query",
            "visitor": create_'user_app_visitor'().pk,
            "page": create_'user_app_visitpage'().pk,
        }
        url = reverse('main_searchhistory_update', args=[searchhistory.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


