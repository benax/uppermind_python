from django.core.urlresolvers import reverse
from django.db.models import *
from django.db import models as models



class Version(models.Model):
    version = CharField("Версия", max_length=10)
    private_desc = TextField("Закрытое описание", blank=True)
    desc = TextField("Описание")
    released_at = DateField("Релиз")

    def __str__(self):
        return self.version

    class Meta:
        verbose_name = 'Версия'
        verbose_name_plural = 'Версии'



class SupportChat(models.Model):
    visitor = ForeignKey('user_app.Visitor', verbose_name="Посетитель")
    
    to_support = BooleanField("Направлен в поддержку")
    message = TextField("Сообщение")
    created_at = DateTimeField("Добавлен", auto_now_add=True)


    # def __str__(self):
    #     return self.created_at

    class Meta:
        verbose_name = 'Сообщение в поддержку'
        verbose_name_plural = 'Сообщения в поддержку'



class SearchHistory(models.Model):
    visitor = ForeignKey('user_app.Visitor', verbose_name="Посетитель")
    page = ForeignKey('user_app.VisitPage', verbose_name="Страница")

    query = CharField("Запрос", max_length=255)
    created_at = DateTimeField("Добавлен", auto_now_add=True)
    

    def __str__(self):
        return self.query

    class Meta:
        verbose_name = 'Поисковой запрос'
        verbose_name_plural = 'Поисковые запросы'
