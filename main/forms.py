from django import forms
from .models import SupportChat, SearchHistory


class SupportChatForm(forms.ModelForm):
    class Meta:
        model = SupportChat
        fields = ['message']


class SearchHistoryForm(forms.ModelForm):
    class Meta:
        model = SearchHistory
        fields = ['query']


