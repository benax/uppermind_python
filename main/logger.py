from django.conf import settings
from datetime import datetime
import os


def save_log_file(log_dir, event_type, data_string):
    now = datetime.now()
    filename = '{:0>2}-{:0>2}-{:0>2}_{:0>2}.txt'.format(now.year, now.month, now.day, now.hour)
    with open(os.path.join(log_dir, filename), 'a', encoding="utf-8") as f:
        f.write('{:0>2}:{:0>2}:{:0>2}\t{}\t{}\n'.format(now.hour, now.minute, now.second, event_type, data_string))


def try_2(event_type, data_string):
    log_dir = os.path.join(settings.BASE_DIR, 'logs', '2')
    try:
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        save_log_file(log_dir, event_type, data_string)
    except:
        pass


def save_log(event_type, data_string):
    log_dir = os.path.join(settings.BASE_DIR, 'logs', '1')
    try:
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        save_log_file(log_dir, event_type, data_string)
    except:
        try_2(event_type, "(invalid data)")

