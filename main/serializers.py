from main import models

from rest_framework import serializers


class VersionSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Version
        fields = (
            'pk', 
            'version', 
            'desc', 
            'private_desc', 
            'released_at', 
        )


class SupportChatSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SupportChat
        fields = (
            'pk', 
            'to_support', 
            'message', 
            'created_at', 
        )


class SearchHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SearchHistory
        fields = (
            'pk', 
            'query', 
            'created_at', 
        )


