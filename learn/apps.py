from django.apps import AppConfig


class LearnConfig(AppConfig):
    name = 'learn'
    verbose_name = 'Обучение'
