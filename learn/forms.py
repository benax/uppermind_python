from django import forms
from .models import TaskExecution


class TaskExecutionForm(forms.ModelForm):
    class Meta:
        model = TaskExecution
        fields = ['answer']
