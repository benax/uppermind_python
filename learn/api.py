from learn import models
from learn import serializers
from rest_framework import viewsets, permissions


class SpeakerViewSet(viewsets.ModelViewSet):
    """ViewSet for the Speaker class"""

    queryset = models.Speaker.objects.all()
    serializer_class = serializers.SpeakerSerializer
    permission_classes = [permissions.IsAuthenticated]


class CourseCategoryViewSet(viewsets.ModelViewSet):
    """ViewSet for the CourseCategory class"""

    queryset = models.CourseCategory.objects.all()
    serializer_class = serializers.CourseCategorySerializer
    permission_classes = [permissions.IsAuthenticated]


class CourseViewSet(viewsets.ModelViewSet):
    """ViewSet for the Course class"""

    queryset = models.Course.objects.all()
    serializer_class = serializers.CourseSerializer
    permission_classes = [permissions.IsAuthenticated]


class CourseExecutionViewSet(viewsets.ModelViewSet):
    base_name = "CourseExecution"
    """ViewSet for the CourseExecution class"""

    queryset = models.CourseExecution.objects.filter(pk__lt=0)
    serializer_class = serializers.CourseExecutionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return models.CourseExecution.objects.filter(user=user)


class LessonViewSet(viewsets.ModelViewSet):
    """ViewSet for the Lesson class"""

    queryset = models.Lesson.objects.all()
    serializer_class = serializers.LessonSerializer
    permission_classes = [permissions.IsAuthenticated]


class LessonBlockViewSet(viewsets.ModelViewSet):
    """ViewSet for the LessonBlock class"""

    queryset = models.LessonBlock.objects.all()
    serializer_class = serializers.LessonBlockSerializer
    permission_classes = [permissions.IsAuthenticated]


class LessonVideoViewSet(viewsets.ModelViewSet):
    """ViewSet for the LessonVideo class"""

    queryset = models.LessonVideo.objects.all()
    serializer_class = serializers.LessonVideoSerializer
    permission_classes = [permissions.IsAuthenticated]


class LessonExecutionViewSet(viewsets.ModelViewSet):
    """ViewSet for the LessonExecution class"""

    queryset = models.LessonExecution.objects.filter(pk__lt=0)
    serializer_class = serializers.LessonExecutionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return models.LessonExecution.objects.filter(user=user)


class LessonQuestionViewSet(viewsets.ModelViewSet):
    """ViewSet for the LessonQuestion class"""

    queryset = models.LessonQuestion.objects.all()
    serializer_class = serializers.LessonQuestionSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProgrammingLanguageViewSet(viewsets.ModelViewSet):
    """ViewSet for the ProgrammingLanguage class"""

    queryset = models.ProgrammingLanguage.objects.all()
    serializer_class = serializers.ProgrammingLanguageSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskViewSet(viewsets.ModelViewSet):
    """ViewSet for the Task class"""

    queryset = models.Task.objects.all()
    serializer_class = serializers.TaskSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskQuestionViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskQuestion class"""

    queryset = models.TaskQuestion.objects.all()
    serializer_class = serializers.TaskQuestionSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskConditionViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskCondition class"""

    queryset = models.TaskCondition.objects.all()
    serializer_class = serializers.TaskConditionSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskOptionViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskOption class"""

    queryset = models.TaskOption.objects.all()
    serializer_class = serializers.TaskOptionSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskExecutionViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskExecution class"""

    queryset = models.TaskExecution.objects.filter(pk__lt=0)
    serializer_class = serializers.TaskExecutionSerializer
    permission_classes = [permissions.IsAuthenticated]
    
    def get_queryset(self):
        user = self.request.user
        return models.TaskExecution.objects.filter(user=user)


class TaskConditionHistoryViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskConditionHistory class"""

    queryset = models.TaskConditionHistory.objects.all()
    serializer_class = serializers.TaskConditionHistorySerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskCodePairViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskCodePair class"""

    queryset = models.TaskCodePair.objects.all()
    serializer_class = serializers.TaskCodePairSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskCodeCheckDetailViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskCodeCheckDetail class"""

    queryset = models.TaskCodeCheckDetail.objects.all()
    serializer_class = serializers.TaskCodeCheckDetailSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskNoteViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskNote class"""

    queryset = models.TaskNote.objects.all()
    serializer_class = serializers.TaskNoteSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskCodeManualCheckNoteViewSet(viewsets.ModelViewSet):
    """ViewSet for the TaskCodeManualCheckNote class"""

    queryset = models.TaskCodeManualCheckNote.objects.all()
    serializer_class = serializers.TaskCodeManualCheckNoteSerializer
    permission_classes = [permissions.IsAuthenticated]


