from django.contrib import admin
from django import forms
from learn.models import Speaker, CourseCategory, Course, CourseExecution, Lesson, LessonBlock, LessonVideo, LessonExecution, LessonQuestion, ProgrammingLanguage, Task, TaskQuestion, TaskCondition, TaskOption, TaskExecution, TaskConditionHistory, TaskCodePair, TaskCodeCheckDetail, TaskNote, TaskCodeManualCheckNote, ManualDifficultySelect, ReferenceCode, ReferenceCodeView, CompilerNote, CompilerCheckNote
from user_app.models import TaskSkill


class SpeakerAdminForm(forms.ModelForm):

    class Meta:
        model = Speaker
        fields = '__all__'


class SpeakerAdmin(admin.ModelAdmin):
    form = SpeakerAdminForm
    list_display = ['id', 'image', 'last_name', 'name', 'surname', 'sex', 'work_from', 'work_to']
    #readonly_fields = ['image', 'square_image', 'last_name', 'name', 'surname', 'sex', 'work_from', 'work_to']

admin.site.register(Speaker, SpeakerAdmin)


class CourseCategoryAdminForm(forms.ModelForm):

    class Meta:
        model = CourseCategory
        fields = '__all__'


class CourseCategoryAdmin(admin.ModelAdmin):
    form = CourseCategoryAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(CourseCategory, CourseCategoryAdmin)


class CourseAdminForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = '__all__'


class CourseAdmin(admin.ModelAdmin):
    form = CourseAdminForm
    list_display = ['title', 'short_desc', 'difficulty', 'cost_coins', 'cost_crystals', 'cost_rubles', 'available_from', 'available_to', 'created_at']
    readonly_fields = ['created_at']

admin.site.register(Course, CourseAdmin)


class CourseExecutionAdminForm(forms.ModelForm):

    class Meta:
        model = CourseExecution
        fields = '__all__'


class CourseExecutionAdmin(admin.ModelAdmin):
    form = CourseExecutionAdminForm
    list_display = ['id', 'user', 'course', 'started_at', 'finished_at']
    list_filter = ['finished_at']
    readonly_fields = ['started_at', 'finished_at']

admin.site.register(CourseExecution, CourseExecutionAdmin)



class TaskInline(admin.TabularInline):
    model = Task

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'desc':
            kwargs['widget'] = forms.Textarea(attrs={'rows': 3})
        return super(TaskInline,self).formfield_for_dbfield(db_field,**kwargs)

class LessonBlockInline(admin.TabularInline):
    model = LessonBlock

class LessonAdminForm(forms.ModelForm):

    class Meta:
        model = Lesson
        fields = '__all__'


class LessonAdmin(admin.ModelAdmin):
    form = LessonAdminForm
    inlines = [
        LessonBlockInline,
        TaskInline,
    ]
    list_display = ['title', 'course', 'short_desc', 'created_at', 'order_index']
    list_filter = ['course']
    #readonly_fields = ['title', 'short_desc', 'html', 'created_at', 'order_index']

admin.site.register(Lesson, LessonAdmin)


class LessonBlockAdminForm(forms.ModelForm):

    class Meta:
        model = LessonBlock
        fields = '__all__'


class LessonBlockAdmin(admin.ModelAdmin):
    form = LessonBlockAdminForm
    list_display = ['block_type', 'html', 'order_index']
    #readonly_fields = ['block_type', 'html', 'order_index']

admin.site.register(LessonBlock, LessonBlockAdmin)


class LessonVideoAdminForm(forms.ModelForm):

    class Meta:
        model = LessonVideo
        fields = '__all__'


class LessonVideoAdmin(admin.ModelAdmin):
    form = LessonVideoAdminForm
    list_display = ['src']
    #readonly_fields = ['src']

admin.site.register(LessonVideo, LessonVideoAdmin)


class LessonExecutionAdminForm(forms.ModelForm):

    class Meta:
        model = LessonExecution
        fields = '__all__'


class LessonExecutionAdmin(admin.ModelAdmin):
    form = LessonExecutionAdminForm
    list_display = ['id', 'lesson', 'user', 'started_at', 'finished_at']
    list_filter = ['finished_at']
    readonly_fields = ['started_at', 'finished_at']

admin.site.register(LessonExecution, LessonExecutionAdmin)



class LessonQuestionAdminForm(forms.ModelForm):

    class Meta:
        model = LessonQuestion
        fields = '__all__'

    def clean(self):
        parent = self.cleaned_data.get('parent')
        lesson = self.cleaned_data.get('lesson')
        if parent and parent.lesson.pk != lesson.pk:
            raise forms.ValidationError("Урок и урок родителя не совпадают")
        return self.cleaned_data

class LessonQuestionAdmin(admin.ModelAdmin):
    form = LessonQuestionAdminForm
    list_display = ['message', 'created_at']
    #readonly_fields = ['message', 'created_at']

admin.site.register(LessonQuestion, LessonQuestionAdmin)


class ProgrammingLanguageAdminForm(forms.ModelForm):

    class Meta:
        model = ProgrammingLanguage
        fields = '__all__'


class ProgrammingLanguageAdmin(admin.ModelAdmin):
    form = ProgrammingLanguageAdminForm
    list_display = ['title']
    #readonly_fields = ['title']

admin.site.register(ProgrammingLanguage, ProgrammingLanguageAdmin)





class TaskSkillInline(admin.TabularInline):
    model = TaskSkill
    extra = 0


class TaskCodePairInline(admin.TabularInline):
    model = TaskCodePair
    extra = 0

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ['input_str', 'output_str']:
            kwargs['widget'] = forms.Textarea(attrs={'rows': 3})
        return super(TaskCodePairInline,self).formfield_for_dbfield(db_field,**kwargs)


class TaskOptionInline(admin.TabularInline):
    model = TaskOption
    extra = 0

class TaskConditionInline(admin.TabularInline):
    model = TaskCondition
    extra = 0

class TaskNoteInline(admin.TabularInline):
    model = TaskNote
    extra = 0

class TaskAdminForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = '__all__'


class TaskAdmin(admin.ModelAdmin):
    form = TaskAdminForm
    inlines = [
        TaskSkillInline,
        TaskOptionInline,
        TaskCodePairInline,
        TaskConditionInline,
        TaskNoteInline,
    ]
    list_display = ['title', 'task_type', 'difficulty', 'only_manual', 'created_at', 'external_code_id', 'gives_coins', 'gives_base_experience', 'can_be_uploaded_until']
    list_filter = ['task_type', 'difficulty', 'only_manual', 'programming_language']
    readonly_fields = ['created_at']

admin.site.register(Task, TaskAdmin)



class TaskQuestionAdminForm(forms.ModelForm):

    class Meta:
        model = TaskQuestion
        fields = '__all__'

    def clean(self):
        parent = self.cleaned_data.get('parent')
        task = self.cleaned_data.get('task')
        if parent and parent.task.pk != task.pk:
            raise forms.ValidationError("Задание и задание родителя не совпадают")
        return self.cleaned_data


class TaskQuestionAdmin(admin.ModelAdmin):
    form = TaskQuestionAdminForm
    list_display = ['message', 'created_at']
    readonly_fields = ['created_at']

admin.site.register(TaskQuestion, TaskQuestionAdmin)



class TaskConditionAdminForm(forms.ModelForm):

    class Meta:
        model = TaskCondition
        fields = '__all__'


class TaskConditionAdmin(admin.ModelAdmin):
    form = TaskConditionAdminForm
    list_display = ['id', 'uid', 'title', 'need_to_use', 'short_desc', 'created_at']
    #readonly_fields = ['need_to_use', 'title', 'short_desc', 'uid', 'created_at']

admin.site.register(TaskCondition, TaskConditionAdmin)


class TaskOptionAdminForm(forms.ModelForm):

    class Meta:
        model = TaskOption
        fields = '__all__'


class TaskOptionAdmin(admin.ModelAdmin):
    form = TaskOptionAdminForm
    list_display = ['id', 'task', 'title', 'is_correct']
    #readonly_fields = ['title', 'is_correct']

admin.site.register(TaskOption, TaskOptionAdmin)



class CompilerCheckNoteInline(admin.TabularInline):
    model = CompilerCheckNote
    extra = 0


class TaskExecutionAdminForm(forms.ModelForm):

    class Meta:
        model = TaskExecution
        fields = '__all__'


class TaskExecutionAdmin(admin.ModelAdmin):
    form = TaskExecutionAdminForm
    inlines = [
        CompilerCheckNoteInline,
    ]
    list_display = ['id', 'task', 'user', 'is_correct', 'started_at', 'finished_at', 'status', 'status_string']
    list_filter = ['finished_at', 'status', 'checked_at', 'is_active']
    readonly_fields = ['started_at', 'finished_at']

admin.site.register(TaskExecution, TaskExecutionAdmin)


class ReferenceCodeAdminForm(forms.ModelForm):

    class Meta:
        model = ReferenceCode
        fields = '__all__'


class ReferenceCodeAdmin(admin.ModelAdmin):
    form = ReferenceCodeAdminForm
    list_display = ['id', 'task', 'short_desc']
    #readonly_fields = ['task', 'short_desc']

admin.site.register(ReferenceCode, ReferenceCodeAdmin)


class TaskConditionHistoryAdminForm(forms.ModelForm):

    class Meta:
        model = TaskConditionHistory
        fields = '__all__'


class TaskConditionHistoryAdmin(admin.ModelAdmin):
    form = TaskConditionHistoryAdminForm
    list_display = ['id', 'task_exec', 'task_condition', 'is_correct', 'created_at']
    #readonly_fields = ['is_correct']

admin.site.register(TaskConditionHistory, TaskConditionHistoryAdmin)


class TaskCodePairAdminForm(forms.ModelForm):

    class Meta:
        model = TaskCodePair
        fields = '__all__'
        widgets = {
            'input_str': forms.Textarea(attrs={'rows': 3}),
            'output_str': forms.Textarea(attrs={'rows': 3}),
        }


class TaskCodePairAdmin(admin.ModelAdmin):
    form = TaskCodePairAdminForm
    list_display = ['id', 'task', 'number', 'input_str', 'output_str', 'on_error_text']
    list_filter = ['number']
    #readonly_fields = ['number', 'input_str', 'output_str', 'on_error_text']

admin.site.register(TaskCodePair, TaskCodePairAdmin)



class TaskCodeCheckDetailAdminForm(forms.ModelForm):

    class Meta:
        model = TaskCodeCheckDetail
        fields = '__all__'


class TaskCodeCheckDetailAdmin(admin.ModelAdmin):
    form = TaskCodeCheckDetailAdminForm
    list_display = ['id', 'pair', 'status']
    list_filter = ['status']
    #readonly_fields = ['status']

admin.site.register(TaskCodeCheckDetail, TaskCodeCheckDetailAdmin)


class TaskNoteAdminForm(forms.ModelForm):

    class Meta:
        model = TaskNote
        fields = '__all__'


class TaskNoteAdmin(admin.ModelAdmin):
    form = TaskNoteAdminForm
    list_display = ['note', 'points', 'note_type']
    list_filter = ['note_type', 'points']
    #readonly_fields = ['note_type', 'note', 'points']

admin.site.register(TaskNote, TaskNoteAdmin)


class TaskCodeManualCheckNoteAdminForm(forms.ModelForm):

    class Meta:
        model = TaskCodeManualCheckNote
        fields = '__all__'


class TaskCodeManualCheckNoteAdmin(admin.ModelAdmin):
    form = TaskCodeManualCheckNoteAdminForm


admin.site.register(TaskCodeManualCheckNote, TaskCodeManualCheckNoteAdmin)



class ManualDifficultySelectAdminForm(forms.ModelForm):

    class Meta:
        model = ManualDifficultySelect
        fields = '__all__'


class ManualDifficultySelectAdmin(admin.ModelAdmin):
    form = ManualDifficultySelectAdminForm
    list_display = ['pk', 'user', 'task_exec', 'rate', 'created_at']
    readonly_fields = ['user', 'task_exec', 'created_at']

admin.site.register(ManualDifficultySelect, ManualDifficultySelectAdmin)


class ReferenceCodeViewAdminForm(forms.ModelForm):

    class Meta:
        model = ReferenceCodeView
        fields = '__all__'


class ReferenceCodeViewAdmin(admin.ModelAdmin):
    form = ReferenceCodeViewAdminForm
    list_display = ['pk', 'user', 'task', 'created_at']
    # readonly_fields = ['user', 'task_exec', 'created_at']

admin.site.register(ReferenceCodeView, ReferenceCodeViewAdmin)



class CompilerNoteAdminForm(forms.ModelForm):

    class Meta:
        model = CompilerNote
        fields = '__all__'


class CompilerNoteAdmin(admin.ModelAdmin):
    form = CompilerNoteAdminForm
    list_display = ['note', 'note_type']
    list_filter = ['note_type']
    #readonly_fields = ['note_type', 'note', 'points']

admin.site.register(CompilerNote, CompilerNoteAdmin)




class CompilerCheckNoteAdminForm(forms.ModelForm):

    class Meta:
        model = CompilerCheckNote
        fields = '__all__'


class CompilerCheckNoteAdmin(admin.ModelAdmin):
    form = CompilerCheckNoteAdminForm
    list_display = ['pk', 'check_obj', 'note_obj']


admin.site.register(CompilerCheckNote, CompilerCheckNoteAdmin)
