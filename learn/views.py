from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView, ListView, UpdateView, CreateView
from learn.models import Speaker, CourseCategory, Course, CourseExecution, Lesson, LessonBlock, LessonVideo, LessonExecution, LessonQuestion, ProgrammingLanguage, Task, TaskCondition, TaskOption, TaskExecution, TaskConditionHistory, TaskCodePair, TaskCodeCheckDetail, TaskNote, TaskCodeManualCheckNote, ManualDifficultySelect, ReferenceCode, ReferenceCodeView, CompilerNote, CompilerCheckNote
from django.contrib.contenttypes.models import ContentType
from user_app.models import Transaction, UseStat
from knowledge.models import GraphNode, GraphNodeType
from learn.custom_verificators import process_task, process_pairs
import datetime
from django.utils import timezone
import rest_framework
from rest_framework.response import Response

from learn.serializers import CourseSerializer, CoursePageSerializer, LessonPageSerializer, LessonSerializer, CourseExecutionSerializer, LessonExecutionSerializer, TaskSerializer, TaskExecutionSerializer, TaskExecutionAdminSerializer, TaskExecutionFinalSerializer, TaskNoteSerializer, ReferenceCodeSerializer, CompilerNoteSerializer, CompilerCheckNoteSerializer
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from user_app.authentication import MyTokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.exceptions import PermissionDenied

from django.conf import settings
import simplejson
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.models import User, Group
import os
from django.db.models import Count
from main.logger import save_log


@login_required
def tree(request):
	entities = Course.objects.all()
	return render(request, 'learn/tree.html', {'entities': entities})



@login_required
def create_executions(request):
	if request.GET.get('method') == "POST":
		errors = list()
		with open(os.path.join(settings.BASE_DIR, 'export.json'), encoding='utf-8') as f:
			course = Course.objects.get(title="Основы C++ (осень 17-18)")

			data = simplejson.loads(f.read())
			last_names = list()
			last_names_arr = dict()
			for item in data:
				last_name = item['last_name']
				if last_name not in last_names:
					last_names.append(last_name)
			
			for obj in User.objects.filter(last_name__in=last_names):
				last_names_arr.update({obj.last_name: obj})

			for item in data:
				last_name = item['last_name']
				sended_at = item['sended_at']
				l_number = item['l_number']
				t_number = item['t_number']
				# task_code = item['task_code']
				code = item['code']
				user = last_names_arr[last_name]

				task_execution = TaskExecution()
				lesson = Lesson.objects.get(course=course, order_index=l_number)

				less_exec = LessonExecution.objects.filter(user=user, lesson=lesson)
				if not less_exec:
					less_exec = LessonExecution()
					less_exec.lesson = lesson
					less_exec.difficulty = '3'
					less_exec.user = user
					less_exec.save()

				try:
					task_execution.task = Task.objects.get(lesson=lesson, order_index=t_number)
				except Task.DoesNotExist:
					errors.append("{} {}.{} {}".format(last_name, l_number, t_number, sended_at))
					continue

				task_execution.status_string = "Ожидает проверки"
				if task_execution.task.only_manual:
					task_execution.status_string = "Ожидайте проверки преподавателем"
				
				dt = datetime.datetime.strptime(sended_at, "%Y-%m-%d %H:%M:%S")
				if task_execution.task.can_be_uploaded_until and task_execution.task.can_be_uploaded_until < timezone.make_aware(dt):
					task_execution.status = "TO"
					task_execution.status_string = task_execution.get_status_string(task_execution.status)

				task_execution.user = user
				task_execution.answer = code
				task_execution.finished_at = datetime.datetime.strptime(sended_at, "%Y-%m-%d %H:%M:%S")
				task_execution.save()

		if errors:
			return HttpResponse("<br>".join(errors))
		return HttpResponse("Ok")

	return HttpResponse('<form><input type="submit" name="method" value="POST"></form>')



@login_required
def create_initial(request):
	if request.GET.get('method') == "POST":
		speaker_me = Speaker.objects.create(last_name="Пономаренко", name="Дмитрий", surname="Вячеславович", sex='M', work_from=datetime.datetime(2017, 1, 1))
		lang_node_type = GraphNodeType.objects.create(title="Язык программирования")
		notion_node_type = GraphNodeType.objects.create(title="Понятие")
		func_node_type = GraphNodeType.objects.create(title="Функция")

		cpp_node = GraphNode.objects.create(node_type=lang_node_type, title="С++")
		cpp_lang = ProgrammingLanguage.objects.create(title="C++", ace_name="c_cpp", default_filename="main.cpp", lang_id=1)
		cpp_category = CourseCategory.objects.create(title="C++", node=cpp_node)

		return HttpResponse("Ok")

	return HttpResponse('<form><input type="submit" name="method" value="POST"></form>')



@login_required
def create_cpp_course(request):
	if request.GET.get('method') == "POST":
		cpp_lang = ProgrammingLanguage.objects.get(title="C++")
		cpp_category = CourseCategory.objects.get(title="C++")
		my_user = User.objects.get(username="loki912")
		
		if not cpp_category:
			raise Exception("No c++ category")

		data = {
			'_courses': [
				{
					'category': cpp_category,
					'title': "Основы C++ (осень 17-18)",
					'short_desc': "Краткое описание курса C++",
					'html': "Полное описание курса C++",
					'difficulty': '1',
					'_group': "CPP_17_18_1",
					'_lessons': [
						{
							'title': "01 Введение в программирование",
							'short_desc': "Знакомство с особенностями алгоритмов",
							'html': "Были рассмотрены:<br>алгоритмы и программы<br>как компьютер выполняет программы <br>языки программирования",
							'order_index': 1,
							'_tasks': [
								{
									'title': "Первый вопрос",
									'desc': "<p>Вы проходите курс по своему желанию?</p>",
									'programming_language': cpp_lang,
									'task_type': 'O',
									'difficulty': '3',
									'gives_coins': 10,
									'gives_base_experience': 10,
									'order_index': 1,
									'_options': [
										{
											'title': "Да",
											'is_correct': True,
											'message': "Отлично! Тогда, продолжаем",
										},
										{
											'title': "Нет",
											'is_correct': True,
											'message': "Ну что ж, постараемся тебя заинтересовать",
										},
									],
								},
							],
						},
						{
							'title': "02 Первая программа",
							'short_desc': "Структура кода C++",
							'html': "Были рассмотрены:<br>структура программы<br>комментарии<br>арифметические операторы<br>типы данных<br>переменные, названия переменных <br>ввод/вывод данных<br>русский язык через setlocale<br>создание проекта в Visual Studio<br>запуск проекта",
							'order_index': 2,
							'_tasks': [
								{
									'title': "Сумма 3х чисел",
									'desc': "<p>Запросить у пользователя 3 числа. Вывести их сумму</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 20,
									'gives_base_experience': 20,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 19, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "5\n8\n3",
											'output_str': "16",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 2,
											'input_str': "4\n2\n11",
											'output_str': "17",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 3,
											'input_str': "0\n2\n4",
											'output_str': "6",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 4,
											'input_str': "3\n-5\n-3",
											'output_str': "-5",
											'on_error_text': "Необходимо обрабатывать отрицательные числа",
											'judge_type': '15',
										},
										{
											'number': 5,
											'input_str': "0\n0\n0",
											'output_str': "0",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 6,
											'input_str': "-4\n-1\n-3",
											'output_str': "-8",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 7,
											'input_str': "3\n4\n0",
											'output_str': "7",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 8,
											'input_str': "0\n-5\n0",
											'output_str': "-5",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 9,
											'input_str': "1\n5\n7",
											'output_str': "13",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 10,
											'input_str': "20\n40\n100",
											'output_str': "160",
											'on_error_text': "",
											'judge_type': '15',
										},
										{
											'number': 11,
											'input_str': "94\n65\n21",
											'output_str': "180",
											'on_error_text': "",
											'judge_type': '15',
										},

									],
								},
								{
									'title': "Сумма, разность, произведение",
									'desc': "<p>Запросить 2 числа. Найти их сумму, разность и произведение.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 25,
									'gives_base_experience': 25,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 19, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "8\n5",
											'output_str': "13\n3\n40",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "13\n4",
											'output_str': "17\n9\n52",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "0\n0",
											'output_str': "0\n0\n0",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "-1\n3",
											'output_str': "2\n-4\n-3",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "-1\n-3",
											'output_str': "-4\n2\n3",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 6,
											'input_str': "0\n2",
											'output_str': "2\n-2\n0",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "1000\n24",
											'output_str': "1024\n976\n24000",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 8,
											'input_str': "4.2\n2.3",
											'output_str': "6.5\n1.9\n9.66",
											'judge_type': "15",
											'on_error_text': "Необходимо обрабатывать вещественные числа",
										},
										{
											'number': 9,
											'input_str': "-0.6\n8.2",
											'output_str': "7.6\n-8.8\n-4.92",
											'judge_type': "15",
											'on_error_text': "",
										},

									],
								},
								{
									'title': "Метры в футы",
									'desc': "<p>Запросить у пользователя расстояние к метрах. Вывести расстояние в футах (1 фут = 0,305 м).</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 30,
									'gives_base_experience': 30,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 19, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "500",
											'output_str': "1639.34",
											'judge_type': "2",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "840",
											'output_str': "2754.1",
											'judge_type': "2",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "0",
											'output_str': "0",
											'judge_type': "2",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "7839",
											'output_str': "25701.6",
											'judge_type': "2",
											'on_error_text': "Необходимо обрабатывать вещественные числа",
										},
										{
											'number': 5,
											'input_str': "14123.3",
											'output_str': "46304.9",
											'judge_type': "2",
											'on_error_text': "",
										},

									],
								}, 
							],
						},
						{
							'title': "03 Условные выражения и условия",
							'short_desc': "Экранирование строк. Логические выражения. Условия",
							'html': "Были рассмотрены:<br>деление, остаток от деления<br>условные выражения<br>присваивание vs сравнение<br>условия (if, else if, esle)",
							'order_index': 3,
							'_tasks': [
								{
									'title': "Определить четность и окончание - 7",
									'desc': "<p>Дано натуральное число. Определить: <br>а) является ли оно четным; <br>б) оканчивается ли оно цифрой 7.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 30,
									'gives_base_experience': 30,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 23, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "15",
											'output_str': "a) no\nb) no",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "18",
											'output_str': "a) yes\nb) no",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "37",
											'output_str': "a) no\nb) yes",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "2",
											'output_str': "a) yes\nb) no",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "7",
											'output_str': "a) no\nb) yes",
											'on_error_text': "",
										},
										{
											'number': 6,
											'input_str': "1",
											'output_str': "a) no\nb) no",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "107",
											'output_str': "a) no\nb) yes",
											'on_error_text': "",
										},
										{
											'number': 8,
											'input_str': "26",
											'output_str': "a) yes\nb) no",
											'on_error_text': "",
										},
										{
											'number': 9,
											'input_str': "100",
											'output_str': "a) yes\nb) no",
											'on_error_text': "",
										},
										{
											'number': 10,
											'input_str': "61",
											'output_str': "a) no\nb) no",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Самое большое, самое маленькое",
									'desc': "<p>Даны три различных целых числа. Определить, какое из них (первое, второе или третье): <br>а) самое большое;<br>б) самое маленькое;</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 40,
									'gives_base_experience': 40,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 23, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 3,
									'_pairs': [
										{
											'number': 1,
											'input_str': "9\n18\n3",
											'output_str': "18\n3",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "93\n18\n10",
											'output_str': "93\n10",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "5\n15\n20",
											'output_str': "20\n5",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "0\n-1\n2",
											'output_str': "2\n-1",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "10\n0\n1",
											'output_str': "10\n0",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 6,
											'input_str': "-10\n-7\n0",
											'output_str': "0\n-10",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "-19\n-12\n-2",
											'output_str': "-2\n-19",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 8,
											'input_str': "0\n12\n1",
											'output_str': "12\n0",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 9,
											'input_str': "-5\n2\n1",
											'output_str': "2\n-5",
											'judge_type': "15",
											'on_error_text': "",
										},
										{
											'number': 10,
											'input_str': "9\n34\n-2",
											'output_str': "34\n-2",
											'judge_type': "15",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Километры и футы (сравнение)",
									'desc': "<p>Известны  два  расстояния:  одно  в  километрах,  другое  —  в  футах (1 фут = 0,305 м). Какое из расстояний меньше?</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 45,
									'gives_base_experience': 45,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 23, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 3,
									'_pairs': [
										{
											'number': 1,
											'input_str': "5\n15000",
											'output_str': "5 km>15000 ft",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "1\n3500",
											'output_str': "1 km<3500 ft",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "305\n1000000",
											'output_str': "305 km=1000000 ft",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "0\n334",
											'output_str': "0 km<334 ft",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "23\n0",
											'output_str': "23 km>0 ft",
											'on_error_text': "",
										},
									],
								}, 

							],
						},
						{
							'title': "04 Оптимизации",
							'short_desc': "Сокращения. Приведение типов. Константы. Логические операторы",
							'html': "Были рассмотрены:<br>сокращения (инкремент, декремент) <br>приведение типов (явное, неявное) <br>константы<br>логические операторы (и, или, не)",
							'order_index': 4,
							'_tasks': [
								{
									'title': "Существование треугольника",
									'desc': "<p>Напишите программу, которая запрашивает значения углов треугольника, и выводит на экран, может ли существовать такой треугольник, или нет.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 40,
									'gives_base_experience': 40,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "60\n30\n90",
											'output_str': "Yes",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "90\n45\n50",
											'output_str': "No",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "23\n1\n156",
											'output_str': "Yes",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "3\n2\n1",
											'output_str': "No",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "0\n100\n80",
											'output_str': "No",
											'on_error_text': "Углы не могут быть равны 0",
										},
										{
											'number': 6,
											'input_str': "0\n0\n180",
											'output_str': "No",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "-10\n100\n90",
											'output_str': "No",
											'on_error_text': "Отрицательных углов не существует",
										},
										{
											'number': 8,
											'input_str': "98\n34\n48",
											'output_str': "Yes",
											'on_error_text': "",
										},
										{
											'number': 9,
											'input_str': "10\n80\n90",
											'output_str': "Yes",
											'on_error_text': "",
										},
										{
											'number': 10,
											'input_str': "-20\n-120\n140",
											'output_str': "No",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Время суток",
									'desc': "<p>Программа должна вывести приветствие на основании времени суток. Деление выглядит следующим образом:<ul><li>00:00-04:00 - ночь;</li><li>04:00-12:00 - утро;</li><li>12:00-17:00 - день;</li><li>17:00-24:00 - вечер.</li></ul></p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 30,
									'gives_base_experience': 30,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 3,
									'_pairs': [
										{
											'number': 1,
											'input_str': "15",
											'output_str': "Добрый день",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "20",
											'output_str': "Добрый вечер",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "9",
											'output_str': "Доброе утро",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "4",
											'output_str': "Доброе утро",
											'on_error_text': "В 4 часа уже утро",
										},
										{
											'number': 5,
											'input_str': "17",
											'output_str': "Добрый вечер",
											'on_error_text': "В 17 часов уже вечер",
										},
										{
											'number': 6,
											'input_str': "2",
											'output_str': "Доброй ночи",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "12",
											'output_str': "Добрый день",
											'on_error_text': "В 12 часов уже день",
										},
										{
											'number': 8,
											'input_str': "13",
											'output_str': "Добрый день",
											'on_error_text': "",
										},
										{
											'number': 9,
											'input_str': "0",
											'output_str': "Доброй ночи",
											'on_error_text': "0 часов это ночь",
										},
										{
											'number': 10,
											'input_str': "3",
											'output_str': "Доброй ночи",
											'on_error_text': "",
										},

									],
								}, 
								{
									'title': "Рубли",
									'desc': "<p>Написать программу, которая дописывает слово «рубль» в правильной форме</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 55,
									'gives_base_experience': 55,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 3,
									'_pairs': [
										{
											'number': 1,
											'input_str': "50",
											'output_str': "50 рублей",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "21",
											'output_str': "21 рубль",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "74",
											'output_str': "74 рубля",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "0",
											'output_str': "0 рублей",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "11",
											'output_str': "11 рублей",
											'on_error_text': "11 ",
										},
										{
											'number': 6,
											'input_str': "342",
											'output_str': "342 рубля",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "84974234",
											'output_str': "84974234 рубля",
											'on_error_text': "",
										},
										{
											'number': 8,
											'input_str': "14",
											'output_str': "14 рублей",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Расстояние между точками",
									'desc': "<p>Напишите программу, которая будет запрашивать у пользователя координаты двух точек на плоскости, а затем выведет на экран расстояние между ними. Значения при вводе в следующем порядке<br>a. Координата X первой точки<br>b. Координата Yпервой точки<br>c. Координата X второй точки<br>d. Координата Yвторой точки<br><em>Для вычисления квадратного корня используйте функцию sqrt из библиоткеки cmath</em></p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 50,
									'gives_base_experience': 50,
									'order_index': 4,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "2\n5\n2\n0",
											'output_str': "5",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 2,
											'input_str': "7\n6\n2\n2",
											'output_str': "6.40312",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 3,
											'input_str': "0\n0\n0\n0",
											'output_str': "0",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 4,
											'input_str': "-4\n-2\n9\n3",
											'output_str': "13.9284",
											'on_error_text': "Необходимо обрабатывать отрицательные числа",
											'judge_type': "2",
										},
										{
											'number': 5,
											'input_str': "-1\n-2\n-3\n-4",
											'output_str': "2.82842",
											'on_error_text': "",
											'judge_type': "2",
										}, 
									],
								}, 
							],
						},
						{
							'title': "05 Работа с русским языком",
							'short_desc': "Эталонный код 2 занятия. Русский (cp866)",
							'html': "Были рассмотрены:<br>подключение кириллицы<br>решение задач<br>эталонный код",
							'order_index': 5,
							'_tasks': [
								{
									'title': "Трехзначное число в слова",
									'desc': "<p>Напишите программу, которая получает на ввод трехзначное  число, а затем выводит его словесное название.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 45,
									'gives_base_experience': 45,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "147",
											'output_str': "Сто сорок семь",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "834",
											'output_str': "Восемьсот тридцать четыре",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "100",
											'output_str': "Сто",
											'on_error_text': "Число может иметь нули в разрядах",
										},
										{
											'number': 4,
											'input_str': "106",
											'output_str': "Сто шесть",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "712",
											'output_str': "Семьсот двенадцать",
											'on_error_text': "",
										},
										{
											'number': 6,
											'input_str': "211",
											'output_str': "Двести одиннадцать",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "541",
											'output_str': "Пятьсот сорок один",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Калькулятор",
									'desc': "<p>Напишите программу-калькулятор, которая принимает первое число, операцию (+, -, *, /) и второе число и выводит результат</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 40,
									'gives_base_experience': 40,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 45),
									'master_judge': '0',
									'external_code_id': 1,
									'_pairs': [
										{
											'number': 1,
											'input_str': "5\n*\n3",
											'output_str': "15",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 2,
											'input_str': "5 / 2",
											'output_str': "2.5",
											'on_error_text': "При делении результат должен быть вещественным",
											'judge_type': "2",
										},
										{
											'number': 3,
											'input_str': "0 + 0",
											'output_str': "0",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 4,
											'input_str': "8 - 10",
											'output_str': "-2",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 5,
											'input_str': "9 - -3",
											'output_str': "12",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 6,
											'input_str': "-4 * -2",
											'output_str': "8",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 7,
											'input_str': "0.2 + 0.2",
											'output_str': "0.4",
											'on_error_text': "Необходимо обрабатывать вещественные числа",
											'judge_type': "2",
										},
										{
											'number': 8,
											'input_str': "5\n/\n0",
											'output_str': "Error",
											'on_error_text': "На ноль делить нельзя",
											'judge_type': "1",
											'external_pair_id': 1,
										}, 
									],
								}, 
							],
						},
						{
							'title': "06 Практика",
							'short_desc': "",
							'html': "",
							'order_index': 6,
							'_tasks': [
								{
									'title': "Корни квадратного уравнения",
									'desc': "<p>Напишите программу, которая находит корни (или корень) уравнения: ax<sup>2</sup>+bx+c=0, если они есть. Если их нет, то выводит ошибку. Программа должна запрашивать у пользователя ввести a, b и с.<br>ax<sup>2</sup> + bx + c = 0, где x–переменная; a, b, c – некоторые числа, причем a ≠ 0.<br>(TODO: картинка)<br>Если D > 0, то уравнение имеет два корня;<br>Если D = 0, то уравнение один корень;<br>Если D &lt; 0, то корней нет;<br>(TODO: картинка)</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 55,
									'gives_base_experience': 55,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 30, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 3,
									'_pairs': [
										{
											'number': 1,
											'input_str': "1\n2\n6",
											'output_str': "Нет корней",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "1\n-2\n1",
											'output_str': "1",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "2\n-3\n1",
											'output_str': "1\n0.5",
											'judge_type': "2",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "3\n-2\n1",
											'output_str': "Нет корней",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "1\n6\n-3",
											'output_str': "0.464\n−6.464",
											'judge_type': "2",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Расписание занятий",
									'desc': "<p>Напишите программу, которая запрашивает номер дня недели (пн - 1, вт - 2 и т д.) и выводит на экран ваше расписание уроков на этот день и ошибку, если введено слишком большое или маленькое число.Расписание дня выводится в соответствии с таблицей:<table><tr><td>№</td><td>понедельник</td><td>вторник</td><td>среда</td><td>четверг</td><td>пятница</td><td>суббота</td></tr><tr><td>1</td><td>информатика</td><td>история</td><td>математика</td><td>русский язык</td><td>черчение</td><td>математика</td></tr><tr><td>2</td><td>география</td><td>физ-ра</td><td>информатика</td><td>ОБЖ</td><td>биология</td><td>физ-ра</td></tr><tr><td>3</td><td>русский язык</td><td>черчение</td><td>литература</td><td>обществознание</td><td>математика</td><td>русский язык</td></tr><tr><td>4</td><td>литература</td><td>ОБЖ</td><td>биология</td><td>труд</td><td>труд</td><td>история</td></tr><tr><td>5</td><td></td><td>обществознание</td><td>физ-ра</td><td>труд</td><td>информатика</td><td></td></tr><tr><td>6</td><td></td><td></td><td>география</td><td></td><td></td><td></td></tr></table></p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 45,
									'gives_base_experience': 45,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 30, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "3",
											'output_str': "математика\nинформатика\nлитература\nбиология\nфиз-ра\nгеография",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "10",
											'output_str': "Дня недели с таким порядковым номером не существует",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "1",
											'output_str': "информатика\nгеография\nрусский язык\nлитература",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "0",
											'output_str': "Дня недели с таким порядковым номером не существует",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "2",
											'output_str': "история\nфиз-ра\nчерчение\nОБЖ\nобществознание",
											'on_error_text': "",
										},
										{
											'number': 6,
											'input_str': "4",
											'output_str': "русский язык\nОБЖ\nобществознание\nтруд\nтруд",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "5",
											'output_str': "черчение\nбиология\nматематика\nтруд\nинформатика",
											'on_error_text': "",
										},
										{
											'number': 8,
											'input_str': "6",
											'output_str': "математика\nфиз-ра\nрусский язык\nистория",
											'on_error_text': "",
										},
										{
											'number': 9,
											'input_str': "-5",
											'output_str': "Дня недели с таким порядковым номером не существует",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Расстояние брошенного объекта",
									'desc': "<p>Напишите программу, которая вычисляет расстояние, которое пролетит объект, брошенный под углом к горизонту. Программа запрашивает значение угла и начальной скорости и выводит на экран расстояние. Для вычисления синуса угла используйте функцию sin из библиотеки cmath. Расстояние выводится целым числом (TODO: картинка)</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 55,
									'gives_base_experience': 55,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 10, 30, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "10\n10",
											'output_str': "3",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 2,
											'input_str': "15\n22",
											'output_str': "24",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 3,
											'input_str': "3\n70",
											'output_str': "52",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 4,
											'input_str': "80\n10",
											'output_str': "3",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 5,
											'input_str': "0\n34",
											'output_str': "0",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 6,
											'input_str': "12\n0",
											'output_str': "0",
											'on_error_text': "",
											'judge_type': "2",
										},
										{
											'number': 7,
											'input_str': "-12\n2",
											'output_str': "0",
											'on_error_text': "",
											'judge_type': "2",
										},

									],
								}, 
							],
						},
						{
							'title': "07 Циклы",
							'short_desc': "for, while. Эталонный код 3 занятия",
							'html': "Были рассмотрены:<br>эталонный код задач 3 занятия <br>for <br>while<br>логические операторы (и, или, не)",
							'order_index': 7,
							'_tasks': [
								{
									'title': "Стоимость товара",
									'desc': "<p>Одна штука некоторого товара стоит 20,4 руб. Запросить у пользователя число и напечатать таблицу стоимости от 2 до веденного количества штук этого товара. Вывести ошибку, если введенное число ошибочно.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 45,
									'gives_base_experience': 45,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "6",
											'output_str': "2 - 40.8\n3 - 61.2\n4 - 81.6\n5 - 102\n6 - 122.4",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "1",
											'output_str': "Error",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "0",
											'output_str': "Error",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "4",
											'output_str': "2 - 40.8\n3 - 61.2\n4 - 81.6",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "-10",
											'output_str': "Error",
											'on_error_text': "",
										},

										{
											'number': 6,
											'input_str': "8",
											'output_str': "2 - 40.8\n3 - 61.2\n4 - 81.6\n5 - 102\n6 - 122.4\n7 - 142.8\n8 - 163.2",
											'on_error_text': "",
										},
									],
								}, 
								{
									'title': "Сумма чисел от 1 до указанного",
									'desc': "<p>Напишите программу, которая получает на вход число, и выводит на экран сумму всех чисел до этого числа.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 45,
									'gives_base_experience': 45,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{ 
											'number': 1,
											'input_str': "10",
											'output_str':"45",
											'on_error_text': "",
										},
										{ 
											'number': 2,
											'input_str': "5",
											'output_str':"10",
											'on_error_text': "",
										},
										{ 
											'number': 3,
											'input_str': "0",
											'output_str':"0",
											'on_error_text': "",
										},
										{ 
											'number': 4,
											'input_str': "35",
											'output_str':"595",
											'on_error_text': "",
										},

									],
								}, 
								{
									'title': "Треугольник из символов",
									'desc': "<p>Напишите программу, которая получает на вход число, а затем выводит на экран перевернутый треугольник из символов *, в основании которого будет столько же символов *, сколько было указано в вводе. С каждой строкой количество символов * должно уменьшаться на 1.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 60,
									'gives_base_experience': 60,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "7",
											'output_str': "*******\n******\n*****\n****\n***\n**\n*",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "2",
											'output_str': "**\n*",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "4",
											'output_str': "****\n***\n**\n*",
											'on_error_text': "",
										},

									],
								}, 
								{
									'title': "Перевернутое число",
									'desc': "<p>Напишите программу, которая получает на вход число и выводит его в обратном порядке.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 65,
									'gives_base_experience': 65,
									'order_index': 4,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "84937",
											'output_str': "73948",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "23",
											'output_str': "32",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "909",
											'output_str': "909",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "0",
											'output_str': "0",
											'on_error_text': "Ноль тоже число. Его нужно выводить",
										},
									],
								}, 
							],
						}, 
						{
							'title': "08 Switch",
							'short_desc': "Эталонный код 4 и 5 занятий. Switch + break. Вложенные циклы. Псевдослучайные числа",
							'html': "Были рассмотрены:<br>система оценки кода<br>эталонный код задач 4 занятия <br>эталонный код задач 5 занятия<br>вложенные циклы for, итераторы <br>switch, break<br>do while<br>случайное значение (rand(), srand(), time(0))",
							'order_index': 8,
							'_tasks': [
								{
									'title': "Конвертер расстояний",
									'desc': "<p>Напишите программу, которая получает на вход два числа, первое — номер единицы длины (целое число в диапазоне 1 – 5 включительно), второе — длина отрезка в этих единицах. Единицы длины пронумерованы следующим образом:<ol><li>дециметр</li><li>километр</li><li>метр</li><li>миллиметр</li><li>сантиметр</li></ol>Вывести длину отрезка в метрах.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 55,
									'gives_base_experience': 55,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "3\n700",
											'output_str': "700",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "5\n1078",
											'output_str': "10.78",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "3\n0",
											'output_str': "0",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "1\n6435",
											'output_str': "643.5",	
											'on_error_text': "",
										},

									],
								}, 
								{
									'title': "Предыдущая дата",
									'desc': "<p>Даны два целых числа: D (день) и M (месяц), определяющие правильную дату не високосного года. Вывести значения D и M для даты, предшествующей указанной.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 65,
									'gives_base_experience': 65,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "30\n4",
											'output_str': "29\n4",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "1\n9",
											'output_str': "31\n8",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "14\n3",
											'output_str': "13\n3",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "1\n8",
											'output_str': "31\n7",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "1\n12",
											'output_str': "30\n11",
											'on_error_text': "",
										},
										{
											'number': 6,
											'input_str': "1\n1",
											'output_str': "31\n12",
											'on_error_text': "",
										},

									],
								}, 
								{
									'title': "Знаки зодиака",
									'desc': "<p>Напишите программу, которая получает на вход два целых числа: D (день) и M (месяц) (гарантируется существование даты). Нужно вывести знак Зодиака, соответствующий этой дате: </p><ul><li>Водолей (20.1–18.2)</li><li>Рыбы (19.2–20.3)</li><li>Овен (21.3–19.4)</li><li>Телец (20.4–20.5)</li><li>Близнецы (21.5–21.6)</li><li>Рак (22.6–22.7)</li><li>Лев (23.7–22.8)</li><li>Дева (23.8–22.9)</li><li>Весы (23.9–22.10)</li><li>Скорпион (23.10–22.11)</li><li>Стрелец (23.11–21.12)</li><li>Козерог (22.12–19.1)</li></ul>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 65,
									'gives_base_experience': 65,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{ 
											'number': 1,
											'input_str': "12\n1",
											'output_str': "Козерог",
											'on_error_text': "",
										},
										{ 
											'number': 2,
											'input_str': "10\n11",
											'output_str': "Скорпион",
											'on_error_text': "",
										},
										{ 
											'number': 3,
											'input_str': "8\n2",
											'output_str': "Водолей",
											'on_error_text': "",
										},
										{ 
											'number': 4,
											'input_str': "22\n11",
											'output_str': "Скорпион",
											'on_error_text': "",
										},
										{ 
											'number': 5,
											'input_str': "25\n7",
											'output_str': "Лев",
											'on_error_text': "",
										},
										{ 
											'number': 6,
											'input_str': "5\n10",
											'output_str': "Весы",
											'on_error_text': "",
										},
										{ 
											'number': 7,
											'input_str': "22\n3",
											'output_str': "Овен",
											'on_error_text': "",
										},
										{ 
											'number': 8,
											'input_str': "16\n5",
											'output_str': "Телец",
											'on_error_text': "",
										},
										{ 
											'number': 9,
											'input_str': "26\n5",
											'output_str': "Близнецы",
											'on_error_text': "",
										},
										{ 
											'number': 10,
											'input_str': "27\n6",
											'output_str': "Рак",
											'on_error_text': "",
										},
										{ 
											'number': 11,
											'input_str': "23\n8",
											'output_str': "Дева",
											'on_error_text': "",
										},
										{ 
											'number': 12,
											'input_str': "25\n11",
											'output_str': "Стрелец",
											'on_error_text': "",
										},
										{ 
											'number': 13,
											'input_str': "25\n2",
											'output_str': "Рыбы",
											'on_error_text': "",
										},

									],
								}, 
								{
									'title': "Угадай число (творческое)",
									'desc': "<p>Написать программу, которая загадывает число от 1 до 10 (включительно) и просит пользователя угадать это число. Когда введенное число совпадет с загаданным, вывести «You win!!!». В этой задаче нет тестовых данных. Оформляйте ввод и вывод для понимания пользователей (если вы отправите программу своему другу, то он должен понять, при запуске, что нужно делать без внешних инструкций)</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 80,
									'gives_base_experience': 80,
									'order_index': 4,
									'only_manual': True,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 21, 45), # edited
									'master_judge': '0',
								}, 

							],
						},
						{
							'title': "09 Практика",
							'short_desc': "Тест + практика",
							'html': "",
							'order_index': 9,
							'_tasks': [
								{
									'title': "Факториал",
									'desc': "<p>Напишите программу, которая вычисляет факториал введенного числа. Гарантируется, что вводимое число находится в диапазоне от 1 до 12 включительно</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 60,
									'gives_base_experience': 60,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 3, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "5",
											'output_str': "5! = 120",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "10",
											'output_str': "10! = 3628800",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "1",
											'output_str': "1! = 1",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "4",
											'output_str': "4! = 24",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "6",
											'output_str': "6! = 720",
											'on_error_text': "",
										},

									],
								}, 
							],
						},
						{
							'title': "10 Массивы",
							'short_desc': "Эталонный код 6 занятия. Одномерные статические массивы",
							'html': "Были рассмотрены:<br>эталонный код задач 6 занятия<br>while<br>вывод однотипных значений<br>одномерные статические массивы",
							'order_index': 10,
							'_tasks': [
								{
									'title': "Массив в обратном порядке",
									'desc': "<p>Создать массив вещественных чисел из 10 элементов. Заполнить значениями от пользователя. Вывести через запятую в обратном порядке</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 60,
									'gives_base_experience': 60,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 27, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "1.5\n2\n3.5\n4\n5.5\n6\n7.5\n8\n9.5\n10",
											'output_str': "10, 9.5, 8, 7.5, 6, 5.5, 4, 3.5, 2, 1.5",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "9\n8\n7\n6\n5\n4\n3\n2\n1\n0",
											'output_str': "0, 1, 2, 3, 4, 5, 6, 7, 8, 9",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "0\n3\n2\n0\n12\n-9\n9.23\n2\n-2.3\n0",
											'output_str': "0, -2.3, 2, 9.23, -9, 12, 0, 2, 3, 0",
											'on_error_text': "",
										},

									],
								}, 

							],
						},
						{
							'title': "11 Практика",
							'short_desc': "Эталонный код 7 и8 занятий. Ввод в массив. Сборка проекта в Visual Studio",
							'html': "Были рассмотрены:<br>эталонный код задач 7 занятия<br>эталонный код задач 8 занятия<br>ввод в массив<br>сниппеты в Visual Studio<br>сборка решения в исполняемый exe файл (для запуска на ПК без VS)",
							'order_index': 11,
							'_tasks': [
								{
									'title': "Разница зарплат",
									'desc': "<p>В отделе работают 3 сотрудника, которые получают заработную плату в рублях. Требуется определить: на сколько зарплата самого высокооплачиваемого из них отличается от самого низкооплачиваемого.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 50,
									'gives_base_experience': 50,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 27, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "35000\n28000\n25000",
											'output_str': "10000",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "10000\n14000\n16000",
											'output_str': "6000",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "1000\n10000\n200",
											'output_str': "9800",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "20000\n10000\n90000",
											'output_str': "80000",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "74000\n90000\n20000",
											'output_str': "70000",
											'on_error_text': "",
										},

									],
								}, 
								{
									'title': "Преобразователь величин",
									'desc': "<p>Написать программу, которая преобразует величины:<br>•	температуры - из градусов Цельсия в градусы по Фаренгейту (и наоборот)<br>•	скорости - км/ч в м/с (и наоборот)<br>Пользователь выбирает сначала что он хочет перевести (температура, скорость), а затем направление перевода (км/ч->м/с или м/с ->км/ч). Программа предлагает ввести значение и считает результат.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 35,
									'gives_base_experience': 35,
									'order_index': 2,
									'only_manual': True,
									'can_be_uploaded_until': datetime.datetime(2017, 11, 27, 18, 45),
									'master_judge': '0',
								}, 
							],
						},
						{
							'title': "12 Строки",
							'short_desc': "Эталонный код 9 занятия. Массив символов. String. Срезы строк. Сравнение строк. Преобразование в число",
							'html': "Были рассмотрены:<br>эталонный код задач 9 занятия<br>строки (массив char)<br>ввод строки, сравнение строк<br>строки (string), срезы",
							'order_index': 12,
							'_tasks': [
								{
									'title': "Возраст",
									'desc': "<p>Пользователь вводит дату рождения в формате «дд.мм.гггг». Программа вычисляет его возраст и выводит с правильным склонением. Пример вывода взят при вычислении возраста на 1 января 2015.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 85,
									'gives_base_experience': 85,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 3,
									'only_manual': True,
									'_pairs': [
										{
											'number': 1,
											'input_str': "28.12.1993",
											'output_str': "Вам 22 года",
											'on_error_text': "",
										}, 
										{
											'number': 2,
											'input_str': "03.09.2000",
											'output_str': "Вам 15 лет",
											'on_error_text': "",
										}, 
										{
											'number': 3,
											'input_str': "03.08.2037",
											'output_str': "Вы еще не родились",
											'on_error_text': "Если дата рождения указана в будущем, то нужно вывести сообщение как в примере",
										}, 
										{
											'number': 4,
											'input_str': "12.01.2010",
											'output_str': "Вам 5 лет",
											'on_error_text': "",
										}, 
										{
											'number': 5,
											'input_str': "01.01.2015",
											'output_str': "Вам 0 лет",
											'on_error_text': "Человеку 0 лет, если он недавно родился",
										}, 
									],
								}, 
								{
									'title': "Авторизация",
									'desc': "Написать программу для авторизации. Пользователь вводит логин и пароль. Если данные совпадают с данными о пользователе в программе, то выводится \"Добро пожаловать, %username%\"(вместо %username% вставить логин пользователя), если данные не подходят, то вывести \"Доступ запрещен\" и завершить программу. Программа должна обрезать лишние пробелы, если пользователь случайно нажмет пробел до или после ввода логина или пароля. Данные о пользователях:<br><ul><li>логин: «admin», пароль: «f1a2c3»</li><li>логин: «masha1993», пароль: «parol»</li><li>логин: «serg», пароль: «123123»</li></ul>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 85,
									'gives_base_experience': 85,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									# 'only_manual': True,
									'_pairs': [
										{
											'number': 1,
											'input_str': "admin\nf1a2c3",
											'output_str': "Добро пожаловать, admin",
											'on_error_text': "",
										}, 
										{
											'number': 2,
											'input_str': "serg\n123",
											'output_str': "Доступ запрещен",
											'on_error_text': "",
										}, 
										{
											'number': 3,
											'input_str': "masha1993\nparol",
											'output_str': "Добро пожаловать, masha1993",
											'on_error_text': "",
										}, 
										{
											'number': 4,
											'input_str': "serg\n123123",
											'output_str': "Добро пожаловать, serg",
											'on_error_text': "",
										}, 
										{
											'number': 5,
											'input_str': "admin  \n f1a2c3",
											'output_str': "Добро пожаловать, admin",
											'on_error_text': "Необходимо обрезать пробелы",
										}, 

									],
									'_conditions': [
										{
											'title': "Реализуйте ввод пароля c отображением звездочек вместо вводимых символов",
											'short_desc': "+5 баллов",
											'external_code_id': 0,
											'gives_coins': 50,
											'gives_base_experience': 50,
										},
									],
								}, 
								{
									'title': "Только уникальные символы",
									'desc': "<p>Напишите программу, которая получает на вход строку и заменяет все повторяющиеся символы (кроме первого) символом *, после чего выводит получившуюся строку на экран.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 75,
									'gives_base_experience': 75,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "2233300102038",
											'output_str': "2*3**0*1****8",
											'on_error_text': "",
										}, 
										{
											'number': 2,
											'input_str': "Мама мыла раму...",
											'output_str': "Мам* *ыл**р**у.**",
											'on_error_text': "",
										}, 
										{
											'number': 3,
											'input_str': "000",
											'output_str': "0**",
											'on_error_text': "",
										}, 
										{
											'number': 4,
											'input_str': "q",
											'output_str': "q",
											'on_error_text': "Строка может состоять из одного символа",
										}, 
									],
								}, 
							],
						},
						{
							'title': "13 Практика",
							'short_desc': "",
							'html': "",
							'order_index': 13,
							'_tasks': [
								{
									'title': "Сумма чека",
									'desc': "<p>В программу заложены массивы с названиями товаров и их стоимостью за штуку в рублях:<table><tr><th>Массив 1</th><th>Массив 2</th></tr><tr><td>апельсин</td><td>15</td></tr><tr><td>банан</td><td>25</td></tr><tr><td>кокос</td><td>12</td></tr><tr><td>ананас</td><td>30</td></tr><tr><td>персик</td><td>18</td></tr></table>Необходимо запросить кол-во купленных товаров с клавиатуры и вывести на экран данные для печати чека. Если кол-во штук данного товара равно 0, то в чеке этот товар не печатать.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 70,
									'gives_base_experience': 70,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "1\n5\n0\n2\n1",
											'output_str': "апельсин x 1 = 15 руб.\nбанан x 5 = 125 руб.\nананас x 2 = 60 руб.\nперсик x 1 = 18 руб.\nИтого: 218 руб.",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "0\n5\n0\n0\n2",
											'output_str': "банан x 5 = 125 руб.\nперсик x 2 = 36 руб.\nИтого: 161 руб.",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "0\n0\n0\n0\n0",
											'output_str': "Итого: 0 руб.",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "2\n3\n4\n5\n6",
											'output_str': "апельсин x 2 = 30 руб.\nбанан x 3 = 75 руб.\nкокос x 4 = 48 руб.\nананас x 5 = 150 руб.\nперсик x 6 = 108 руб.\nИтого: 411 руб.",
											'on_error_text': "",
										},

									],
								},
								{
									'title': "Маска",
									'desc': "<p>Напишите программу, которая получает на вход два массива (по 5 элементов), один из которых состоит из значений true или false, а второй из целых чисел. Все значения элементов второго массива, индекс которых равен индексу элементов со значением false первого массива, обнулить. Вывести получившийся массив на экран.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 70,
									'gives_base_experience': 70,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "true\ntrue\nfalse\nfalse\ntrue\n9\n0\n-1\n3\n4",
											'output_str': "9\n0\n0\n0\n4",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "false\ntrue\ntrue\nfalse\nfalse\n13\n2\n45\n-1\n-34",
											'output_str': "0\n2\n45\n0\n0",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "false\nfalse\nfalse\nfalse\nfalse\n5\n5\n5\n5\n5",
											'output_str': "0\n0\n0\n0\n0",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "true\ntrue\ntrue\ntrue\ntrue\n5\n5\n5\n5\n5",
											'output_str': "5\n5\n5\n5\n5",
											'on_error_text': "",
										},
									],
								},
								{
									'title': "Замена окончаний",
									'desc': "<p>Написать программу, которая во вводимом с клавиатуры тексте заменит все окончания слов «ть» на «ся» и выведет результат на экран.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 85,
									'gives_base_experience': 85,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 3,
									'_pairs': [
										{
											'number': 1,
											'input_str': "сидеть",
											'output_str': "сидеся",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "молчать",
											'output_str': "молчася",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "учиться",
											'output_str': "учиться",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "тьтьть",
											'output_str': "тьться",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "сидеть молчать",
											'output_str': "сидеся молчася",
											'on_error_text': "Текст может состоять из нескольких слов",
										},
										{
											'number': 6,
											'input_str': "учиться молчать",
											'output_str': "учиться молчася",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "14 Практика",
							'short_desc': "",
							'html': "",
							'order_index': 14,
							'_tasks': [
								{
									'title': "Младший и старший сотрудник",
									'desc': "<p>В программе хранятся данные о сотрудниках отдела. Необходимо вывести фамилию, имя и отчество самого старшего и самого младшего сотрудника. На первой строке выводится ФИО старшего, на второй - самого младшего. При расчете возраста использовать дату 10.11.2017. Если сотрудники родились в один день, то вывести первого в алфавитном порядке. Таблица с данными:<table><tr><th>№</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Дата рождения</th><th>Дата приема на работу</th></tr><tr><td>1</td><td>Васильков</td><td>Петр</td><td>Григорьевич</td><td>02.05.1980</td><td>02.05.2001</td></tr><tr><td>2</td><td>Гостев</td><td>Дмитрий</td><td>Александрович</td><td>15.11.1986</td><td>31.04.1999</td></tr><tr><td>3</td><td>Лузинов</td><td>Александр</td><td>Иванович</td><td>13.01.1992</td><td>05.12.2010</td></tr><tr><td>4</td><td>Муравьева</td><td>Светлана</td><td>Петровна</td><td>25.12.1974</td><td>25.05.1995</td></tr><tr><td>5</td><td>Шумилин</td><td>Сергей</td><td>Владимирович</td><td>30.04.1989</td><td>18.09.2005</td></tr></table></p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 105,
									'gives_base_experience': 105,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "",
											'output_str': "Муравьева Светлана Петровна\nЛузинов Александр Иванович",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "15 Практика",
							'short_desc': "Занятие с Ангелиной Сергеевной",
							'html': "",
							'order_index': 15,
							'_tasks': [
								{
									'title': "Нечетные числа в диапазоне",
									'desc': "<p>Напишите программу, которая суммирует все нечетные целые числа в диапазоне, который введет пользователь с клавиатуры.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 70,
									'gives_base_experience': 70,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "1\n11",
											'output_str': "1 3 5 7 9 11\nСумма нечетных чисел в диапазоне от 1 до 11 = 36",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "2\n12",
											'output_str': "3 5 7 9 11\nСумма нечетных чисел в диапазоне от 2 до 12 = 35",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "0\n0",
											'output_str': "\nСумма нечетных чисел в диапазоне от 0 до 0 = 0",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "4\n6",
											'output_str': "5\nСумма нечетных чисел в диапазоне от 4 до 6 = 5",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "17 Хранение данных",
							'short_desc': "Диапазоны значений. Двоичное кодирование. Хранение символов",
							'html': "Были рассмотрены:<br>мини проект<br>хранение данных, диапазоны значений<br>хранение символов",
							'order_index': 17,
							'_tasks': [
								{
									'title': "Срезы",
									'desc': "<p>Выполните разбор строки, согласно приведенным правилам.<strong><p>Входные данные</p></strong>Дана строка.<strong><p>Выходные данные</p></strong>Сначала выведите третий символ этой строки.<br>Во второй строке выведите предпоследний символ этой строки.<br>В третьей строке выведите первые пять символов этой строки.<br>В четвертой строке выведите всю строку, кроме последних двух символов.<br>В пятой строке выведите все символы с четными индексами (индексация начинается с 0, поэтому символы выводятся, начиная с первого).<br>В шестой строке выведите все символы с нечетными индексами, то есть, начиная со второго символа строки.<br>В седьмой строке выведите все символы в обратном порядке.<br>В восьмой строке выведите все символы строки через один в обратном порядке, начиная с последнего.<br>В девятой строке выведите длину данной строки.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 70,
									'gives_base_experience': 70,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 19, 15),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "Abrakadabra",
											'output_str': "r\nr\nAbrak\nAbrakadab\nArkdba\nbaaar\narbadakarbA\nabdkrA\n11",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "Megatron",
											'output_str': "g\no\nMegat\nMegatr\nMgto\nearn\nnortageM\nnrae\n8",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "20 Практика",
							'short_desc': "",
							'html': "",
							'order_index': 20,
							'_tasks': [
								{
									'title': "Коробка и дверь",
									'desc': "<p>Имеется коробка со сторонами:  АхВхС.  Определить пройдёт ли она в дверь с размерами МхК.Вводятся по порядку: A, B, C, M, K</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 105,
									'gives_base_experience': 105,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "8.5\n4\n3.2\n5\n6.5",
											'output_str': "Yes",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "4.5\n18\n6\n5\n5",
											'output_str': "No",
										},
										{
											'number': 3,
											'input_str': "2\n4\n6\n2.5\n4.5",
											'output_str': "Yes",
										},
										{
											'number': 4,
											'input_str': "2\n4\n6\n4.5\n2.5",
											'output_str': "Yes",
										},
										{
											'number': 5,
											'input_str': "4\n2\n6\n2.5\n4.5",
											'output_str': "Yes",
										},
										{
											'number': 6,
											'input_str': "4\n2\n6\n4.5\n2.5",
											'output_str': "Yes",
										},
										{
											'number': 7,
											'input_str': "6\n2\n4\n2.5\n4.5",
											'output_str': "Yes",
										},
										{
											'number': 8,
											'input_str': "6\n2\n4\n4.5\n2.5",
											'output_str': "Yes",
										},
										{
											'number': 9,
											'input_str': "2\n4\n6\n1.5\n4.5",
											'output_str': "No",
										},
										{
											'number': 10,
											'input_str': "2\n4\n6\n4.5\n1.5",
											'output_str': "No",
										},
										{
											'number': 11,
											'input_str': "4\n2\n6\n1.5\n4.5",
											'output_str': "No",
										},
										{
											'number': 12,
											'input_str': "4\n2\n6\n4.5\n1.5",
											'output_str': "No",
										},
										{
											'number': 13,
											'input_str': "6\n2\n4\n1.5\n4.5",
											'output_str': "No",
										},
										{
											'number': 14,
											'input_str': "6\n2\n4\n4.5\n1.5",
											'output_str': "No",
										},
										{
											'number': 15,
											'input_str': "2\n2\n2\n1\n5",
											'output_str': "No",
										},
									],
								},
								{
									'title': "Размен денег",
									'desc': "<p>Известна денежная сумма. Разменять её купюрами 500, 100, 10 и монетой 2 руб., если это возможно.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 90,
									'gives_base_experience': 90,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "1234",
											'output_str': "500+500+100+100+10+10+10+2+2",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "473",
											'output_str': "Error",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "1452",
											'output_str': "500+500+100+100+100+100+10+10+10+10+10+2",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "0",
											'output_str': "Error",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "24 Практика",
							'short_desc': "",
							'html': "",
							'order_index': 24,
							'_tasks': [
								{
									'title': "Обратный порядок слов",
									'desc': "<p>Изменить порядок слов в строке на обратный</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 105,
									'gives_base_experience': 105,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "мама мыла красивую раму",
											'output_str': "раму красивую мыла мама",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "совсем не обязательно",
											'output_str': "обязательно не совсем",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "тип топ",
											'output_str': "топ тип",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "А могли бы и не напоминать",
											'output_str': "напоминать не и бы могли А",
											'on_error_text': "",
										},
									],
								},
								{
									'title': "Три по три",
									'desc': "<p>Дана строка. Напечатать первые 3 буквы 3 раза</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 70,
									'gives_base_experience': 70,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "Java",
											'output_str': "JavJavJav",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "Шоколад",
											'output_str': "ШокШокШок",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "Bor",
											'output_str': "BorBorBor",
											'on_error_text': "",
										},
									],
								},
								{
									'title': "Количество гласных",
									'desc': "<p>Дана строка. Посчитать количество гласных букв</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 85,
									'gives_base_experience': 85,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 18, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "А если в нашу занесет сторонку",
											'output_str': "11",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "Вдруг в огороде расцвела",
											'output_str': "8",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "йцукенгшщзхъфывапролджэячсмитьбю",
											'output_str': "9",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ",
											'output_str': "18",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "26 Функции",
							'short_desc': "Функции, процедуры, аргументы функций",
							'html': "",
							'order_index': 26,
							'_tasks': [
								{
									'title': "Функция для склонения",
									'desc': "<p>Вводятся 3 числа со значениями игровой валюты: монет, кристаллов, рублей. Необходимо вывести их количество и слова с правильным склонением. Если конкретное значение равно 0, то не выводить это значение. Если все значения равны 0, то вывести \"У Вас пока ничего нет\". Для реализации использовать функции.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 130,
									'gives_base_experience': 130,
									'order_index': 1,
									'code_pairs_count': 4,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 25, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "2530\n15\n100",
											'output_str': "У Вас есть 2530 монет, 15 кристаллов и 100 рублей",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "1321\n34\n0",
											'output_str': "У Вас есть 1321 монета и 34 кристалла",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "0\n20\n0",
											'output_str': "У Вас есть 20 кристаллов",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "0\n0\n0",
											'output_str': "У Вас пока ничего нет",
											'on_error_text': "",
										},
										{
											'number': 5,
											'input_str': "1\n1\n1",
											'output_str': "У Вас есть 1 монета, 1 кристалл и 1 рубль",
											'on_error_text': "",
										},
										{
											'number': 6,
											'input_str': "11\n12\n13",
											'output_str': "У Вас есть 11 монет, 12 кристаллов и 13 рублей",
											'on_error_text': "",
										},
										{
											'number': 7,
											'input_str': "22\n32\n42",
											'output_str': "У Вас есть 22 монеты, 32 кристалла и 42 рубля",
											'on_error_text': "",
										},
										{
											'number': 8,
											'input_str': "22\n11\n0",
											'output_str': "У Вас есть 22 монеты и 11 кристаллов",
											'on_error_text': "",
										},
										{
											'number': 9,
											'input_str': "22\n0\n11",
											'output_str': "У Вас есть 22 монеты и 11 рублей",
											'on_error_text': "",
										},
										{
											'number': 10,
											'input_str': "0\n0\n10",
											'output_str': "У Вас есть 10 рублей",
											'on_error_text': "",
										},
										{
											'number': 11,
											'input_str': "20\n0\n0",
											'output_str': "У Вас есть 20 монет",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "27 Функции",
							'short_desc': "Функции, процедуры, аргументы функций",
							'html': "",
							'order_index': 27,
							'_tasks': [
								{
									'title': "Математические функции",
									'desc': "<p>Написать 3 функции для сложения, вычитания и умножения 2х чисел. Запросить 2 числа и вывести на экран сначала результат функции сложения, затем вычитания, затем умножения</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 85,
									'gives_base_experience': 85,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 28, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "88\n14",
											'output_str': "102\n74\n1232",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "13\n-4",
											'output_str': "9\n17\n-52",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "10\n3.5",
											'output_str': "13.5\n6.5\n35",
											'on_error_text': "Необходимо обрабатывать вещественные числа",
										},
									],
								},
							],
						},
						{
							'title': "29 Практика, функции",
							'short_desc': "Функции, процедуры, аргументы функций",
							'html': "",
							'order_index': 29,
							'_tasks': [
								{
									'title': "Функция нахождения минимального из 3 чисел",
									'desc': "<p>Написать функцию для нахождения минимального значения из 3 чисел. Для демонстрации работы функции необходимо считать 3 числа и вывести результат на экран.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 40,
									'gives_base_experience': 40,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 25, 18, 45),
									'master_judge': '0',
									'default_value': "#include <iostream>\nusing namespace std;\n\ndouble min_val(double a, double b, double c) {\n\tdouble min;\n\t// пишите код здесь\n\treturn min;\n}\n\nint main() {\n\tdouble a, b, c;\n\tcin >> a >> b >> c;\n\tcout << min_val(a, b, c);\n\treturn 0;\n}",
									'cursor_pos_row': 5,
									'cursor_pos_col': 20,
									'_pairs': [
										{
											'number': 1,
											'input_str': "95\n5\n8",
											'output_str': "5",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "456\n-3\n89",
											'output_str': "-3",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "1.5\n3\n4",
											'output_str': "1.5",
											'on_error_text': "Необходимо обрабатывать вещественные числа",
										},
									],
								},
								{
									'title': "Функции поиска минимального и максимального элементов массива",
									'desc': "<p>Написать 2 функции для поиска минимального и максимального значения в массиве. Каждая функция должна принимать первым аргументом массив целых чисел и 2 аргументом - количество элементов в нем. Для демонстрации работы функций необходимо принять 10 чисел из ввода и вывести сначала результат функции поиска минимального значения, а затем - максимального.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 50,
									'gives_base_experience': 50,
									'order_index': 2,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 25, 18, 45),
									'master_judge': '0',
									'code_pairs_count': 1,
									'_pairs': [
										{
											'number': 1,
											'input_str': "4\n814\n5\n-10\n89\n564\n-4\n45\n65\n5",
											'output_str': "-10\n814",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "5\n8\n15\n-3\n25\n30\n40\n45\n50\n55",
											'output_str': "-3, 55",
											'on_error_text': "",
										},
									],
								},
								{
									'title': "Процедура для вывода массива",
									'desc': "<p>Написать процедуру для вывода значений массива целых чисел через запятую. Для демонстрации вывести массив 5 вводимых с клавиатуры чисел.</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 40,
									'gives_base_experience': 40,
									'order_index': 3,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 25, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "564\n-4\n45\n65\n5",
											'output_str': "564, -4, 45, 65, 5",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "4\n814\n5\n-10\n89",
											'output_str': "4, 814, 5, -10, 89",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "4\n5\n6\n78\n9",
											'output_str': "4, 5, 6, 78, 9",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "30 Олимпиданые задачи",
							'short_desc': "Пример олимпиадной задачи",
							'html': "",
							'order_index': 30,
							'_tasks': [
								{
									'title': "Свойства чисел",
									'desc': "<p>Гарри Поттер на досуге занимается исследованием свойств чисел. Однажды в старом заклинании он увидел число 54765287694769587387647836748 и захотел узнать, а делится ли оно на 3? Напишите программу, помогающую Гарри решить эту проблему для любого N.</p><br><p><strong>ВХОДНЫЕ ДАННЫЕ</strong></p><p>Строка ввода содержит одно целое число N, (0&lt;N&lt;10<sup>1000</sup>).</p><br><p><strong>ВЫХОДНЫЕ ДАННЫЕ</strong></p><p>Строка вывода содержит  слово «Yes», если число N делится на 3, или остаток от деления N на 3</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 40,
									'gives_base_experience': 40,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 28, 18, 45),
									'master_judge': '0',
									'_pairs': [
										{
											'number': 1,
											'input_str': "54765287694769587387647836748",
											'output_str': "2",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "3",
											'output_str': "Yes",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "1",
											'output_str': "1",
											'on_error_text': "",
										},
									],
								},
							],
						},
						{
							'title': "31 Функции практика",
							'short_desc': "",
							'html': "",
							'order_index': 31,
							'_tasks': [
								{
									'title': "Функция для получения возраста из даты",
									'desc': "<p>Написать функцию calculate_age, которая принимает дату рождения в формате \"ДД.ММ.ГГГГ\" (гарантируется существование даты). Если день или месяц меньше 10, то дата может быть записана в формате \"Д.М.ГГГГ\" (смотрите 2й пример). Вернуть возраст и склоненное слово \"лет\". Для вычисления возраста использовать дату \"22.12.2017\". Если дата указана в будущем, то вернуть \"Вы еще не родились\"</p>",
									'programming_language': cpp_lang,
									'task_type': 'C',
									'difficulty': '3',
									'gives_coins': 60,
									'gives_base_experience': 60,
									'order_index': 1,
									'can_be_uploaded_until': datetime.datetime(2017, 12, 30, 18, 45),
									'master_judge': '0',
									'default_value': "#include <iostream>\n#include <string>\nusing namespace std;\n\nstring calculate_age(string birth_date) {\n\t\n\t// пишите код здесь\n\t\n\treturn to_string(age) + \" лет\";\n}\n\nint main() {\n\tstring b_date;\n\tcin >> b_date;\n\tcout << calculate_age(b_date);\n\treturn 0;\n}",
									'_pairs': [
										{
											'number': 1,
											'input_str': "25.12.2014",
											'output_str': "2 года",
											'on_error_text': "",
										},
										{
											'number': 2,
											'input_str': "13.8.2005",
											'output_str': "12 лет",
											'on_error_text': "",
										},
										{
											'number': 3,
											'input_str': "3.8.1984",
											'output_str': "33 года",
											'on_error_text': "",
										},
										{
											'number': 4,
											'input_str': "31.12.2017",
											'output_str': "Вы еще не родились",
											'on_error_text': "",
										},
									],
								},
							],
						},
					],
				},
			],
			'_references': [
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main()\n{\n\tfloat a, b, c;\n\tcin >> a >> b >> c;\n\tcout << a + b + c;\n\treturn 0;\n}",
					"lesson": 2,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main()\n{\n\tfloat a, b;\n\tcin >> a >> b;\n\tcout << \"Сумма: \" << a + b << endl;\n\tcout << \"Разность: \" << a - b << endl;\n\tcout << \"Произведение: \" << a * b << endl;\n\treturn 0;\n}",
					"lesson": 2,
				},
				{
					"task": 3,
					"code": "#include <iostream>\nusing namespace std;\n\nint main()\n{\n\tint distance;\n\tcin >> distance;\n\tcout << distance / 0.305 << endl;\n\treturn 0;\n}",
					"lesson": 2,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint number;\n\tcin >> number;\n\tif (number % 2 == 0) \n\t\tcout << \"a) yes\" << endl;\n\telse\n\t\tcout << \"a) no\" << endl;\n\tif (number % 10 == 7) \n\t\tcout << \"b) yes\" << endl;\n\telse \n\t\tcout << \"b) no\" << endl;\n\treturn 0;\n}",
					"lesson": 3,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint number1, number2, number3, max, min;\n\tcin >> number1 >> number2 >> number3;\n\tif (number1 >= number2 && number1 >= number3)\n\t\tmax = number1;\n\telse if (number2 >= number1 && number2 >= number3)\n\t\tmax = number2;\n\telse \n\t\tmax = number3;\n\tif (number1 <= number2 && number1 <= number3)\n\t\tmin = number1;\n\telse if (number2 <= number1 && number2 <= number3)\n\t\tmin = number2;\n\telse\n\t\tmin = number3;\n\tcout << \"The largest number is \" << max << endl;\n\tcout << \"The smallest number is \" << min << endl;\n\treturn 0;\n}",
					"lesson": 3,
				},
				{
					"task": 3,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint km_dist, ft_dist, ft1_dist;\n\tcin >> km_dist >> ft_dist;\n\tft1_dist = km_dist * 1000 / 0.305;\n\tif (ft1_dist < ft_dist)\n\t\tcout << km_dist << \" km<\" << ft_dist << \" ft\" << endl;\n\telse if (ft1_dist > ft_dist)\n\t\tcout << km_dist << \" km>\" << ft_dist << \" ft\" << endl;\n\telse \n\t\tcout << km_dist << \" km=\" << ft_dist << \" ft\" << endl;\n\treturn 0;\n}",
					"lesson": 3,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint angle1, angle2, angle3;\n\tcin >> angle1 >> angle2 >> angle3;\n\tif (angle1 + angle2 + angle3 == 180 && angle1 > 0 && angle2 > 0 && angle3 > 0)\n\t\tcout << \"Yes\" << endl;\n\telse\n\t\tcout << \"No\" << endl;\n\treturn 0;\n}\n",
					"lesson": 4,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint hour;\n\tcin >> hour;\n\tif (hour >= 0 && hour < 4)\n\t\tcout << \"Доброй ночи\" << endl;\n\telse if (hour >= 4 && hour < 12)\n\t\tcout << \"Доброе утро\" << endl;\n\telse if (hour >= 12 && hour < 17)\n\t\tcout << \"Добрый день\" << endl;\n\telse if (hour >= 17 && hour < 24)\n\t\tcout << \"Добрый вечер\" << endl;\n\telse\n\t\tcout << \"Ошибка\" << endl;\n\treturn 0;\n}\n",
					"lesson": 4,
				},
				{
					"task": 3,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint rubles;\n\tcin >> rubles;\n\tif (rubles % 100 >= 11 && rubles % 100 < 15)\n\t\tcout << rubles << \" рублей\" << endl;\n\telse if (rubles % 10 == 1)\n\t\tcout << rubles << \" рубль\" << endl;\n\telse if (rubles % 10 >= 2 && rubles % 10 < 5)\n\t\tcout << rubles << \" рубля\" << endl;\n\telse \n\t\tcout << rubles << \" рублей\" << endl;\n\treturn 0;\n}",
					"lesson": 4,
				},
				{
					"task": 4,
					"code": "#include <iostream>\n#include <cmath>\nusing namespace std;\n\nint main() {\n\tfloat a_x, a_y, b_x, b_y, x, y;\n\tcin >> a_x >> a_y >> b_x >> b_y;\n\tx = a_x - b_x;\n\ty = a_y - b_y;\n\tcout << sqrt(x*x + y*y);\n\treturn 0;\n}\n",
					"lesson": 4,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint number, n1, n2, n3;\n\tcin >> number;\n\tn1 = number / 100;\n\tn2 = (number / 10) % 10;\n\tn3 = number % 10;\n\tif (n1 == 1) \n\t\tcout << \"Сто\";\n\telse if (n1 == 2)\n\t\tcout << \"Двести\";\n\telse if (n1 == 3)\n\t\tcout << \"Триста\";\n\telse if (n1 == 4)\n\t\tcout << \"Четыреста\";\n\telse if (n1 == 5)\n\t\tcout << \"Пятьсот\";\n\telse if (n1 == 6)\n\t\tcout << \"Шестьсот\";\n\telse if (n1 == 7)\n\t\tcout << \"Семьсот\";\n\telse if (n1 == 8)\n\t\tcout << \"Восемьсот\";\n\telse if (n1 == 9)\n\t\tcout << \"Девятьсот\";\n\t\n\tif (number % 100 == 11)\n\t\tcout << \" одиннадцать\";\n\telse if (number % 100 == 12)\n\t\tcout << \" двенадцать\";\n\telse if (number % 100 == 13)\n\t\tcout << \" тринадцать\";\n\telse if (number % 100 == 14)\n\t\tcout << \" четырнадцать\";\n\telse if (number % 100 == 15)\n\t\tcout << \" пятнадцать\";\n\telse if (number % 100 == 16)\n\t\tcout << \" шестнадцать\";\n\telse if (number % 100 == 17)\n\t\tcout << \" семнадцать\";\n\telse if (number % 100 == 18)\n\t\tcout << \" восемнадцать\";\n\telse if (number % 100 == 19)\n\t\tcout << \" девятнадцать\";\n\n\telse if (n2 == 2)\n\t\tcout << \" двадцать\";\n\telse if (n2 == 3)\n\t\tcout << \" тридцать\";\n\telse if (n2 == 4)\n\t\tcout << \" сорок\";\n\telse if (n2 == 5)\n\t\tcout << \" пятьдесят\";\n\telse if (n2 == 6)\n\t\tcout << \" шестьдесят\";\n\telse if (n2 == 7)\n\t\tcout << \" семьдесят\";\n\telse if (n2 == 8)\n\t\tcout << \" восемьдесят\";\n\telse if (n2 == 9)\n\t\tcout << \" девяносто\";\n\n\tif (n3 == 1 && n2 != 1)\n\t\tcout << \" один\";\n\telse if (n3 == 2 && n2 != 1)\n\t\tcout << \" два\";\n\telse if (n3 == 3 && n2 != 1)\n\t\tcout << \" три\";\n\telse if (n3 == 4 && n2 != 1)\n\t\tcout << \" четыре\";\n\telse if (n3 == 5 && n2 != 1)\n\t\tcout << \" пять\";\n\telse if (n3 == 6 && n2 != 1)\n\t\tcout << \" шесть\";\n\telse if (n3 == 7 && n2 != 1)\n\t\tcout << \" семь\";\n\telse if (n3 == 8 && n2 != 1)\n\t\tcout << \" восемь\";\n\telse if (n3 == 9 && n2 != 1)\n\t\tcout << \" девять\";\n\treturn 0;\n}",
					"lesson": 5,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tfloat number1, number2;\n\tchar operation;\n\tcin >> number1 >> operation >> number2;\n\tif (operation == '+') \n\t\tcout << number1 + number2 << endl;\n\telse if (operation == '-') \n\t\tcout << number1 - number2 << endl;\n\telse if (operation == '*') \n\t\tcout << number1 * number2 << endl;\n\telse if (operation == '/') {\n\t\tif (number2 != 0)\n\t\t\tcout << number1 / number2 << endl;\n\t\telse\n\t\t\tcout << \"Error\" << endl;\n\t}\n}",
					"lesson": 5,
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <cmath>\nusing namespace std;\nint main() {\n\tfloat a, b, c, d;\n\tcin >> a >> b >> c;\n\td = (b * b) - (4 * a * c);\n\tif (d > 0) {\n\t\tcout << (-b + sqrt(d)) / (2 * a) << endl;\n\t\tcout << (-b - sqrt(d)) / (2 * a);\n\t}\n\telse if (d == 0) {\n\t\tcout << (-b + sqrt(d)) / (2 * a);\n\t}\n\telse {\n\t\tcout << \"Нет корней\";\n\t}   \n\treturn 0;\n}",
					"lesson": 6,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint day;\n\tcin >> day;\n\tif (day == 1) { // понедельник\n\t\tcout << \"информатика\" << endl; // 1\n\t\tcout << \"география\" << endl; // 2\n\t\tcout << \"русский язык\" << endl; // 3\n\t\tcout << \"литература\" << endl; // 4\n\t}\n\telse if (day == 2) { // вторник\n\t\tcout << \"история\" << endl; // 1\n\t\tcout << \"физ-ра\" << endl; // 2\n\t\tcout << \"черчение\" << endl; // 3\n\t\tcout << \"ОБЖ\" << endl; // 4\n\t\tcout << \"обществознание\" << endl; // 5\n\t}\n\telse if(day == 3) { // среда\n\t\tcout << \"математика\" << endl; // 1\n\t\tcout << \"информатика\" << endl; // 2\n\t\tcout << \"литература\" << endl; // 3\n\t\tcout << \"биология\" << endl; // 4\n\t\tcout << \"физ-ра\" << endl; // 5\n\t\tcout << \"география\" << endl; // 6\n\t}\n\telse if(day == 4) { // четверг\n\t\tcout << \"русский язык\" << endl; // 1\n\t\tcout << \"ОБЖ\" << endl; // 2\n\t\tcout << \"обществознание\" << endl; // 3\n\t\tcout << \"труд\" << endl; // 4\n\t\tcout << \"труд\" << endl; // 5\n\t}\n\telse if(day == 5) { // пятница\n\t\tcout << \"черчение\" << endl; // 1\n\t\tcout << \"биология\" << endl; // 2\n\t\tcout << \"математика\" << endl; // 3\n\t\tcout << \"труд\" << endl; // 4\n\t\tcout << \"информатика\" << endl; // 5\n\t}\n\telse if(day == 6) { // суббота\n\t\tcout << \"математика\" << endl; // 1\n\t\tcout << \"физ-ра\" << endl; // 2\n\t\tcout << \"русский язык\" << endl; // 3\n\t\tcout << \"история\" << endl; // 4\n\t}\n\telse {\n\t\tcout << \"Дня недели с таким порядковым номером не существует\";\n\t}\n\treturn 0;\n}",
					"lesson": 6,
				},
				{
					"task": 3,
					"code": "#include <iostream>\n#include <cmath>\nusing namespace std;\n\nint main() {\n\tconst float G = 9.81;\n\tconst float PI = 3.14;\n\tfloat angle, speed;\n\tcin >> angle >> speed;\n\tcout << int(speed*speed*sin(2*angle*PI/180)/G);\n\treturn 0;\n}\n",
					"lesson": 6,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint count; \n\tcin >> count;\n\tif (count < 2) \n\t\tcout << \"Error\";\n\telse\n\t\tfor (int i = 2; i <= count; i++)\n\t\t{\n\t\t\tcout << i << \" - \" << i * 20.4 << endl;\n\t\t}\n\treturn 0;\n}",
					"lesson": 7,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint number, sum = 0; \n\tcin >> number;\n\tfor (int i = 1; i < number; i++)\n\t{\n\t\tsum += i;\n\t}\n\tcout << sum;\n\treturn 0;\n}",
					"lesson": 7,
				},
				{
					"task": 3,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint number; \n\tcin >> number;\n\tfor (int i = number; i > 0; i--)\n\t{\n\t\tfor (int j = 0; j < i; j++)\n\t\t{\n\t\t\tcout << '*';\n\t\t}\n\t\tcout << endl;\n\t}\n\treturn 0;\n}",
					"lesson": 7,
				},
				{
					"task": 4,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint number; \n\tcin >> number;\n\tif (number == 0) {\n\t\tcout << 0;\n\t\treturn 0;\n\t}\n\twhile (number > 0) {\n\t\tcout << number % 10;\n\t\tnumber /= 10;\n\t}\n\treturn 0;\n}",
					"lesson": 7,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint action;\n\tfloat value;\n\tcin >> action >> value;\n\tswitch (action)\n\t{\n\t\tcase 1: // дециметр\n\t\t\tvalue /= 10;\n\t\t\tbreak;\n\t\tcase 2: // километр\n\t\t\tvalue *= 1000;\n\t\t\tbreak;\n\t\tcase 3: // метр\n\t\t\tbreak;\n\t\tcase 4: // миллиметр\n\t\t\tvalue /= 1000;\n\t\t\tbreak;\n\t\tcase 5: // сантиметр\n\t\t\tvalue /= 100;\n\t\t\tbreak;\n\t}\n\tcout << value;\n\treturn 0;\n}",
					"lesson": 8,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint day, month;\n\tcin >> day >> month;\n\tday--;\n\tif (day == 0) {\n\t\tmonth--;\n\t\tif (month == 0) {\n\t\t\tmonth = 12;\n\t\t}\n\t\tswitch (month)\n\t\t{\n\t\tcase 1: day = 31; break;\n\t\t\tcase 3: day = 31; break;\n\t\t\tcase 5: day = 31; break;\n\t\t\tcase 7: day = 31; break;\n\t\t\tcase 8: day = 31; break;\n\t\t\tcase 10: day = 31; break;\n\t\t\tcase 12: day = 31; break;\n\t\t\tcase 4: day = 30; break;\n\t\t\tcase 6: day = 30; break;\n\t\t\tcase 9: day = 30; break;\n\t\t\tcase 11: day = 30; break;\n\t\t\tcase 2: day = 28; break;\n\t\t}\n\t}\n\tcout << day << endl;\n\tcout << month;\n\treturn 0;\n}",
					"lesson": 8,
				},
				{
					"task": 3,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint day, month;\n\tcin >> day >> month;\n\tswitch (month)\n\t{\n\t\tcase 1: \n\t\t\tif (day < 20)\n\t\t\t\tcout << \"Козерог\";\n\t\t\telse\n\t\t\t\tcout << \"Водолей\";\n\t\t\tbreak;\n\t\tcase 2: \n\t\t\tif (day < 19)\n\t\t\t\tcout << \"Водолей\";\n\t\t\telse\n\t\t\t\tcout << \"Рыбы\";\n\t\t\tbreak;\n\t\tcase 3: \n\t\t\tif (day < 21)\n\t\t\t\tcout << \"Рыбы\";\n\t\t\telse\n\t\t\t\tcout << \"Овен\";\n\t\t\tbreak;\n\t\tcase 4: \n\t\t\tif (day < 20)\n\t\t\t\tcout << \"Овен\";\n\t\t\telse\n\t\t\t\tcout << \"Телец\";\n\t\t\tbreak;\n\t\tcase 5: \n\t\t\tif (day < 21)\n\t\t\t\tcout << \"Телец\";\n\t\t\telse\n\t\t\t\tcout << \"Близнецы\";\n\t\t\tbreak;\n\t\tcase 6: \n\t\t\tif (day < 22)\n\t\t\t\tcout << \"Близнецы\";\n\t\t\telse\n\t\t\t\tcout << \"Рак\";\n\t\t\tbreak;\n\t\tcase 7: \n\t\t\tif (day < 23)\n\t\t\t\tcout << \"Рак\";\n\t\t\telse\n\t\t\t\tcout << \"Лев\";\n\t\t\tbreak;\n\t\tcase 8: \n\t\t\tif (day < 23)\n\t\t\t\tcout << \"Лев\";\n\t\t\telse\n\t\t\t\tcout << \"Дева\";\n\t\t\tbreak;\n\t\tcase 9: \n\t\t\tif (day < 23)\n\t\t\t\tcout << \"Дева\";\n\t\t\telse\n\t\t\t\tcout << \"Весы\";\n\t\t\tbreak;\n\t\tcase 10: \n\t\t\tif (day < 23)\n\t\t\t\tcout << \"Весы\";\n\t\t\telse\n\t\t\t\tcout << \"Скорпион\";\n\t\t\tbreak;\n\t\tcase 11: \n\t\t\tif (day < 23)\n\t\t\t\tcout << \"Скорпион\";\n\t\t\telse\n\t\t\t\tcout << \"Стрелец\";\n\t\t\tbreak;\n\t\tcase 12: \n\t\t\tif (day < 22)\n\t\t\t\tcout << \"Стрелец\";\n\t\t\telse\n\t\t\t\tcout << \"Козерог\";\n\t\t\tbreak;\n\t}\n\treturn 0;\n}",
					"lesson": 8,
				},
				{
					"task": 4,
					"code": "#include <iostream>\n#include <cstdlib>\n#include <ctime>\nusing namespace std;\n\nint main() {\n\tint a, b;\n\tsrand(time(0));\n\ta = ((rand() % 10) + 1);\n\t\n\tdo {\n\t\tcin >> b;\n\t} while (b != a);\n\tcout << \"+\";\n\treturn 0;\n}",
					"lesson": 8,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint number, res = 1;\n\tcin >> number;\n\tfor (int i = 2; i <= number; i++)\n\t{\n\t\tres *= i;\n\t}\n\tcout << number << \"! = \" << res;\n\treturn 0;\n}",
					"lesson": 9,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tfloat numbers[10];\n\tfor (int i = 0; i < 10; i++) \n\t\tcin >> numbers[i];\n\tfor (int i = 9; i >= 0; i--) {\n\t\tcout << numbers[i];\n\t\tif (i > 0) \n\t\t\tcout << \", \";\n\t}\n\treturn 0;\n}",
					"lesson": 10,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint sallary[3];\n\tcin >> sallary[0] >> sallary[1] >> sallary[2];\n\n\tif (sallary[0] >= sallary[1] && sallary[1] >= sallary[2]) {\n\t\tcout << sallary[0] - sallary[2];\n\t}\n\telse if (sallary[0] >= sallary[2] && sallary[2] >= sallary[1]) {\n\t\tcout << sallary[0] - sallary[1];\n\t}\n\telse if (sallary[1] >= sallary[2] && sallary[2] >= sallary[0]) {\n\t\tcout << sallary[1] - sallary[0];\n\t}\n\telse if (sallary[1] >= sallary[0] && sallary[0] >= sallary[2]) {\n\t\tcout << sallary[1] - sallary[2];\n\t}\n\telse if (sallary[2] >= sallary[1] && sallary[1] >= sallary[0]) {\n\t\tcout << sallary[2] - sallary[0];\n\t}\n\telse if (sallary[2] >= sallary[0] && sallary[0] >= sallary[1]) {\n\t\tcout << sallary[2] - sallary[1];\n\t}\n\treturn 0;\n}",
					"lesson": 11,
					"short_desc": "Вариант 1",
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint temp, min, max;\n\tfor (int i = 0; i < 3; i++) {\n\t\tcin >> temp;\n\t\tif (i == 0) {\n\t\t\tmin = temp;\n\t\t\tmax = temp;\n\t\t}\n\t\tif (temp < min)\n\t\t\tmin = temp;\n\t\tif (temp > max)\n\t\t\tmax = temp;\n\t}\n\tcout << max - min;\n\treturn 0;\n}",
					"lesson": 11,
					"short_desc": "Вариант 2 (масштабируемый)",
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main() {\n\tstring birth_date;\n\tint day, month, year, age, c_day = 1, c_month = 1, c_year = 2017;\n\tcin >> birth_date;\n\tday = stoi(birth_date.substr(0, 2));\n\tmonth = stoi(birth_date.substr(3, 2));\n\tyear = stoi(birth_date.substr(6, 4));\n\tage = c_year - year;\n\tif (c_month < month || (c_month == month && c_day < day))\n\t\tage--;\n\tif (age < 0) \n\t\tcout << \"Вы еще не родились\";\n\telse if (age % 100 >= 11 && age % 100 <= 14) \n\t\tcout << age << \" лет\";\n\telse if (age % 10 == 1) \n\t\tcout << age << \" год\";\n\telse if (age % 10 >= 2 && age % 10 <= 4) \n\t\tcout << age << \" года\";\n\telse \n\t\tcout << age << \" лет\";\n\treturn 0;\n}\n",
					"lesson": 12,
				},
				{
					"task": 2,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main() {\n\tbool is_authenticated = false;\n\tstring login, password;\n\tstring arr_login[3] = { \"admin\", \"masha1993\", \"serg\" };\n\tstring arr_pass[3] = { \"f1a2c3\", \"parol\", \"123123\" };\n\tint index = 0;\n\n\t//cout << \"Введите логин\" << endl;\n\tgetline(cin, login);\n\t//cout << \"Введите пароль\" << endl;\n\tgetline(cin, password);\n\n\t// найти индекс первого не пробельного символа и обрезать логин, начиная с него\n\tfor (int i = 0; i < login.length(); i++) {\n\t\tif (login[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tlogin = login.substr(index);\n\t\n\t// найти индекс последнего не пробельного символа и обрезать логин, начиная с него\n\tfor (int i = login.length() - 1; i >= 0; i--) {\n\t\tif (login[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tlogin = login.substr(0, index + 1);\n\t\n\t// найти индекс первого не пробельного символа и обрезать пароль, начиная с него\n\tfor (int i = 0; i < password.length(); i++) {\n\t\tif (password[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tpassword = password.substr(index);\n\n\t// найти индекс последнего не пробельного символа и обрезать пароль, начиная с него\n\tfor (int i = password.length() - 1; i >= 0; i--) {\n\t\tif (password[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tpassword = password.substr(0, index + 1);\n\n\t// пройти по всем парам логин-пароль и найти совпадение\n\tfor (int i = 0; i < 3; i++) {\n\t\tif (login == arr_login[i] && password == arr_pass[i]) {\n\t\t\tcout << \"Добро пожаловать, \" << login << endl;\n\t\t\tis_authenticated = true;\n\t\t\tbreak;\n\t\t}\n\t}\n\tif (!is_authenticated) {\n\t\tcout << \"Доступ запрещен\" << endl;\n\t}\n\treturn 0;\n}\n",
					"lesson": 12,
					"short_desc": "Обычное решение с обрезанием пробелов",
				},
				{
					"task": 2,
					"code": "#include <iostream>\n#include <string>\n#include <conio.h>\nusing namespace std;\n\nint main() {\n\tbool is_authenticated = false;\n\tstring login, password = \"\";\n\tstring arr_login[3] = { \"admin\", \"masha1993\", \"serg\" };\n\tstring arr_pass[3] = { \"f1a2c3\", \"parol\", \"123123\" };\n\tint index = 0;\n\tchar ch;\n\n\t//cout << \"Введите логин\" << endl;\n\tgetline(cin, login);\n\t//cout << \"Введите пароль\" << endl;\n\tch = _getch();\n\twhile (ch != 13) {\n\t\tpassword.push_back(ch);\n\t\tcout << '*';\n\t\tch = _getch();\n\t}\n\tcout << endl;\n\n\t// найти индекс первого не пробельного символа и обрезать логин, начиная с него\n\tfor (int i = 0; i < login.length(); i++) {\n\t\tif (login[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tlogin = login.substr(index);\n\t\n\t// найти индекс последнего не пробельного символа и обрезать логин, начиная с него\n\tfor (int i = login.length() - 1; i >= 0; i--) {\n\t\tif (login[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tlogin = login.substr(0, index + 1);\n\t\n\t// найти индекс первого не пробельного символа и обрезать пароль, начиная с него\n\tfor (int i = 0; i < password.length(); i++) {\n\t\tif (password[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tpassword = password.substr(index);\n\n\t// найти индекс последнего не пробельного символа и обрезать пароль, начиная с него\n\tfor (int i = password.length() - 1; i >= 0; i--) {\n\t\tif (password[i] != ' ') {\n\t\t\tindex = i;\n\t\t\tbreak;\n\t\t}\n\t}\n\tpassword = password.substr(0, index + 1);\n\n\t// пройти по всем парам логин-пароль и найти совпадение\n\tfor (int i = 0; i < 3; i++) {\n\t\tif (login == arr_login[i] && password == arr_pass[i]) {\n\t\t\tcout << \"Добро пожаловать, \" << login << endl;\n\t\t\tis_authenticated = true;\n\t\t\tbreak;\n\t\t}\n\t}\n\tif (!is_authenticated) {\n\t\tcout << \"Доступ запрещен\" << endl;\n\t}\n\tsystem(\"pause\");\n\treturn 0;\n}\n",
					"lesson": 12,
					"short_desc": "Пароль в виде звездочек",
				},
				{
					"task": 3,
					"code": "#include <iostream>\n#include <string>\n\nusing namespace std;\n\nint main() {\n\tstring a;\n\tgetline(cin, a);\n\n\tfor (int i = 0; i < a.length(); i++ ) {\n\t\tfor (int k = i+1; k < a.length(); k++) {\n\t\t\tif (a[k] == a[i]) {\n\t\t\t\ta[k] = '*';\n\t\t\t}\n\t\t}\n\t}\n\tcout << a << endl;\n\treturn 0;\n}",
					"lesson": 12,
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main()\n{\n\tstring fruits[5] = { \"апельсин\", \"банан\", \"кокос\", \"ананас\", \"персик\" };\n\tunsigned int price[5] = { 15, 25, 12, 30, 18 };\n\tunsigned int quantity[5];\n\tunsigned int sum = 0;\n\n\tfor (int i = 0; i < 5; i++)\n\t\tcin >> quantity[i];\n\n\tfor (int i = 0; i < 5; i++) {\n\t\tif (quantity[i] > 0) {\n\t\t\tcout << fruits[i] << \" x \" << quantity[i] << \" = \" << price[i] * quantity[i] << \" руб.\" << endl;\n\t\t\tsum += price[i] * quantity[i];\n\t\t}\n\t}\n\tcout << \"Итого: \" << sum << \" руб.\" << endl;\n\treturn 0;\n}",
					"lesson": 13,
				},
				{
					"task": 2,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main()\n{\n\tstring masks[5];\n\tint numbers[5];\n\n\tfor (int i = 0; i < 5; i++) \n\t\tcin >> masks[i];\n\tfor (int i = 0; i < 5; i++) \n\t\tcin >> numbers[i];\n\n\tfor (int i = 0; i < 5; i++) {\n\t\tif (masks[i] == \"false\") \n\t\t\tnumbers[i] = 0;\n\t\tcout << numbers[i] << endl;\n\t}\n\t\n\treturn 0;\n}",
					"lesson": 13,
				},
				{
					"task": 3,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main()\n{\n\tstring text;\n\tgetline(cin, text);\n\tfor (int i = 0; i < text.length() + 1; i++)\n\t{\n\t\tif (text[i] == 'т' && text[i + 1] == 'ь' && (text[i + 2] == ' ' || i + 2 == text.length()))\n\t\t{\n\t\t\ttext[i] = 'с';\n\t\t\ttext[i + 1] = 'я';\n\t\t}\n\t}\n\tcout << text << endl;\n\treturn 0;\n}",
					"lesson": 13,
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main()\n{\n\tstring last_names[5] = { \"Васильков\", \"Гостев\", \"Лузинов\", \"Муравьева\", \"Шумилин\" };\n\tstring names[5] = { \"Петр\", \"Дмитрий\", \"Александр\", \"Светлана\", \"Сергей\" };\n\tstring surnames[5] = { \"Григорьевич\", \"Александрович\", \"Иванович\", \"Петровна\", \"Владимирович\" };\n\tstring birth_dates[5] = { \"02.05.1980\", \"15.11.1986\", \"13.01.1992\", \"25.12.1974\", \"30.04.1989\" };\n\tint age = 0, c_day = 10, c_month = 11, c_year = 2017; \n\tint min, max, index_min = 0, index_max = 0, day, month, year;\n\n\tfor (int i = 0; i < 5; i++)\n\t{\n\t\tday = stoi(birth_dates[i].substr(0, 2));\n\t\tmonth = stoi(birth_dates[i].substr(3, 2));\n\t\tyear = stoi(birth_dates[i].substr(6, 4));\n\t\tage = c_year - year;\n\t\tif (c_month < month || (c_month == month && c_day < day))\n\t\t\tage--;\n\n\t\tif (i == 0) {\n\t\t\tmin = age;\n\t\t\tmax = age;\n\t\t}\n\t\tif (age < min)\n\t\t{\n\t\t\tmin = age;\n\t\t\tindex_min = i;\n\t\t}\n\t\tif (age > max)\n\t\t{\n\t\t\tmax = age;\n\t\t\tindex_max = i;\n\t\t}\n\t}\n\n\tcout << last_names[index_max] << \" \" << names[index_max] << \" \" << surnames[index_max] << endl;\n\tcout << last_names[index_min] << \" \" << names[index_min] << \" \" << surnames[index_min] << endl;\n\treturn 0;\n}",
					"lesson": 14,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main()\n{\n\tint begin, end, sum = 0;\n\tcin >> begin >> end;\n\n\tfor (int i = begin; i <= end; i++)\n\t{\n\t\tif (i % 2 == 1)\n\t\t{\n\t\t\tcout << i << \" \";\n\t\t\tsum += i;\n\t\t}\n\t}\n\n\tcout << endl << \"Сумма нечетных чисел в диапазоне от \" << begin << \" до \" << end << \" = \" << sum << endl;\n\treturn 0;\n}",
					"lesson": 15,
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main()\n{\n\tstring text;\n\tgetline(cin, text);\n\n\tcout << text.substr(2, 1) << endl;\n\tcout << text.substr(text.length() - 2, 1) << endl;\n\tcout << text.substr(0, 5) << endl;\n\tcout << text.substr(0, text.length() - 2) << endl;\n\n\tfor (int i = 0; i < text.length(); i += 2) \n\t\tcout << text.substr(i, 1);\n\tcout << endl;\n\n\tfor (int i = 1; i < text.length(); i += 2) \n\t\tcout << text.substr(i, 1);\n\tcout << endl;\n\n\tfor (int i = text.length() - 1; i > -1; i--) \n\t\tcout << text.substr(i, 1);\n\tcout << endl;\n\n\tfor (int i = text.length() - 1; i > -1; i -= 2) \n\t\tcout << text.substr(i, 1);\n\tcout << endl;\n\n\tcout << text.length() << endl;\n\treturn 0;\n}",
					"lesson": 17,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main()\n{\n\tdouble A, B, C, M, K;\n\tcin >> A >> B >> C >> M >> K;\n\n\tif (A >= B && A >= C && (B <= M && C <= K || B <= K && C <= M)) {\n\t\tcout << \"Yes\" << endl;\n\t}\n\telse if (B >= C && B >= A && (A <= M && C <= K || A <= K && C <= M)) {\n\t\tcout << \"Yes\" << endl;\n\t}\n\telse if (C >= B && C >= A && (A <= M && B <= K || A <= K && B <= M)) {\n\t\tcout << \"Yes\" << endl;\n\t}\n\telse {\n\t\tcout << \"No\" << endl;\n\t}\n\tcout << endl << endl << endl << endl;\n\treturn 0;\n}",
					"lesson": 20,
				},
				{
					"task": 2,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tint money, exchange[] = { 500, 100, 10, 2 };\n\tcin >> money;\n\tif (money % 2 == 0 && money > 0) {\n\t\tfor (int i = 0; i < 4; i++)\n\t\t{\n\t\t\twhile (money - exchange[i] >= 0 && money != 1) {\n\t\t\t\tmoney -= exchange[i];\n\t\t\t\tcout << exchange[i];\n\t\t\t\tif (money > 0)\n\t\t\t\t\tcout << \"+\";\n\t\t\t}\n\t\t}\n\t}\n\telse {\n\t\tcout << \"Error\";\n\t}\n\treturn 0;\n}\n",
					"lesson": 20,
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main() {\n\tstring text;\n\tint end, start;\n\tgetline(cin, text);\n\tend = text.length();\n\tfor (int i = text.length() - 1; i >= 0; i--)\n\t{\n\t\tif (text[i] == ' ' || i == 0) {\n\t\t\tstart = i;\n\t\t\tif (i > 0)\n\t\t\t\tstart++;\n\n\t\t\tfor (int j = start; j < end; j++)\n\t\t\t{\n\t\t\t\tcout << text[j];\n\t\t\t}\n\t\t\tcout << \" \";\n\t\t\tend = i;\n\t\t}\n\t}\n\treturn 0;\n}\n",
					"lesson": 24,
				},
				{
					"task": 2,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main() {\n\tstring text;\n\tgetline(cin, text);\n\tfor (int i = 0; i < 3; i++)\n\t{\n\t\tcout << text.substr(0, 3);\n\t}\n\treturn 0;\n}\n",
					"lesson": 24,
				},
				{
					"task": 3,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nint main() {\n\tstring text, chars = \"уеыаоэяиюУЕЫАОЭЯИЮ\";\n\tunsigned int count = 0;\n\tgetline(cin, text);\n\tfor (unsigned int i = 0; i < text.length(); i++)\n\t{\n\t\tfor (int j = 0; j < chars.length(); j++)\n\t\t{\n\t\t\tif (text[i] == chars[j])\n\t\t\t\tcount++;\n\t\t}\n\t}\n\tcout << count;\n\treturn 0;\n}",
					"lesson": 24,
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nstring pluralize(int number, string zero, string one, string two) {\n\tif (number % 100 >= 11 && number % 100 <= 14)\n\t\treturn to_string(number) + \" \" + zero;\n\telse if (number % 10 == 1)\n\t\treturn to_string(number) + \" \" + one;\n\telse if (number % 10 >= 2 && number % 10 <= 4)\n\t\treturn to_string(number) + \" \" + two;\n\telse\n\t\treturn to_string(number) + \" \" + zero;\n}\n\nint main() {\n\tstring arr[3];\n\tint index = 0, coins, crystals, rubles;\n\tcin >> coins >> crystals >> rubles;\n\tif (coins > 0) \n\t\tarr[index++] = pluralize(coins, \"монет\", \"монета\", \"монеты\");\n\tif (crystals > 0)\n\t\tarr[index++] = pluralize(crystals, \"кристаллов\", \"кристалл\", \"кристалла\");\n\tif (rubles > 0)\n\t\tarr[index++] = pluralize(rubles, \"рублей\", \"рубль\", \"рубля\");\n\tif (index == 3) \n\t\tcout << \"У Вас есть \" << arr[0] << \", \" << arr[1] << \" и \" << arr[2];\n\telse if (index == 2)\n\t\tcout << \"У Вас есть \" << arr[0] << \" и \" << arr[1];\n\telse if (index == 1)\n\t\tcout << \"У Вас есть \" + arr[0];\n\telse\n\t\tcout << \"У Вас пока ничего нет\";\n\treturn 0;\n}",
					"lesson": 26,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\ndouble sum(double a, double b) {\n\treturn a + b;\n}\ndouble subtract(double a, double b) {\n\treturn a - b;\n}\ndouble multiply(double a, double b) {\n\treturn a * b;\n}\n\nint main()\n{\n\tdouble a, b;\n\tcin >> a >> b;\n\tcout << sum(a, b) << endl;\n\tcout << subtract(a, b) << endl;\n\tcout << multiply(a, b);\n\treturn 0;\n}",
					"lesson": 27,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\ndouble min_val(double a, double b, double c) {\n\tif (a <= b && a <= c) {\n\t\treturn a;\n\t}\n\telse if (b <= a && b <= c) {\n\t\treturn b;\n\t}\n\telse {\n\t\treturn c;\n\t}\n}\n\nint main() {\n\tdouble a, b, c;\n\tcin >> a >> b >> c;\n\tcout << min_val(a, b, c);\n\treturn 0;\n}",
					"lesson": 29,
				},
				{
					"task": 1,
					"code": "#include <iostream>\nusing namespace std;\n\nint main() {\n\tchar chars[1001];\n\tint sum = 0;\n\tcin >> chars;\n\tfor (int i = 0; i < 1001; i++)\n\t{\n\t\tif (chars[i] == '\\0') {\n\t\t\tbreak;\n\t\t}\n\t\tsum += chars[i] - '0';\n\t}\n\tif (sum % 3 == 0) {\n\t\tcout << \"Yes\";\n\t}\n\telse\n\t{\n\t\tcout << sum % 3;\n\t}\n\treturn 0;\n}",
					"lesson": 30,
				},
				{
					"task": 1,
					"code": "#include <iostream>\n#include <string>\nusing namespace std;\n\nstring calculate_age(string birth_date) {\n\tint day, month, year, age, c_day = 22, c_month = 12, c_year = 2017;\n\tint dots[2], index = 0;\n\tfor (int i = 0; i < birth_date.length(); i++) {\n\t\tif (birth_date[i] == '.') {\n\t\t\tdots[index++] = i;\n\t\t}\n\t}\n\tday = stoi(birth_date.substr(0, dots[0]));\n\tmonth = stoi(birth_date.substr(dots[0] + 1, dots[1] - dots[0] - 1));\n\tyear = stoi(birth_date.substr(dots[1] + 1, 4));\n\tage = c_year - year;\n\tif (c_month < month || (c_month == month && c_day < day))\n\t\tage--;\n\tif (age < 0)\n\t\treturn \"Вы еще не родились\";\n\telse if (age % 100 >= 11 && age % 100 <= 14)\n\t\treturn to_string(age) + \" лет\";\n\telse if (age % 10 == 1)\n\t\treturn to_string(age) + \" год\";\n\telse if (age % 10 >= 2 && age % 10 <= 4)\n\t\treturn to_string(age) + \" года\";\n\telse\n\t\treturn to_string(age) + \" лет\";\n}\n\nint main() {\n\tstring b_date;\n\tcin >> b_date;\n\tcout << calculate_age(b_date);\n\treturn 0;\n}",
					"lesson": 31,
				},
			],
		}

		for course_item in data.get('_courses', list()):
			new_course = Course()
			if '_group' in course_item.keys():
				group = Group.objects.create(name=course_item['_group'])
				new_course.group = group
			new_course.category = course_item['category']
			new_course.title = course_item.get('title', "")
			new_course.short_desc = course_item.get('short_desc', "")
			new_course.difficulty = course_item['difficulty']
			new_course.cost_coins = course_item.get('cost_coins', 0)
			new_course.cost_cristals = course_item.get('cost_cristals', 0)
			new_course.cost_rubles = course_item.get('cost_rubles', 0)
			new_course.available_from = course_item.get('available_from')
			new_course.available_to = course_item.get('available_to')
			new_course.save()

			# lessons_i = 1
			if '_lessons' in course_item.keys():
				for lesson_item in course_item['_lessons']:
					new_lesson = Lesson()
					new_lesson.course = new_course
					new_lesson.title = lesson_item.get('title', "")
					new_lesson.short_desc = lesson_item.get('short_desc', "")
					new_lesson.html = lesson_item.get('html', "")
					new_lesson.order_index = lesson_item['order_index']
					# if lessons_i != lesson_item['order_index']:
						# raise ValueError("lesson order_index must be {} given {}".format(lessons_i, lesson_item['order_index']))
					new_lesson.save()

					if '_tasks' in lesson_item.keys():
						tasks_i = 1
						for task_item in lesson_item['_tasks']:
							new_task = Task()
							new_task.lesson = new_lesson
							new_task.node = task_item.get('node')
							new_task.programming_language = task_item.get('programming_language')
							
							new_task.task_type = task_item['task_type']
							new_task.difficulty = task_item['difficulty']

							new_task.title = task_item['title']
							new_task.desc = task_item.get('desc', "")
							
							new_task.default_value = task_item.get('default_value', "")
							new_task.cursor_pos_row = task_item.get('cursor_pos_row')
							new_task.cursor_pos_col = task_item.get('cursor_pos_col')

							new_task.gives_coins = task_item['gives_coins']
							new_task.gives_base_experience = task_item['gives_base_experience']
							new_task.can_be_uploaded_until = task_item.get('can_be_uploaded_until')

							new_task.external_code_id = task_item.get('external_code_id', 0)
							new_task.order_index = task_item['order_index']
							if tasks_i != task_item['order_index']:
								raise ValueError("task order_index must be {} given {}".format(tasks_i, task_item['order_index']))
							new_task.only_manual = task_item.get('only_manual', False)
							new_task.code_pairs_count = task_item.get('code_pairs_count', 2)
							if new_task.task_type == 'C':
								new_task.master_judge = task_item['master_judge']
							else:
								new_task.master_judge = task_item.get('master_judge')
							new_task.save()

							if '_conditions' in task_item.keys():
								for condition_item in task_item['_conditions']:
									new_condition = TaskCondition()
									new_condition.task = new_task
									new_condition.node = condition_item.get('node')
									new_condition.need_to_use = condition_item.get('need_to_use')

									new_condition.title = condition_item['title']
									new_condition.short_desc = condition_item.get('short_desc', "")
									new_condition.external_code_id = condition_item.get('external_code_id', 0)
									new_condition.gives_coins = condition_item['gives_coins']
									new_condition.gives_base_experience = condition_item['gives_base_experience']
									new_condition.save()

							if '_pairs' in task_item.keys():
								pairs_i = 1
								for pair_item in task_item['_pairs']:
									new_pair = TaskCodePair()
									new_pair.task = new_task
									new_pair.node = pair_item.get('node')
									new_pair.skill = pair_item.get('skill')

									new_pair.number = pair_item['number']
									if pairs_i != pair_item['number']:
										raise ValueError("pair number must be {} given {}".format(pairs_i, pair_item['number']))
									new_pair.input_str = pair_item.get('input_str', "")
									new_pair.output_str = pair_item.get('output_str', "")
									new_pair.on_error_text = pair_item.get('on_error_text', "")
									new_pair.judge_type = pair_item.get('judge_type', '1')
									new_pair.external_pair_id = pair_item.get('external_pair_id', 0)

									new_pair.save()

									pairs_i += 1

							if '_options' in task_item.keys():
								for option_item in task_item['_options']:
									new_option = TaskOption()
									new_option.task = new_task
									new_option.title = option_item['title']
									new_option.message = option_item.get('message')
									new_option.is_correct = option_item['is_correct']

									new_option.save()
							tasks_i += 1
					# lessons_i += 1


		if '_references' in data.keys():
			for reference_item in data['_references']:
				task = Task.objects.get(lesson=Lesson.objects.get(order_index=int(reference_item['lesson']), course__title="Основы C++ (осень 17-18)"), order_index=int(reference_item['task']))
				new_reference = ReferenceCode()
				new_reference.task = task
				new_reference.code = reference_item['code']
				new_reference.short_desc = reference_item.get('short_desc', "")

				new_reference.save()

				new_task_exec = TaskExecution()
				new_task_exec.task = task
				new_task_exec.user = my_user
				new_task_exec.answer = reference_item['code']
				new_task_exec.finished_at = datetime.datetime(2017, 10, 9, 18, 00)

				new_task_exec.save()
		
		return HttpResponse("Ok")


	return HttpResponse('<form><input type="submit" name="method" value="POST"></form>')



@login_required
def courses_list(request):
	courses_list = []
	for item in Course.objects.all():
		courses_list.append((item, item in [x.course for x in request.user.course_executions.all()]))
	return render(request, 'learn/courses_list.html', {'courses_list': courses_list})



@login_required
def task_execution_list(request):
	entities = TaskExecution.objects.filter(is_active=True, finished_at__isnull=False).order_by('task__pk', 'user__pk', '-finished_at')
	return render(request, 'learn/task_execution_list.html', {'entities': entities})




@login_required
def course_page(request, course_id):
	item = get_object_or_404(Course, pk=course_id)
	bought = item in [x.course for x in request.user.course_executions.all()]
	lessons_list = []
	for lesson in item.lessons.order_by('order_index', 'id'):
		lessons_list.append((lesson, lesson in [x.lesson for x in request.user.lesson_executions.all()]))
	return render(request, 'learn/course_page.html', {'item': item, 'bought': bought, 'lessons_list': lessons_list})


@login_required
def start_course(request, course_id):
	course_obj = get_object_or_404(Course, pk=course_id)
	
	# check if enought money
	if course_obj.cost_coins <= request.user.profile.coins \
		and course_obj.cost_cristals <= request.user.profile.cristals \
		and course_obj.cost_rubles <= request.user.profile.rubles:
		
		item, created = CourseExecution.objects.get_or_create(user=request.user, course=course_obj)
		if not created:
			# TODO: flash message
			raise Exception('currently bought')
		else:
			if course_obj.cost_coins > 0 or course_obj.cost_cristals > 0 or course_obj.cost_rubles > 0:
				# B - buy transaction type
				request.user.change_user_money("B", coins=course_obj.cost_coins * -1, cristals=course_obj.cost_cristals * -1, rubles=course_obj.cost_rubles * -1)
	else:
		raise Exception("Not enought money")

	return redirect(reverse('course_page', args=[str(course_id)]))



@login_required
def lesson_page(request, lesson_id):
	# TODO: check permissions
	lesson = get_object_or_404(Lesson, pk=lesson_id)
	lesson_execution, created = LessonExecution.objects.get_or_create(lesson=lesson, user=request.user)

	tasks_list = lesson.tasks.order_by('order_index', 'id')
	first_task = tasks_list[0] if tasks_list.exists() else None

	lessons_list = list(lesson.course.lessons.order_by('order_index', 'id'))
	index = lessons_list.index(lesson)
	next_lesson = lessons_list[index + 1] if len(lessons_list) > index + 1 else None

	return render(request, 'learn/lesson_page.html', {'item': lesson, 'first_task': first_task, 
						   'next_lesson': next_lesson, 'lesson_execution': lesson_execution})


@login_required
def start_task(request, task_id):
	#TODO: check permissions
	item = get_object_or_404(Task, pk=task_id)
	task_execution = TaskExecution.objects.create(task=item, user=request.user)

	return redirect(reverse('task_execute', args=[str(task_execution.id)]))


@login_required
def task_execute(request, task_execution_id):
	task_execution = get_object_or_404(TaskExecution, pk=task_execution_id, is_active=True)
	if task_execution.user != request.user:
		#TODO: check and modify output
		return HttpResponseForbidden()

	return render(request, 'learn/task_execute.html', {'task_execution': task_execution, 
							'task': task_execution.task, 'task_execution_id': task_execution.pk})


@login_required
def stat_by_users(request):
	result = "Фамилия"
	data = dict()
	users = dict()
	task_keys = list()
	for task_exec in TaskExecution.objects.filter(is_active=True, finished_at__isnull=False): #, mark__gt=0
		key = task_exec.user.profile.last_name
		if key not in data:
			data[key] = dict()
		key2 = "{:0>2}_{:0>2}".format(task_exec.task.lesson.order_index, task_exec.task.order_index)
		if key2 not in data[key]:
			data[key][key2] = list()
			task_keys.append(key2)
		data[key][key2].append( (float(task_exec.mark) + (task_exec.percent / 200)) * task_exec.task.gives_base_experience / 10 )
	
	task_keys = sorted(list(set(task_keys)))

	for tk in task_keys:
		result += "\t{}".format(tk)
	result += "\n"
	for key in sorted(data.keys()):
		result += key
		for key2 in task_keys:
			if key2 in data[key]:
				result += "\t{:.2f}".format(max(data[key][key2])).replace('.', ',')
			else:
				result += "\t"
		result += "\n"

	# for key in sorted(data.keys()):
	# 	result += "{}\n".format(users[key])
	# 	for key2 in sorted(data[key].keys()):
	# 		result += "\t{}\n".format(key2)
	# 		result += "\t\t{}\n".format(max(data[key][key2]))
	return HttpResponse(result)


@login_required
def task_check(request, task_execution_id):
	out_html = "task check\n"

	if request.method == 'POST':
		task_execution = get_object_or_404(TaskExecution, pk=task_execution_id, is_active=True)
		if task_execution.user != request.user:
			raise Exception("TODO: warning: req.user != task_execution.user")
		task = task_execution.task
		is_correct = None
		
		# TODO: check if user repeat task

		# TODO: check if try to redo (history back)

		# check answers
		res_message = None
		if task.task_type in 'OAS': # one or answer or sequence
			answer = request.POST.get('answer').strip()
			for item in task.options.all():
				if str(item.id) == answer:
					is_correct = item.is_correct
					res_message = item.message
					break
		
		elif task.task_type == 'M':  # multi
			correct = set()
			for item in task.options.all():
				if item.is_correct:
					correct.add(str(item.id))

			is_correct = set(dict(request.POST).get('answer[]')) == correct

		elif task.task_type == 'C':  # code
			answer = request.POST.get('answer').strip()

			if task.options.exists():
				for item in task.options.all():
					if item.title == answer:
						is_correct = item.is_correct
						res_message = item.message
						break
			
			if is_correct is None and task.external_code_id > 0:
				is_correct, status, res_message = process_task(task_execution)

		if is_correct:
			task_type = ContentType.objects.get_for_model(Task)
			trans = Transaction.objects.filter(content_type__pk=task_type.id, object_id=task.id)
			if len(trans) == 0:
				request.user.change_user_money("CA", coins=task.gives_coins, experience_points=task.gives_base_experience, content_object=task)
				out_html += "correct answer\n"
		elif is_correct is None:
			out_html += "unknown answer\n"
		else:
			out_html += "incorrect answer\n"

		if res_message:
			out_html += "res_message: " + res_message + "\n"


		# update execution object
		if request.POST.get('answer'):
			task_execution.answer = request.POST.get('answer')
		elif dict(request.POST).get('answer[]'):
			task_execution.answer = ",".join(dict(request.POST).get('answer[]'))
		else:
			raise Exception("TODO: warning: need answer value")
		task_execution.finished_at = timezone.now()
		task_execution.is_correct = is_correct
		task_execution.save()

		# calculate index and select next task or finalize lesson
		tasks_list = list(task.lesson.tasks.order_by('order_index', 'id'))
		index = tasks_list.index(task)
		if len(tasks_list) > index + 1:
			raise Exception("TODO: No action after task check")
			return redirect(reverse('start_task', args=[str(tasks_list[index + 1].id)]))
		else:
			last_lesson_execs = LessonExecution.objects.filter(user=request.user, lesson=task_execution.task.lesson, finished_at=None)
			# print(len(last_lesson_execs))
			# print(last_lesson_execs)
			
			if len(last_lesson_execs) > 0:
				last_obj = last_lesson_execs.order_by('-id')[0]
				last_obj.finished_at = timezone.now()
				last_obj.save()

				# calculate index and select next lesson or finalize course
				lessons_list = list(task.lesson.course.lessons.order_by('order_index', 'id'))
				index = lessons_list.index(task.lesson)
				if len(lessons_list) > index + 1:
					out_html += "next lesson redirect\n"
					return HttpResponse(out_html)
					return redirect(reverse('lesson_page', args=[str(lessons_list[index + 1].id)]))
			else:
				# TODO: my courses or dashboard
				return HttpResponse(out_html)
				return redirect(reverse('dashboard'))

			#TODO: finalize course
			return HttpResponse(out_html)
			return redirect(reverse('dashboard'))

	#TODO: flash message
	return HttpResponse(out_html)
	return redirect(reverse('start_task', args=[str(task_id)]))



@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_course_list(request):
	courses = Course.objects.all()
	serializer = CourseSerializer(courses, many=True, context={'request': request})
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_course_detail(request, pk):
	try:
		course = Course.objects.get(pk=pk)
	except Course.DoesNotExist:
		save_log("api_course_detail", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	serializer = CoursePageSerializer(course, context={'request': request})
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_course_execution_list(request):
	course_executions = CourseExecution.objects.filter(user=request.user)
	serializer = CourseExecutionSerializer(course_executions, many=True, context={'request': request})
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_course_exection_detail(request, pk):
	try:
		course_execution = CourseExecution.objects.get(pk=pk)
	except CourseExecution.DoesNotExist:
		save_log("api_course_exection_detail", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	if course_execution.user != request.user:
		raise PermissionDenied()

	serializer = CourseExecutionSerializer(course_execution, context={'request': request})
	return Response(serializer.data)


@api_view(['POST'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_lesson_start(request, pk):
	try:
		lesson = Lesson.objects.get(pk=pk)
	except LessonExecution.DoesNotExist:
		save_log("api_lesson_start", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	lesson_execution = LessonExecution()
	lesson_execution.lesson = lesson
	lesson_execution.user = request.user
	lesson_execution.select_difficulty()
	lesson_execution.save()
	serializer = LessonExecutionSerializer(lesson_execution, context={'request': request})
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_lesson_detail(request, pk):
	try:
		lesson = Lesson.objects.get(pk=pk)
	except Lesson.DoesNotExist:
		save_log("api_lesson_detail", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	serializer = LessonPageSerializer(lesson, context={'request': request})
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_lesson_execution_detail(request, pk):
	try:
		lesson_execution = LessonExecution.objects.get(pk=pk)
	except LessonExecution.DoesNotExist:
		save_log("api_lesson_execution_detail", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	serializer = LessonExecutionSerializer(lesson_execution, context={'request': request})
	return Response(serializer.data)



@api_view(['POST'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_task_start(request, pk):
	try:
		task = Task.objects.get(pk=pk)
	except Task.DoesNotExist:
		save_log("api_task_start", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	task_execution = TaskExecution()
	task_execution.task = task
	task_execution.user = request.user
	task_execution.save()
	serializer = TaskExecutionSerializer(task_execution, context={'request': request})
	return Response(serializer.data)




@api_view(['GET', 'PUT'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAdminUser, ))
def api_task_execution_detail_admin(request, pk):
	try:
		task_execution = TaskExecution.objects.get(pk=pk, is_active=True)
	except TaskExecution.DoesNotExist:
		save_log("api_task_execution_detail_admin", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = TaskExecutionAdminSerializer(task_execution, context={'request': request})
		return Response(serializer.data)
	
	elif request.method == 'PUT':
		old_notes = set([x.pk for x in task_execution.notes.all()])
		new_notes = set([x.get('pk') for x in request.data.get('notes')])
		task_execution.notes.filter(pk__in=list(old_notes - new_notes)).delete()

		for x in request.data.get('notes'):
			if x.get('pk') is None:
				# print(x)
				note = TaskNote.objects.get(pk=x.get('note_obj', {}).get('pk'))
				new_task_note = task_execution.notes.create(note_obj=note, ranges=x.get('ranges', ""))
			else: 
				task_note = task_execution.notes.get(pk=x.get('pk'))
				if x.get('ranges', "") != task_note.ranges:
					task_note.ranges = x.get('ranges', "")
					task_note.save()

		task_execution.mark = request.data.get('mark')
		task_execution.checked_at = timezone.now()
		task_execution.save()

		serializer = TaskExecutionAdminSerializer(task_execution, context={'request': request})
		return Response(serializer.data)



@api_view(['GET', 'PUT'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_task_execution_detail(request, pk):
	try:
		task_execution = TaskExecution.objects.get(pk=pk, is_active=True)
	except TaskExecution.DoesNotExist:
		save_log("api_task_execution_detail", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	if task_execution.user != request.user:
		save_log("api_task_execution_detail", "No permissions in " + str(task_execution.pk))
		msg = "Нет доступа"
		return Response({"detail": msg}, status=rest_framework.status.HTTP_403_FORBIDDEN)

	if request.method == 'GET':
		serializer = TaskExecutionSerializer(task_execution, context={'request': request})
		return Response(serializer.data)
	
	elif request.method == 'PUT':
		out_str = "task check\n"
		now = timezone.now()
		task = task_execution.task
		
		# update execution object
		if request.data.get('answer'):
			if task.task_type == 'O': # one
				task_execution.answer = str(request.data.get('answer'))
			elif task.task_type == 'A': # answer
				task_execution.answer = request.data.get('answer').strip()
			elif task.task_type == 'S': # sequence
				task_execution.answer = ",".join([str(x) for x in request.data.get('answer')])
			elif task.task_type == 'M':  # multi
				task_execution.answer = ",".join([str(x) for x in request.data.get('answer')])
			elif task.task_type == 'C':  # code
				task_execution.answer = request.data.get('answer').strip()
			else:
				task_execution.answer = str(request.data.get('answer')).strip()
				save_log("api_task_execution_detail", "unknown task_type " + str(task.task_type))

		else:
			save_log("api_task_execution_detail", "need answer value in " + str(task_execution.pk))
			msg = "Требуется ответ"
			return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)

		if request.GET.get('only_save', "0") == "1":
			task_execution.save()
			UseStat.objects.create(user=request.user, stat_type='SAV', json_string=simplejson.dumps(dict(length=len(task_execution.answer))), content_object=task_execution)
			save_log("api_task_execution_detail", "only save in " + str(task_execution.pk) + " with length: " + str(len(task_execution.answer)))
			serializer = TaskExecutionSerializer(task_execution, context={'request': request})
			return Response({'item': serializer.data, 'only_save': True})

		# check if user repeat task
		if task.task_type in 'OMAS':
			if TaskExecution.objects.filter(user=request.user, task=task, finished_at__isnull=False).exists():
				save_log("api_task_execution_detail", "task with type {} has been already sended in {}".format(task.task_type, task_execution.pk))
				msg = "Вы уже отправляли эту задачу"
				return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)

		# check answers
		is_correct = None
		res_message = None
		if task.task_type == 'O': # one
			answer = str(request.data.get('answer'))
			for item in task.options.all():
				if str(item.id) == str(answer):
					is_correct = item.is_correct
					res_message = item.message
					break

		elif task.task_type == 'A': # answer
			answer = request.data.get('answer').strip()
			for item in task.options.all():
				if item.title == answer:
					is_correct = item.is_correct
					res_message = item.message
					break

		elif task.task_type == 'S': # sequence
			answer = ",".join([str(x) for x in request.data.get('answer')])
			for item in task.options.all():
				if item.title == answer:
					is_correct = item.is_correct
					res_message = item.message
					break
		
		elif task.task_type == 'M':  # multi
			answer = ",".join([str(x) for x in request.data.get('answer')])
			correct = set()
			for item in task.options.all():
				if item.is_correct:
					correct.add(item.id)

			is_correct = set(request.data.get('answer')) == correct

		elif task.task_type == 'C':  # code

			task_execution.status_string = "Ожидает проверки"
			if task.only_manual:
				task_execution.status_string = "Ожидайте проверки преподавателем"

			answer = request.data.get('answer').strip()
			if request.user.reference_views.filter(task=task).exists():
				task_execution.status = "RCV"
			elif not task.get_can_be_uploaded(request.user):
				task_execution.status = "TO"
			else:
				if task.options.exists():
					for item in task.options.all():
						if item.title == answer:
							is_correct = item.is_correct
							res_message = item.message
							break
				
				if is_correct is None and task.external_code_id > 0:
					is_correct, status, res_message = process_task(task_execution)


		if is_correct:
			if task_execution.status == 'NO': 
				task_execution.status = "OK"
				task_execution.status_string = task_execution.get_status_string(task_execution.status)
			out_str += "correct answer\n"

			content_type = ContentType.objects.get_for_model(Task)
			trans = Transaction.objects.filter(content_type__pk=content_type.id, object_id=task.id, user=request.user)
			if len(trans) == 0:
				request.user.change_user_money("CA", coins=task.gives_coins, content_object=task)
				out_str += "first task execution with this user\n"
			else:
				out_str += "task with this user already has transaction\n"

		elif is_correct is None:
			if task_execution.status == 'NO' and task.task_type != 'C':
				task_execution.status = "UA"
				task_execution.status_string = "Ожидай проверки преподавателем"
			out_str += "unknown answer\n"

		else:
			if task_execution.status == 'NO':
				task_execution.status = "WA"
				task_execution.status_string = task_execution.get_status_string(task_execution.status)

			out_str += "incorrect answer\n"

		if res_message:
			out_str += "res_message: " + res_message + "\n"
			if task_execution.status_string:
				task_execution.status_string += ": " + res_message
			else:
				task_execution.status_string = res_message


		# print(out_str)

		task_execution.finished_at = timezone.now()
		task_execution.is_correct = is_correct
		task_execution.save()

		# create transaction for code check
		if task.task_type == 'C':  # code
			request.user.change_user_money("CS", coins=-10, content_object=task_execution)

		next_task = None
		next_lesson = None
		go_to_dashboard = False

		# calculate index and select next task or finalize lesson
		tasks_list = list(task.lesson.tasks.order_by('order_index', 'id'))
		index = tasks_list.index(task)

		# if has next task in this lesson
		if len(tasks_list) > index + 1:
			next_task = tasks_list[index + 1].pk
			last_next_task_exec = TaskExecution.objects.filter(user=request.user, task=tasks_list[index + 1], is_active=True).last()
			if last_next_task_exec:
				if last_next_task_exec.task.can_be_uploaded_until:
					if last_next_task_exec.task.get_can_be_uploaded(request.user):
						next_task = -1 * last_next_task_exec.pk
				else:
					next_task = -1 * last_next_task_exec.pk

		data = TaskExecution.objects.filter(user=request.user, task__pk__in=[x.pk for x in tasks_list], finished_at__isnull=False, is_active=True).values('task').annotate(total=Count('task'))
		# print(data.count() / len(tasks_list))
		if data.count() / len(tasks_list) == 1.0:
			last_lesson_exec = LessonExecution.objects.filter(user=request.user, lesson=task.lesson, finished_at=None).last()
			# if has lesson executions
			if last_lesson_exec:
				last_lesson_exec.finished_at = timezone.now()
				last_lesson_exec.save()
			else:
				save_log("api_task_execution_detail: PUT", "No last unfinished lesson execution with task_exec_pk: " + str(task_execution.pk))
				go_to_dashboard = True

			# calculate index and select next lesson or finalize course
			lessons_list = list(task.lesson.course.lessons.order_by('order_index', 'id'))
			index = lessons_list.index(task.lesson)

			# if has next lesson in this course
			if len(lessons_list) > index + 1:
				next_lesson = lessons_list[index + 1].pk
				next_lesson_exec = LessonExecution.objects.filter(user=request.user, lesson=lessons_list[index + 1]).last()
				if next_lesson_exec:
					next_lesson = -1 * next_lesson_exec.pk


				# TODO: finalize course
				# last_course_exec = CourseExecution.objects.filter(user=request.user, lesson=last_course_exec.lesson, finished_at=None).last()
				# if last_course_exec:
				# 	last_course_exec.finished_at = timezone.now()
				# 	last_course_exec.save()
				# else:
				# 	go_to_dashboard = True


		serializer = TaskExecutionSerializer(task_execution, context={'request': request})
		return Response({'item': serializer.data, 'next_task': next_task, 'next_lesson': next_lesson, 'go_to_dashboard': go_to_dashboard, 'only_save': False})



@api_view(['PUT', 'DELETE'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_task_execution_active(request, pk):
	try:
		task_execution = TaskExecution.objects.get(pk=pk)
	except TaskExecution.DoesNotExist:
		save_log("api_task_execution_active", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	if task_execution.user != request.user:
		save_log("api_task_execution_active", "No permissions in " + str(task_execution.pk))
		msg = "Нет доступа"
		return Response({"detail": msg}, status=rest_framework.status.HTTP_403_FORBIDDEN)

	if request.method == 'PUT':
		# log if not sync
		if task_execution.is_active != False:
			save_log("api_task_execution_active: PUT", "No is_active sync task_exec_pk: " + str(task_execution.pk))

		task_execution.is_active = True
		task_execution.save()
		UseStat.objects.create(user=request.user, stat_type='TER', json_string=simplejson.dumps(dict(is_active=task_execution.is_active), ensure_ascii=False), content_object=task_execution)
		return Response({'detail': "Ok"}, status=rest_framework.status.HTTP_201_CREATED)
	
	elif request.method == 'DELETE':
		# log if not sync
		if task_execution.is_active != True:
			save_log("api_task_execution_active: PUT", "No is_active sync task_exec_pk: " + str(task_execution.pk))

		task_execution.is_active = False
		task_execution.save()
		UseStat.objects.create(user=request.user, stat_type='TED', json_string=simplejson.dumps(dict(is_active=task_execution.is_active), ensure_ascii=False), content_object=task_execution)
		return Response(status=rest_framework.status.HTTP_204_NO_CONTENT)



@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_dashboard(request):
	now = timezone.now()
	incompleted_task_code_executions = TaskExecution.objects.filter(task__task_type="C", user=request.user, finished_at__isnull=True, is_active=True).exclude(task__can_be_uploaded_until__lt=now).order_by('-id')
	incompleted_task_code_executions_serialized = TaskExecutionFinalSerializer(incompleted_task_code_executions[:5], many=True)

	completed_task_code_executions = TaskExecution.objects.filter(task__task_type="C",user=request.user, finished_at__isnull=False, is_active=True).order_by('-id')
	completed_task_code_executions_serialized = TaskExecutionFinalSerializer(completed_task_code_executions[:5], many=True)

	data = {
		'incompleted_task_code_executions': incompleted_task_code_executions_serialized.data,
		'incompleted_task_code_executions_count': len(incompleted_task_code_executions),
		'completed_task_code_executions': completed_task_code_executions_serialized.data,
		'completed_task_code_executions_count': len(completed_task_code_executions),
	}
	return Response(data)




@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_get_notes(request):
	notes = TaskNote.objects.all()
	serializer = TaskNoteSerializer(notes, many=True)
	return Response(serializer.data)


@api_view(['GET', 'POST'])
def api_get_task(request):
	return Response({'detail': "No info"})



@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAdminUser, ))
def api_task_execution_admin_list(request):
	count = 10
	page = 1
	if request.GET.get('page', "").isdigit() and int(request.GET.get('page')) > 0:
		page = int(request.GET.get('page'))
		# print(page)
	offset = count * (page - 1)

	items = TaskExecution.objects.filter(task__task_type='C', status='NO', task__only_manual=False, finished_at__isnull=False, is_active=True)[offset:offset + count]
	serializer = TaskExecutionAdminSerializer(items, many=True)
	return Response(serializer.data)



@api_view(['GET', 'PUT'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAdminUser, ))
def api_task_execution_check_admin(request, pk):
	try:
		task_execution = TaskExecution.objects.get(pk=pk, is_active=True)
	except TaskExecution.DoesNotExist:
		save_log("api_task_execution_check_admin", "HTTP_404_NOT_FOUND in " + str(pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = TaskExecutionAdminSerializer(task_execution, context={'request': request})
		return Response(serializer.data)
	
	elif request.method == 'PUT':
		if not request.data:
			return Response({'detail': "No data"}, status=rest_framework.status.HTTP_400_BAD_REQUEST)

		task = task_execution.task
		
		out_dir = os.path.join(settings.MEDIA_ROOT, 'api_check')
		if not os.path.exists(out_dir):
			os.makedirs(out_dir)


		now = timezone.now()
		with open(os.path.join(out_dir, '{}_{}-{}-{}_{}-{}-{}.json'.format(pk, now.year, now.month, now.day, now.hour, now.minute, now.second)), 'w', encoding="utf-8") as f:
			f.write(simplejson.dumps(request.data, ensure_ascii=False))
				
		CompilerCheckNote.objects.filter(check_obj=task_execution).delete()
		if request.data.get('cmperr'):
			errors = request.data.get('cmperr').split('\n')
			for line in errors:
				arr = line.split(')', 1)
				if len(arr) > 1:
					filename, line_number = arr[0].split('(')

					filename = filename.rsplit('\\')[-1].split('/')[-1]
					# TODO: save by files
					if filename != 'main.cpp':
						save_log("api_task_execution_check_admin", "file name is " + str(filename))
					if line_number.isdigit():
						line_number = int(line_number)
						note = arr[1].strip().lstrip(':').strip()
						if 'error' in line:
							note_obj, _ = CompilerNote.objects.get_or_create(note=note, note_type='0')
							new_note, _ = CompilerCheckNote.objects.get_or_create(note_obj=note_obj, check_obj=task_execution, line_number=line_number)
						elif 'warning' in line:
							note_obj, _ = CompilerNote.objects.get_or_create(note=note, note_type='1')
							new_note, _ = CompilerCheckNote.objects.get_or_create(note_obj=note_obj, check_obj=task_execution, line_number=line_number)
						elif 'note' in line:
							note_obj, _ = CompilerNote.objects.get_or_create(note=note, note_type='2')
							new_note, _ = CompilerCheckNote.objects.get_or_create(note_obj=note_obj, check_obj=task_execution, line_number=line_number)

		if request.data.get("status_code", 0) > 8:
			if task_execution.status != 'NO':
				save_log("api_task_execution_check_admin", "TaskExecution has already updated in " + str(task_execution.pk))
				msg = "Объект уже обновлен"
				return Response({'detail': msg}, status=rest_framework.status.HTTP_403_FORBIDDEN)

			if request.data.get('status_code') == 15:
				cases = list()
				for testcase in request.data.get('testcases', list()):
					
					pair = TaskCodePair.objects.get(task=task, number=testcase.get('number', 0))
					temp_list = TaskCodeCheckDetail.objects.filter(task_exec=task_execution, pair=pair)
					if len(temp_list) == 1:
						case = temp_list[0]
					elif len(temp_list) == 0:
						case = TaskCodeCheckDetail(task_exec=task_execution, pair=pair)
					else:
						raise Exception(str(len(temp_list)) + " pairs was founded")
					case.status = testcase['status']
					case.answer = testcase.get('stdout')
					case.save()
					cases.append(case)

				percentage, status, status_string = process_pairs(task_execution, cases)
				task_execution.status = status
				task_execution.percent = percentage
				if status_string:
					task_execution.status_string = status_string
				if percentage == 100:
					if task_execution.status == "NO":
						task_execution.status = 'AC'
						task_execution.status_string = task_execution.get_status_string(task_execution.status)
					task_execution.is_correct = True

					if task_execution.user:
						content_type = ContentType.objects.get_for_model(Task)
						trans = Transaction.objects.filter(content_type__pk=content_type.id, object_id=task.id, user=task_execution.user)
						if len(trans) == 0:
							task_execution.user.change_user_money("CA", coins=task.gives_coins, content_object=task)
				else:
					if task_execution.status == "NO":
						task_execution.status = 'WA'
						task_execution.status_string = task_execution.get_status_string(task_execution.status)
					task_execution.is_correct = False
			elif request.data.get('status_code') == 11:
				task_execution.status = 'CE'
				task_execution.status_string = task_execution.get_status_string(task_execution.status)
				task_execution.is_correct = False
			else:
				raise Exception("Unknown status" + str(request.data.get('status_code')))

			task_execution.save()
			return Response({'detail': "OK"})

		save_log("api_task_execution_check_admin", "Status <= 8: {} in {}".format(request.data.get("status_code"), task_execution.pk))
		msg = "Статус <= 8"
		return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)
	
	msg = "Unknown error"
	save_log("api_task_execution_check_admin", msg + " in " + str(task_execution.pk))
	return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_completed_codes(request):
	count = 50
	page = 1
	if request.GET.get('page', "").isdigit() and int(request.GET.get('page')) > 0:
		page = int(request.GET.get('page'))
		# print(page)
	offset = count * (page - 1)

	completed_task_code_executions = TaskExecution.objects.filter(task__task_type="C",user=request.user, finished_at__isnull=False, is_active=True).order_by('-id')
	completed_task_code_executions_serialized = TaskExecutionFinalSerializer(completed_task_code_executions[offset:offset+count], many=True)

	data = {
		'entities': completed_task_code_executions_serialized.data,
		'all_entities_count': len(completed_task_code_executions),
	}
	return Response(data)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_incompleted_codes(request):
	count = 50
	page = 1
	if request.GET.get('page', "").isdigit() and int(request.GET.get('page')) > 0:
		page = int(request.GET.get('page'))
	offset = count * (page - 1)

	incompleted_task_code_executions = TaskExecution.objects.filter(task__task_type="C", user=request.user, finished_at__isnull=True, is_active=True).order_by('-id')
	incompleted_task_code_executions_serialized = TaskExecutionFinalSerializer(incompleted_task_code_executions[offset:offset+count], many=True)

	data = {
		'entities': incompleted_task_code_executions_serialized.data,
		'all_entities_count': len(incompleted_task_code_executions),
	}

	return Response(data)


@api_view(['POST'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated, ))
def api_manual_difficulty(request, task_exec_pk):
	try:
		task_execution = TaskExecution.objects.get(pk=task_exec_pk, is_active=True)
	except TaskExecution.DoesNotExist:
		save_log("api_manual_difficulty", "HTTP_404_NOT_FOUND in " + str(task_exec_pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)

	if request.data.get('rate') is None:
		save_log("api_manual_difficulty", "rate is required in " + str(task_execution.pk))
		msg = "Поле \"rate\" обязательное"
		return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)
	elif int(request.data.get('rate')) < -2 or int(request.data.get('rate')) > 2:
		save_log("api_manual_difficulty", "rate ValueError: " + str(request.data.get('rate')) + " in " + str(task_execution.pk))
		msg = "Ошибочное значение оценки"
		return Response({'detail': msg}, status=rest_framework.status.HTTP_400_BAD_REQUEST)

	new_obj = ManualDifficultySelect(user=request.user, task_exec=task_execution, rate=request.data.get('rate'))
	new_obj.save()
	return Response({'detail': "Ok"}, status=rest_framework.status.HTTP_201_CREATED)


@api_view(['GET'])
@authentication_classes((MyTokenAuthentication, SessionAuthentication))
def api_reference_codes(request, task_pk):
	try:
		task = Task.objects.get(pk=task_pk)
	except Task.DoesNotExist:
		save_log("api_reference_codes", "HTTP_404_NOT_FOUND in " + str(task_pk))
		return Response(status=rest_framework.status.HTTP_404_NOT_FOUND)
	
	# get or create view
	ReferenceCodeView.objects.get_or_create(task=task, user=request.user)

	entities = ReferenceCode.objects.filter(task=task)
	serializer = ReferenceCodeSerializer(entities, many=True)
	return Response({'task': TaskSerializer(task).data, 'entities': serializer.data})
