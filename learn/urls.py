from django.conf.urls import url, include
from rest_framework import routers
from learn import api
from learn import views

router = routers.DefaultRouter()
router.register(r'speaker', api.SpeakerViewSet)
router.register(r'coursecategory', api.CourseCategoryViewSet)
router.register(r'course', api.CourseViewSet)
router.register(r'courseexecution', api.CourseExecutionViewSet)
router.register(r'lesson', api.LessonViewSet)
router.register(r'lessonblock', api.LessonBlockViewSet)
router.register(r'lessonvideo', api.LessonVideoViewSet)
router.register(r'lessonexecution', api.LessonExecutionViewSet)
router.register(r'lessonquestion', api.LessonQuestionViewSet)
router.register(r'taskquestion', api.TaskQuestionViewSet)
router.register(r'programminglanguage', api.ProgrammingLanguageViewSet)
router.register(r'task', api.TaskViewSet)
router.register(r'taskcondition', api.TaskConditionViewSet)
router.register(r'taskoption', api.TaskOptionViewSet)
router.register(r'taskexecution', api.TaskExecutionViewSet)
router.register(r'taskconditionhistory', api.TaskConditionHistoryViewSet)
router.register(r'taskcodepair', api.TaskCodePairViewSet)
router.register(r'taskcodecheckdetail', api.TaskCodeCheckDetailViewSet)
router.register(r'tasknote', api.TaskNoteViewSet)
router.register(r'taskcodemanualchecknote', api.TaskCodeManualCheckNoteViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    url(r'^api/v1/', include(router.urls)),
)


urlpatterns += (
    url(r'^api/dashboard/$', views.api_dashboard, name='api_dashboard'),

    url(r'^api/courses/$', views.api_course_list, name='api_course_list'),
    url(r'^api/courses/(?P<pk>[0-9]+)$', views.api_course_detail, name='api_course_detail'),

    url(r'^api/course_executions/$', views.api_course_execution_list, name='api_course_execution_list'),
    url(r'^api/course_executions/(?P<pk>[0-9]+)$', views.api_course_exection_detail, name='api_course_exection_detail'),
    
    url(r'^api/lessons/(?P<pk>[0-9]+)$', views.api_lesson_detail, name='api_lesson_detail'),
    url(r'^api/lessons/(?P<pk>[0-9]+)/start/$', views.api_lesson_start, name='api_lesson_start'),
    url(r'^api/lessonexecution/(?P<pk>[0-9]+)$', views.api_lesson_execution_detail, name='api_lesson_execution_detail'),
    
    url(r'^api/tasks/(?P<pk>[0-9]+)/start/$', views.api_task_start, name='api_task_start'),
    url(r'^api/taskexecution/(?P<pk>[0-9]+)$', views.api_task_execution_detail, name='api_task_execution_detail'),
    url(r'^api/taskexecution/(?P<pk>[0-9]+)/active$', views.api_task_execution_active, name='api_task_execution_active'),
    url(r'^api/taskexecution/admin/(?P<pk>[0-9]+)$', views.api_task_execution_detail_admin, name='api_task_execution_detail_admin'),
    url(r'^api/notes/$', views.api_get_notes, name='api_get_notes'),

    url(r'^api/taskexecution/admin/$', views.api_task_execution_admin_list, name='api_task_execution_admin_list'),
    url(r'^api/taskexecution/admin/(?P<pk>[0-9]+)/check$', views.api_task_execution_check_admin, name='api_task_execution_check_admin'),
    
    url(r'^api/manual_difficulty/(?P<task_exec_pk>[0-9]+)$', views.api_manual_difficulty, name='api_manual_difficulty'),
    
    url(r'^api/completed_codes/$', views.api_completed_codes, name='api_completed_codes'),
    url(r'^api/incompleted_codes/$', views.api_incompleted_codes, name='api_incompleted_codes'),
    
    url(r'^api/reference_codes/(?P<task_pk>[0-9]+)$', views.api_reference_codes, name='api_reference_codes'),
    
    url(r'^taskexecution/$', views.task_execution_list, name='task_execution_list'),
    url(r'^tree/$', views.tree, name='tree'),
    url(r'^stat_by_users/$', views.stat_by_users, name='stat_by_users'),
    url(r'^create_initial/$', views.create_initial, name='create_initial'),
    url(r'^create_cpp_course/$', views.create_cpp_course, name='create_cpp_course'),
    url(r'^create_executions/$', views.create_executions, name='create_executions'),
)


# urlpatterns += (
#     # urls for CourseCategory
#     url(r'^coursecategory/$', views.CourseCategoryListView.as_view(), name='learn_coursecategory_list'),
#     url(r'^coursecategory/create/$', views.CourseCategoryCreateView.as_view(), name='learn_coursecategory_create'),
#     url(r'^coursecategory/detail/(?P<pk>\S+)/$', views.CourseCategoryDetailView.as_view(), name='learn_coursecategory_detail'),
#     url(r'^coursecategory/update/(?P<pk>\S+)/$', views.CourseCategoryUpdateView.as_view(), name='learn_coursecategory_update'),
# )

# urlpatterns += (
#     # urls for Course
#     url(r'^course/$', views.CourseListView.as_view(), name='learn_course_list'),
#     url(r'^course/create/$', views.CourseCreateView.as_view(), name='learn_course_create'),
#     url(r'^course/detail/(?P<pk>\S+)/$', views.CourseDetailView.as_view(), name='learn_course_detail'),
#     url(r'^course/update/(?P<pk>\S+)/$', views.CourseUpdateView.as_view(), name='learn_course_update'),
# )

# urlpatterns += (
#     # urls for CourseExecution
#     url(r'^courseexecution/$', views.CourseExecutionListView.as_view(), name='learn_courseexecution_list'),
#     url(r'^courseexecution/create/$', views.CourseExecutionCreateView.as_view(), name='learn_courseexecution_create'),
#     url(r'^courseexecution/detail/(?P<pk>\S+)/$', views.CourseExecutionDetailView.as_view(), name='learn_courseexecution_detail'),
#     url(r'^courseexecution/update/(?P<pk>\S+)/$', views.CourseExecutionUpdateView.as_view(), name='learn_courseexecution_update'),
# )

# urlpatterns += (
#     # urls for Lesson
#     url(r'^lesson/$', views.LessonListView.as_view(), name='learn_lesson_list'),
#     url(r'^lesson/create/$', views.LessonCreateView.as_view(), name='learn_lesson_create'),
#     url(r'^lesson/detail/(?P<pk>\S+)/$', views.LessonDetailView.as_view(), name='learn_lesson_detail'),
#     url(r'^lesson/update/(?P<pk>\S+)/$', views.LessonUpdateView.as_view(), name='learn_lesson_update'),
# )

# urlpatterns += (
#     # urls for LessonExecution
#     url(r'^lessonexecution/$', views.LessonExecutionListView.as_view(), name='learn_lessonexecution_list'),
#     url(r'^lessonexecution/create/$', views.LessonExecutionCreateView.as_view(), name='learn_lessonexecution_create'),
#     url(r'^lessonexecution/detail/(?P<pk>\S+)/$', views.LessonExecutionDetailView.as_view(), name='learn_lessonexecution_detail'),
#     url(r'^lessonexecution/update/(?P<pk>\S+)/$', views.LessonExecutionUpdateView.as_view(), name='learn_lessonexecution_update'),
# )

# urlpatterns += (
#     # urls for LessonQuestion
#     url(r'^lessonquestion/$', views.LessonQuestionListView.as_view(), name='learn_lessonquestion_list'),
#     url(r'^lessonquestion/create/$', views.LessonQuestionCreateView.as_view(), name='learn_lessonquestion_create'),
#     url(r'^lessonquestion/detail/(?P<pk>\S+)/$', views.LessonQuestionDetailView.as_view(), name='learn_lessonquestion_detail'),
#     url(r'^lessonquestion/update/(?P<pk>\S+)/$', views.LessonQuestionUpdateView.as_view(), name='learn_lessonquestion_update'),
# )

# urlpatterns += (
#     # urls for Task
#     url(r'^task/$', views.TaskListView.as_view(), name='learn_task_list'),
#     url(r'^task/create/$', views.TaskCreateView.as_view(), name='learn_task_create'),
#     url(r'^task/detail/(?P<pk>\S+)/$', views.TaskDetailView.as_view(), name='learn_task_detail'),
#     url(r'^task/update/(?P<pk>\S+)/$', views.TaskUpdateView.as_view(), name='learn_task_update'),
# )

# urlpatterns += (
#     # urls for TaskExecution
#     url(r'^taskexecution/$', views.TaskExecutionListView.as_view(), name='learn_taskexecution_list'),
#     url(r'^taskexecution/create/$', views.TaskExecutionCreateView.as_view(), name='learn_taskexecution_create'),
#     url(r'^taskexecution/detail/(?P<pk>\S+)/$', views.TaskExecutionDetailView.as_view(), name='learn_taskexecution_detail'),
#     url(r'^taskexecution/update/(?P<pk>\S+)/$', views.TaskExecutionUpdateView.as_view(), name='learn_taskexecution_update'),
# )

# urlpatterns += (
#     # urls for TaskNote
#     url(r'^tasknote/$', views.TaskNoteListView.as_view(), name='learn_tasknote_list'),
#     url(r'^tasknote/create/$', views.TaskNoteCreateView.as_view(), name='learn_tasknote_create'),
#     url(r'^tasknote/detail/(?P<pk>\S+)/$', views.TaskNoteDetailView.as_view(), name='learn_tasknote_detail'),
#     url(r'^tasknote/update/(?P<pk>\S+)/$', views.TaskNoteUpdateView.as_view(), name='learn_tasknote_update'),
# )
