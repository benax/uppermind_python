import unittest
from django.core.urlresolvers import reverse
from django.test import Client
from .models import Speaker, CourseCategory, Course, CourseExecution, Lesson, LessonBlock, LessonVideo, LessonExecution, LessonQuestion, ProgrammingLanguage, Task, TaskCondition, TaskOption, TaskExecution, TaskConditionHistory, TaskCodePair, TaskCodeCheck, TaskCodeCheckDetail, TaskNote, TaskCodeManualCheck, TaskCodeManualCheckNote
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_speaker(**kwargs):
    defaults = {}
    defaults["image"] = "image"
    defaults["square_image"] = "square_image"
    defaults["last_name"] = "last_name"
    defaults["name"] = "name"
    defaults["surname"] = "surname"
    defaults["sex"] = "sex"
    defaults["work_from"] = "work_from"
    defaults["work_to"] = "work_to"
    defaults.update(**kwargs)
    return Speaker.objects.create(**defaults)


def create_coursecategory(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    if "parent" not in defaults:
        defaults["parent"] = create_'self'()
    if "node" not in defaults:
        defaults["node"] = create_'knowledge_graphnode'()
    return CourseCategory.objects.create(**defaults)


def create_course(**kwargs):
    defaults = {}
    defaults["image"] = "image"
    defaults["title"] = "title"
    defaults["short_desc"] = "short_desc"
    defaults["desc"] = "desc"
    defaults["difficulty"] = "difficulty"
    defaults["cost_coins"] = "cost_coins"
    defaults["cost_cristals"] = "cost_cristals"
    defaults["cost_rubles"] = "cost_rubles"
    defaults["available_from"] = "available_from"
    defaults["available_to"] = "available_to"
    defaults.update(**kwargs)
    if "category" not in defaults:
        defaults["category"] = create_coursecategory()
    return Course.objects.create(**defaults)


def create_courseexecution(**kwargs):
    defaults = {}
    defaults["finished_at"] = "finished_at"
    defaults.update(**kwargs)
    if "course" not in defaults:
        defaults["course"] = create_course()
    if "user" not in defaults:
        defaults["user"] = create_user()
    if "selected_speaker" not in defaults:
        defaults["selected_speaker"] = create_speaker()
    return CourseExecution.objects.create(**defaults)


def create_lesson(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults["short_desc"] = "short_desc"
    defaults["html"] = "html"
    defaults["order_index"] = "order_index"
    defaults.update(**kwargs)
    if "course" not in defaults:
        defaults["course"] = create_course()
    return Lesson.objects.create(**defaults)


def create_lessonblock(**kwargs):
    defaults = {}
    defaults["block_type"] = "block_type"
    defaults["html"] = "html"
    defaults["order_index"] = "order_index"
    defaults.update(**kwargs)
    if "lesson" not in defaults:
        defaults["lesson"] = create_lesson()
    if "node" not in defaults:
        defaults["node"] = create_'knowledge_graphnode'()
    return LessonBlock.objects.create(**defaults)


def create_lessonvideo(**kwargs):
    defaults = {}
    defaults["src"] = "src"
    defaults.update(**kwargs)
    if "lesson" not in defaults:
        defaults["lesson"] = create_lesson()
    if "speaker" not in defaults:
        defaults["speaker"] = create_speaker()
    return LessonVideo.objects.create(**defaults)


def create_lessonexecution(**kwargs):
    defaults = {}
    defaults["finished_at"] = "finished_at"
    defaults.update(**kwargs)
    if "lesson" not in defaults:
        defaults["lesson"] = create_lesson()
    if "user" not in defaults:
        defaults["user"] = create_user()
    return LessonExecution.objects.create(**defaults)


def create_lessonquestion(**kwargs):
    defaults = {}
    defaults["message"] = "message"
    defaults.update(**kwargs)
    if "lesson" not in defaults:
        defaults["lesson"] = create_lesson()
    if "from_user" not in defaults:
        defaults["from_user"] = create_user()
    return LessonQuestion.objects.create(**defaults)


def create_programminglanguage(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults.update(**kwargs)
    return ProgrammingLanguage.objects.create(**defaults)


def create_task(**kwargs):
    defaults = {}
    defaults["task_type"] = "task_type"
    defaults["difficulty"] = "difficulty"
    defaults["title"] = "title"
    defaults["desc"] = "desc"
    defaults["gives_coins"] = "gives_coins"
    defaults["gives_base_experience"] = "gives_base_experience"
    defaults["order_index"] = "order_index"
    defaults.update(**kwargs)
    if "lesson" not in defaults:
        defaults["lesson"] = create_lesson()
    if "programming_language" not in defaults:
        defaults["programming_language"] = create_programminglanguage()
    return Task.objects.create(**defaults)


def create_taskcondition(**kwargs):
    defaults = {}
    defaults["need_to_use"] = "need_to_use"
    defaults["title"] = "title"
    defaults["short_desc"] = "short_desc"
    defaults["external_code_id"] = "external_code_id"
    defaults.update(**kwargs)
    if "task" not in defaults:
        defaults["task"] = create_task()
    if "node" not in defaults:
        defaults["node"] = create_'knowledge_graphnode'()
    return TaskCondition.objects.create(**defaults)


def create_taskoption(**kwargs):
    defaults = {}
    defaults["title"] = "title"
    defaults["is_correct"] = "is_correct"
    defaults.update(**kwargs)
    if "task" not in defaults:
        defaults["task"] = create_task()
    return TaskOption.objects.create(**defaults)


def create_taskexecution(**kwargs):
    defaults = {}
    defaults["answer"] = "answer"
    defaults["is_correct"] = "is_correct"
    defaults["finished_at"] = "finished_at"
    defaults.update(**kwargs)
    if "task" not in defaults:
        defaults["task"] = create_task()
    if "user" not in defaults:
        defaults["user"] = create_user()
    return TaskExecution.objects.create(**defaults)


def create_taskconditionhistory(**kwargs):
    defaults = {}
    defaults["is_correct"] = "is_correct"
    defaults.update(**kwargs)
    if "task_send" not in defaults:
        defaults["task_send"] = create_taskexecution()
    if "task_condition" not in defaults:
        defaults["task_condition"] = create_taskcondition()
    return TaskConditionHistory.objects.create(**defaults)


def create_taskcodepair(**kwargs):
    defaults = {}
    defaults["number"] = "number"
    defaults["input_str"] = "input_str"
    defaults["output_str"] = "output_str"
    defaults["on_error_text"] = "on_error_text"
    defaults.update(**kwargs)
    if "task" not in defaults:
        defaults["task"] = create_task()
    if "node" not in defaults:
        defaults["node"] = create_'knowledge_graphnode'()
    if "skill" not in defaults:
        defaults["skill"] = create_'user_app_skill'()
    return TaskCodePair.objects.create(**defaults)


def create_taskcodecheck(**kwargs):
    defaults = {}
    defaults["percent"] = "percent"
    defaults["status"] = "status"
    defaults["status_string"] = "status_string"
    defaults.update(**kwargs)
    if "task_send" not in defaults:
        defaults["task_send"] = create_taskexecution()
    return TaskCodeCheck.objects.create(**defaults)


def create_taskcodecheckdetail(**kwargs):
    defaults = {}
    defaults["status"] = "status"
    defaults.update(**kwargs)
    if "task_check" not in defaults:
        defaults["task_check"] = create_taskcodecheck()
    if "pair" not in defaults:
        defaults["pair"] = create_taskcodepair()
    return TaskCodeCheckDetail.objects.create(**defaults)


def create_tasknote(**kwargs):
    defaults = {}
    defaults["note_type"] = "note_type"
    defaults["note"] = "note"
    defaults["points"] = "points"
    defaults.update(**kwargs)
    if "task" not in defaults:
        defaults["task"] = create_task()
    return TaskNote.objects.create(**defaults)


def create_taskcodemanualcheck(**kwargs):
    defaults = {}
    defaults["mark"] = "mark"
    defaults.update(**kwargs)
    if "task_send" not in defaults:
        defaults["task_send"] = create_taskexecution()
    if "teacher" not in defaults:
        defaults["teacher"] = create_user()
    return TaskCodeManualCheck.objects.create(**defaults)


def create_taskcodemanualchecknote(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "check" not in defaults:
        defaults["check"] = create_taskcodemanualcheck()
    if "note" not in defaults:
        defaults["note"] = create_tasknote()
    return TaskCodeManualCheckNote.objects.create(**defaults)


class SpeakerViewTest(unittest.TestCase):
    '''
    Tests for Speaker
    '''
    def setUp(self):
        self.client = Client()

    def test_list_speaker(self):
        url = reverse('learn_speaker_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_speaker(self):
        url = reverse('learn_speaker_create')
        data = {
            "image": "image",
            "square_image": "square_image",
            "last_name": "last_name",
            "name": "name",
            "surname": "surname",
            "sex": "sex",
            "work_from": "work_from",
            "work_to": "work_to",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_speaker(self):
        speaker = create_speaker()
        url = reverse('learn_speaker_detail', args=[speaker.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_speaker(self):
        speaker = create_speaker()
        data = {
            "image": "image",
            "square_image": "square_image",
            "last_name": "last_name",
            "name": "name",
            "surname": "surname",
            "sex": "sex",
            "work_from": "work_from",
            "work_to": "work_to",
        }
        url = reverse('learn_speaker_update', args=[speaker.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CourseCategoryViewTest(unittest.TestCase):
    '''
    Tests for CourseCategory
    '''
    def setUp(self):
        self.client = Client()

    def test_list_coursecategory(self):
        url = reverse('learn_coursecategory_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_coursecategory(self):
        url = reverse('learn_coursecategory_create')
        data = {
            "title": "title",
            "parent": create_'self'().pk,
            "node": create_'knowledge_graphnode'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_coursecategory(self):
        coursecategory = create_coursecategory()
        url = reverse('learn_coursecategory_detail', args=[coursecategory.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_coursecategory(self):
        coursecategory = create_coursecategory()
        data = {
            "title": "title",
            "parent": create_'self'().pk,
            "node": create_'knowledge_graphnode'().pk,
        }
        url = reverse('learn_coursecategory_update', args=[coursecategory.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CourseViewTest(unittest.TestCase):
    '''
    Tests for Course
    '''
    def setUp(self):
        self.client = Client()

    def test_list_course(self):
        url = reverse('learn_course_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_course(self):
        url = reverse('learn_course_create')
        data = {
            "image": "image",
            "title": "title",
            "short_desc": "short_desc",
            "desc": "desc",
            "difficulty": "difficulty",
            "cost_coins": "cost_coins",
            "cost_cristals": "cost_cristals",
            "cost_rubles": "cost_rubles",
            "available_from": "available_from",
            "available_to": "available_to",
            "category": create_coursecategory().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_course(self):
        course = create_course()
        url = reverse('learn_course_detail', args=[course.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_course(self):
        course = create_course()
        data = {
            "image": "image",
            "title": "title",
            "short_desc": "short_desc",
            "desc": "desc",
            "difficulty": "difficulty",
            "cost_coins": "cost_coins",
            "cost_cristals": "cost_cristals",
            "cost_rubles": "cost_rubles",
            "available_from": "available_from",
            "available_to": "available_to",
            "category": create_coursecategory().pk,
        }
        url = reverse('learn_course_update', args=[course.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CourseExecutionViewTest(unittest.TestCase):
    '''
    Tests for CourseExecution
    '''
    def setUp(self):
        self.client = Client()

    def test_list_courseexecution(self):
        url = reverse('learn_courseexecution_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_courseexecution(self):
        url = reverse('learn_courseexecution_create')
        data = {
            "finished_at": "finished_at",
            "course": create_course().pk,
            "user": create_user().pk,
            "selected_speaker": create_speaker().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_courseexecution(self):
        courseexecution = create_courseexecution()
        url = reverse('learn_courseexecution_detail', args=[courseexecution.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_courseexecution(self):
        courseexecution = create_courseexecution()
        data = {
            "finished_at": "finished_at",
            "course": create_course().pk,
            "user": create_user().pk,
            "selected_speaker": create_speaker().pk,
        }
        url = reverse('learn_courseexecution_update', args=[courseexecution.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LessonViewTest(unittest.TestCase):
    '''
    Tests for Lesson
    '''
    def setUp(self):
        self.client = Client()

    def test_list_lesson(self):
        url = reverse('learn_lesson_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_lesson(self):
        url = reverse('learn_lesson_create')
        data = {
            "title": "title",
            "short_desc": "short_desc",
            "html": "html",
            "order_index": "order_index",
            "course": create_course().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_lesson(self):
        lesson = create_lesson()
        url = reverse('learn_lesson_detail', args=[lesson.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_lesson(self):
        lesson = create_lesson()
        data = {
            "title": "title",
            "short_desc": "short_desc",
            "html": "html",
            "order_index": "order_index",
            "course": create_course().pk,
        }
        url = reverse('learn_lesson_update', args=[lesson.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LessonBlockViewTest(unittest.TestCase):
    '''
    Tests for LessonBlock
    '''
    def setUp(self):
        self.client = Client()

    def test_list_lessonblock(self):
        url = reverse('learn_lessonblock_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_lessonblock(self):
        url = reverse('learn_lessonblock_create')
        data = {
            "block_type": "block_type",
            "html": "html",
            "order_index": "order_index",
            "lesson": create_lesson().pk,
            "node": create_'knowledge_graphnode'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_lessonblock(self):
        lessonblock = create_lessonblock()
        url = reverse('learn_lessonblock_detail', args=[lessonblock.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_lessonblock(self):
        lessonblock = create_lessonblock()
        data = {
            "block_type": "block_type",
            "html": "html",
            "order_index": "order_index",
            "lesson": create_lesson().pk,
            "node": create_'knowledge_graphnode'().pk,
        }
        url = reverse('learn_lessonblock_update', args=[lessonblock.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LessonVideoViewTest(unittest.TestCase):
    '''
    Tests for LessonVideo
    '''
    def setUp(self):
        self.client = Client()

    def test_list_lessonvideo(self):
        url = reverse('learn_lessonvideo_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_lessonvideo(self):
        url = reverse('learn_lessonvideo_create')
        data = {
            "src": "src",
            "lesson": create_lesson().pk,
            "speaker": create_speaker().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_lessonvideo(self):
        lessonvideo = create_lessonvideo()
        url = reverse('learn_lessonvideo_detail', args=[lessonvideo.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_lessonvideo(self):
        lessonvideo = create_lessonvideo()
        data = {
            "src": "src",
            "lesson": create_lesson().pk,
            "speaker": create_speaker().pk,
        }
        url = reverse('learn_lessonvideo_update', args=[lessonvideo.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LessonExecutionViewTest(unittest.TestCase):
    '''
    Tests for LessonExecution
    '''
    def setUp(self):
        self.client = Client()

    def test_list_lessonexecution(self):
        url = reverse('learn_lessonexecution_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_lessonexecution(self):
        url = reverse('learn_lessonexecution_create')
        data = {
            "finished_at": "finished_at",
            "lesson": create_lesson().pk,
            "user": create_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_lessonexecution(self):
        lessonexecution = create_lessonexecution()
        url = reverse('learn_lessonexecution_detail', args=[lessonexecution.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_lessonexecution(self):
        lessonexecution = create_lessonexecution()
        data = {
            "finished_at": "finished_at",
            "lesson": create_lesson().pk,
            "user": create_user().pk,
        }
        url = reverse('learn_lessonexecution_update', args=[lessonexecution.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LessonQuestionViewTest(unittest.TestCase):
    '''
    Tests for LessonQuestion
    '''
    def setUp(self):
        self.client = Client()

    def test_list_lessonquestion(self):
        url = reverse('learn_lessonquestion_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_lessonquestion(self):
        url = reverse('learn_lessonquestion_create')
        data = {
            "message": "message",
            "lesson": create_lesson().pk,
            "from_user": create_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_lessonquestion(self):
        lessonquestion = create_lessonquestion()
        url = reverse('learn_lessonquestion_detail', args=[lessonquestion.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_lessonquestion(self):
        lessonquestion = create_lessonquestion()
        data = {
            "message": "message",
            "lesson": create_lesson().pk,
            "from_user": create_user().pk,
        }
        url = reverse('learn_lessonquestion_update', args=[lessonquestion.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProgrammingLanguageViewTest(unittest.TestCase):
    '''
    Tests for ProgrammingLanguage
    '''
    def setUp(self):
        self.client = Client()

    def test_list_programminglanguage(self):
        url = reverse('learn_programminglanguage_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_programminglanguage(self):
        url = reverse('learn_programminglanguage_create')
        data = {
            "title": "title",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_programminglanguage(self):
        programminglanguage = create_programminglanguage()
        url = reverse('learn_programminglanguage_detail', args=[programminglanguage.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_programminglanguage(self):
        programminglanguage = create_programminglanguage()
        data = {
            "title": "title",
        }
        url = reverse('learn_programminglanguage_update', args=[programminglanguage.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskViewTest(unittest.TestCase):
    '''
    Tests for Task
    '''
    def setUp(self):
        self.client = Client()

    def test_list_task(self):
        url = reverse('learn_task_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_task(self):
        url = reverse('learn_task_create')
        data = {
            "task_type": "task_type",
            "difficulty": "difficulty",
            "title": "title",
            "desc": "desc",
            "gives_coins": "gives_coins",
            "gives_base_experience": "gives_base_experience",
            "order_index": "order_index",
            "lesson": create_lesson().pk,
            "programming_language": create_programminglanguage().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_task(self):
        task = create_task()
        url = reverse('learn_task_detail', args=[task.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_task(self):
        task = create_task()
        data = {
            "task_type": "task_type",
            "difficulty": "difficulty",
            "title": "title",
            "desc": "desc",
            "gives_coins": "gives_coins",
            "gives_base_experience": "gives_base_experience",
            "order_index": "order_index",
            "lesson": create_lesson().pk,
            "programming_language": create_programminglanguage().pk,
        }
        url = reverse('learn_task_update', args=[task.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskConditionViewTest(unittest.TestCase):
    '''
    Tests for TaskCondition
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskcondition(self):
        url = reverse('learn_taskcondition_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskcondition(self):
        url = reverse('learn_taskcondition_create')
        data = {
            "need_to_use": "need_to_use",
            "title": "title",
            "short_desc": "short_desc",
            "external_code_id": "external_code_id",
            "task": create_task().pk,
            "node": create_'knowledge_graphnode'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskcondition(self):
        taskcondition = create_taskcondition()
        url = reverse('learn_taskcondition_detail', args=[taskcondition.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskcondition(self):
        taskcondition = create_taskcondition()
        data = {
            "need_to_use": "need_to_use",
            "title": "title",
            "short_desc": "short_desc",
            "external_code_id": "external_code_id",
            "task": create_task().pk,
            "node": create_'knowledge_graphnode'().pk,
        }
        url = reverse('learn_taskcondition_update', args=[taskcondition.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskOptionViewTest(unittest.TestCase):
    '''
    Tests for TaskOption
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskoption(self):
        url = reverse('learn_taskoption_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskoption(self):
        url = reverse('learn_taskoption_create')
        data = {
            "title": "title",
            "is_correct": "is_correct",
            "task": create_task().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskoption(self):
        taskoption = create_taskoption()
        url = reverse('learn_taskoption_detail', args=[taskoption.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskoption(self):
        taskoption = create_taskoption()
        data = {
            "title": "title",
            "is_correct": "is_correct",
            "task": create_task().pk,
        }
        url = reverse('learn_taskoption_update', args=[taskoption.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskExecutionViewTest(unittest.TestCase):
    '''
    Tests for TaskExecution
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskexecution(self):
        url = reverse('learn_taskexecution_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskexecution(self):
        url = reverse('learn_taskexecution_create')
        data = {
            "answer": "answer",
            "is_correct": "is_correct",
            "finished_at": "finished_at",
            "task": create_task().pk,
            "user": create_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskexecution(self):
        taskexecution = create_taskexecution()
        url = reverse('learn_taskexecution_detail', args=[taskexecution.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskexecution(self):
        taskexecution = create_taskexecution()
        data = {
            "answer": "answer",
            "is_correct": "is_correct",
            "finished_at": "finished_at",
            "task": create_task().pk,
            "user": create_user().pk,
        }
        url = reverse('learn_taskexecution_update', args=[taskexecution.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskConditionHistoryViewTest(unittest.TestCase):
    '''
    Tests for TaskConditionHistory
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskconditionhistory(self):
        url = reverse('learn_taskconditionhistory_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskconditionhistory(self):
        url = reverse('learn_taskconditionhistory_create')
        data = {
            "is_correct": "is_correct",
            "task_send": create_taskexecution().pk,
            "task_condition": create_taskcondition().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskconditionhistory(self):
        taskconditionhistory = create_taskconditionhistory()
        url = reverse('learn_taskconditionhistory_detail', args=[taskconditionhistory.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskconditionhistory(self):
        taskconditionhistory = create_taskconditionhistory()
        data = {
            "is_correct": "is_correct",
            "task_send": create_taskexecution().pk,
            "task_condition": create_taskcondition().pk,
        }
        url = reverse('learn_taskconditionhistory_update', args=[taskconditionhistory.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskCodePairViewTest(unittest.TestCase):
    '''
    Tests for TaskCodePair
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskcodepair(self):
        url = reverse('learn_taskcodepair_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskcodepair(self):
        url = reverse('learn_taskcodepair_create')
        data = {
            "number": "number",
            "input_str": "input_str",
            "output_str": "output_str",
            "on_error_text": "on_error_text",
            "task": create_task().pk,
            "node": create_'knowledge_graphnode'().pk,
            "skill": create_'user_app_skill'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskcodepair(self):
        taskcodepair = create_taskcodepair()
        url = reverse('learn_taskcodepair_detail', args=[taskcodepair.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskcodepair(self):
        taskcodepair = create_taskcodepair()
        data = {
            "number": "number",
            "input_str": "input_str",
            "output_str": "output_str",
            "on_error_text": "on_error_text",
            "task": create_task().pk,
            "node": create_'knowledge_graphnode'().pk,
            "skill": create_'user_app_skill'().pk,
        }
        url = reverse('learn_taskcodepair_update', args=[taskcodepair.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskCodeCheckViewTest(unittest.TestCase):
    '''
    Tests for TaskCodeCheck
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskcodecheck(self):
        url = reverse('learn_taskcodecheck_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskcodecheck(self):
        url = reverse('learn_taskcodecheck_create')
        data = {
            "percent": "percent",
            "status": "status",
            "status_string": "status_string",
            "task_send": create_taskexecution().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskcodecheck(self):
        taskcodecheck = create_taskcodecheck()
        url = reverse('learn_taskcodecheck_detail', args=[taskcodecheck.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskcodecheck(self):
        taskcodecheck = create_taskcodecheck()
        data = {
            "percent": "percent",
            "status": "status",
            "status_string": "status_string",
            "task_send": create_taskexecution().pk,
        }
        url = reverse('learn_taskcodecheck_update', args=[taskcodecheck.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskCodeCheckDetailViewTest(unittest.TestCase):
    '''
    Tests for TaskCodeCheckDetail
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskcodecheckdetail(self):
        url = reverse('learn_taskcodecheckdetail_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskcodecheckdetail(self):
        url = reverse('learn_taskcodecheckdetail_create')
        data = {
            "status": "status",
            "task_check": create_taskcodecheck().pk,
            "pair": create_taskcodepair().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskcodecheckdetail(self):
        taskcodecheckdetail = create_taskcodecheckdetail()
        url = reverse('learn_taskcodecheckdetail_detail', args=[taskcodecheckdetail.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskcodecheckdetail(self):
        taskcodecheckdetail = create_taskcodecheckdetail()
        data = {
            "status": "status",
            "task_check": create_taskcodecheck().pk,
            "pair": create_taskcodepair().pk,
        }
        url = reverse('learn_taskcodecheckdetail_update', args=[taskcodecheckdetail.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskNoteViewTest(unittest.TestCase):
    '''
    Tests for TaskNote
    '''
    def setUp(self):
        self.client = Client()

    def test_list_tasknote(self):
        url = reverse('learn_tasknote_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_tasknote(self):
        url = reverse('learn_tasknote_create')
        data = {
            "note_type": "note_type",
            "note": "note",
            "points": "points",
            "task": create_task().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_tasknote(self):
        tasknote = create_tasknote()
        url = reverse('learn_tasknote_detail', args=[tasknote.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_tasknote(self):
        tasknote = create_tasknote()
        data = {
            "note_type": "note_type",
            "note": "note",
            "points": "points",
            "task": create_task().pk,
        }
        url = reverse('learn_tasknote_update', args=[tasknote.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskCodeManualCheckViewTest(unittest.TestCase):
    '''
    Tests for TaskCodeManualCheck
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskcodemanualcheck(self):
        url = reverse('learn_taskcodemanualcheck_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskcodemanualcheck(self):
        url = reverse('learn_taskcodemanualcheck_create')
        data = {
            "mark": "mark",
            "task_send": create_taskexecution().pk,
            "teacher": create_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskcodemanualcheck(self):
        taskcodemanualcheck = create_taskcodemanualcheck()
        url = reverse('learn_taskcodemanualcheck_detail', args=[taskcodemanualcheck.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskcodemanualcheck(self):
        taskcodemanualcheck = create_taskcodemanualcheck()
        data = {
            "mark": "mark",
            "task_send": create_taskexecution().pk,
            "teacher": create_user().pk,
        }
        url = reverse('learn_taskcodemanualcheck_update', args=[taskcodemanualcheck.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TaskCodeManualCheckNoteViewTest(unittest.TestCase):
    '''
    Tests for TaskCodeManualCheckNote
    '''
    def setUp(self):
        self.client = Client()

    def test_list_taskcodemanualchecknote(self):
        url = reverse('learn_taskcodemanualchecknote_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_taskcodemanualchecknote(self):
        url = reverse('learn_taskcodemanualchecknote_create')
        data = {
            "check_obj": create_taskcodemanualcheck().pk,
            "note": create_tasknote().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_taskcodemanualchecknote(self):
        taskcodemanualchecknote = create_taskcodemanualchecknote()
        url = reverse('learn_taskcodemanualchecknote_detail', args=[taskcodemanualchecknote.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_taskcodemanualchecknote(self):
        taskcodemanualchecknote = create_taskcodemanualchecknote()
        data = {
            "check_obj": create_taskcodemanualcheck().pk,
            "note": create_tasknote().pk,
        }
        url = reverse('learn_taskcodemanualchecknote_update', args=[taskcodemanualchecknote.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


