
lesson04 = Lesson() {
	'title': "04 Оптимизации",
	'short_desc': "",
	'html': "",
	'order_index': 4,
}, lesson04.save()


task0401 = Task() 
{
	'title': "Существование треугольника",
	'desc': "Напишите программу, которая запрашивает значения углов треугольника, и выводит на экран, может ли существовать такой треугольник, или нет.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 40,
	'gives_base_experience': 40,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 23, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "60\n30\n90",
			'output_str': "Yes",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "90\n45\n50",
			'output_str': "No",
			'on_error_text': "",
		}, 
		{
			'number': 3,
			'input_str': "0\n2\n178",
			'output_str': "No",
			'on_error_text': "Угол не может быть равен нулю",
		}, 
	],
}, 



task0402 = Task() 
{
	'title': "Время суток",
	'desc': "Программа должна вывести приветствие на основании времени суток. Деление выглядит следующим образом:\n•	00:00-04:00 - ночь;\n•	04:00-12:00 - утро;\n•	12:00-17:00 - день;\n•	17:00-24:00 - вечер.\n",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 40,
	'gives_base_experience': 40,
	'order_index': 2,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 23, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "15",
			'output_str': "Добрый день",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "20",
			'output_str': "Добрый вечер",
			'on_error_text': "",
		}, 
		{
			'number': 3,
			'input_str': "9",
			'output_str': "Доброе утро",
			'on_error_text': "",
		}, 
		{
			'number': 4,
			'input_str': "3",
			'output_str': "Доброй ночи",
			'on_error_text': "",
		}, 
	],
}, 



task0403 = Task() 
{
	'title': "Рубли",
	'desc': "Написать программу, которая дописывает слово «рубль» в правильной форме",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 60,
	'gives_base_experience': 60,
	'order_index': 3,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 23, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "50",
			'output_str': "50 рублей",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "21",
			'output_str': "21 рубль",
			'on_error_text': "",
		}, 
		{
			'number': 3,
			'input_str': "34",
			'output_str': "34 рубля",
			'on_error_text': "",
		}, 
		{
			'number': 4,
			'input_str': "11",
			'output_str': "11 рублей",
			'on_error_text': "",
		}, 
	],
}, 



task0404 = Task() 
{
	'title': "Расстояние между точками",
	'desc': "Напишите программу, которая будет запрашивать у пользователя координаты двух точек на плоскости, а затем выведет на экран расстояние между ними. Значения при вводе в следующем порядке\na.	Координата X первой точки\nb.	Координата Yпервой точки\nc.	Координата X второй точки\nd.	Координата Yвторой точки\nДля вычисления квадратного корня используйте функциюsqrtиз библиоткекиcmath\n",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 4,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 23, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "2\n5\n2\n0",
			'output_str': "5",
			'on_error_text': "",
			'judge_type': "2",
		}, 
		{
			'number': 2,
			'input_str': "7\n6\n2\n2",
			'output_str': "6.40312",
			'on_error_text': "",
			'judge_type': "2",
		}, 
	],
}, 








lesson05 = Lesson() {
	'title': "05 Работа с русским языком",
	'short_desc': "",
	'html': "",
	'order_index': 5,
}, lesson05.save()


task0501 = Task() 
{
	'title': "Трехзначное число в слова",
	'desc': "Напишите программу, которая получает на ввод трехзначное  число, а затем выводит его словесное название.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "147",
			'output_str': "Сто сорок семь",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "834",
			'output_str': "Восемьсот тридцать четыре",
			'on_error_text': "",
		}, 
		{
			'number': 3,
			'input_str': "206",
			'output_str': "Двести шесть",
			'on_error_text': "",
		}, 
		{
			'number': 4,
			'input_str': "312",
			'output_str': "Триста двенадцать",
			'on_error_text': "",
		}, 
	],
}, 




task0502 = Task() 
{
	'title': "Калькулятор",
	'desc': "Напишите программу-калькулятор, которая принимает первое число, операцию (+, -, *, /) и второе число и выводит результат",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 2,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 26, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "5\n*\n3",
			'output_str': "15",
			'on_error_text': "",
			'judge_type': "2",
		}, 
		{
			'number': 2,
			'input_str': "5\n/\n2",
			'output_str': "2.5",
			'on_error_text': "",
			'judge_type': "2",
		}, 
		{
			'number': 3,
			'input_str': "5\n+\n3",
			'output_str': "8",
			'on_error_text': "",
			'judge_type': "2",
		}, 
		{
			'number': 4,
			'input_str': "5\n-\n2",
			'output_str': "3",
			'on_error_text': "",
			'judge_type': "2",
		}, 
		{
			'number': 5,
			'input_str': "5\n/\n0",
			'output_str': "Error",
			'on_error_text': "",
			'judge_type': "2",
		}, 
	],
}, 








lesson06 = Lesson() {
	'title': "06 Практика",
	'short_desc': "",
	'html': "",
	'order_index': 6,
}, lesson06.save()


task0601 = Task() 
{
	'title': "Корни квадратного уравнения",
	'desc': "Напишите программу, которая находит корни (или корень) уравнения: ax2+bx+c=0, если они есть. Если их нет, то выводит ошибку.Программа должна запрашивать у пользователя ввести a, b и с. TODO: картинки",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 27, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "1\n2\n6",
			'output_str': "Нет корней",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "1\n-2\n1",
			'output_str': "1",
			'on_error_text': "",
		}, 
		{
			'number': 3,
			'input_str': "2\n-3\n1",
			'output_str': "1\n0.5",
			'on_error_text': "",
		}, 
	],
}, 




task0602 = Task() 
{
	'title': "Расписание занятий",
	'desc': "Напишите программу, которая запрашивает номер дня недели (пн - 1, вт - 2 и т д.) и выводит на экран ваше расписание уроков на этот день и ошибку, если введено слишком большое или маленькое число.Расписание дня выводится в соответствии с таблицей: TODO: таблица",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 2,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 27, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "3",
			'output_str': "математика\nинформатика\nлитература\nбиология\nфиз-ра\nгеография",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "10",
			'output_str': "Дня недели с таким порядковым номером не существует",
			'on_error_text': "",
		}, 
	],
}, 




task0603 = Task() 
{
	'title': "Расстояние брошенного объекта",
	'desc': "Напишите программу, которая вычисляет расстояние, которое пролетит объект, брошенный под углом к горизонту. Программа запрашивает значение угла и начальной скорости и выводит на экран расстояние. Для вычисления синуса угла используйте функцию sin из библиотеки cmath. Расстояние выводится целым числом",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 3,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 27, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "10\n10",
			'output_str': "3",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "15\n22",
			'output_str': "24",
			'on_error_text': "",
		}, 
	],
}, 









lesson07 = Lesson() {
	'title': "07 Циклы",
	'short_desc': "",
	'html': "",
	'order_index': 7,
}, lesson07.save()


task0701 = Task() 
{
	'title': "Стоимость товара",
	'desc': "Одна штука некоторого товара стоит 20,4 руб. Запросить у пользователя число и напечатать таблицу стоимости от 2 до веденного количества штук этого товара. Вывести ошибку, если введенное число ошибочно.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 30, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "6",
			'output_str': "2 - 40.8\n3 - 61.2\n4 - 81.6\n5 - 102\n6 - 122.4",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "1",
			'output_str': "Error",
			'on_error_text': "Программа должна выводить ошибку при вводе количества товаров меньше 2",
		}, 
	],
}, 



task0702 = Task() 
{
	'title': "Сумма чисел до 1 до указанного",
	'desc': "Напишите программу, которая получает на вход число, и выводит на экран сумму всех чисел до этого числа.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 2,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 30, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "10",
			'output_str': "45",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "5",
			'output_str': "10",
			'on_error_text': "",
		}, 
	],
}, 




task0703 = Task() 
{
	'title': "Треугольник из символов",
	'desc': "Напишите программу, которая получает на вход число, а затем выводит на экран перевернутый треугольник из символов *, в основании которого будет столько же символов *, сколько было указано в вводе. С каждой строкой количество символов * должно уменьшаться на 1.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 3,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 30, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "7",
			'output_str': "*******\n******\n*****\n****\n***\n**\n*",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "2",
			'output_str': "**\n*",
			'on_error_text': "",
		}, 
	],
}, 



task0704 = Task() 
{
	'title': "Перевернутое число",
	'desc': "Напишите программу, которая получает на вход число и выводит его в обратном порядке.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 4,
	'can_be_uploaded_until': datetime.datetime(2017, 10, 30, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "84937",
			'output_str': "73948",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "23",
			'output_str': "32",
			'on_error_text': "",
		}, 
	],
}, 






lesson08 = Lesson() {
	'title': "08 Switch",
	'short_desc': "",
	'html': "",
	'order_index': 8,
}, lesson08.save()


task0801 = Task() 
{
	'title': "Конвертер расстояний",
	'desc': "Напишите программу, которая получает на вход два числа, первое — номер единицы длины (целое число в диапазоне 1 – 5 включительно), второе — длина отрезка в этих единицах. Единицы длины пронумерованы следующим образом: \n1.	дециметр\n2.	километр\n3.	метр\n4.	миллиметр\n5.	сантиметр\nВывести длину отрезка в метрах.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "3\n700",
			'output_str': "700",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "5\n1078",
			'output_str': "10.78",
			'on_error_text': "",
		}, 
	],
}, 




task0802 = Task() 
{
	'title': "Предыдущая дата",
	'desc': "Даны два целых числа: D (день) и M (месяц), определяющие правильную дату не високосного года. Вывести значения D и M для даты, предшествующей указанной.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 2,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "30\n4",
			'output_str': "29\n4",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "1\n9",
			'output_str': "31\n8",
			'on_error_text': "",
		}, 
	],
}, 



task0803 = Task() 
{
	'title': "Знаки зодиака",
	'desc': "Напишите программу, которая получает на вход два целых числа: D (день) и M (месяц) (гарантируется существование даты). Нужно вывести знак Зодиака, соответствующий этой дате: \n•	Водолей (20.1–18.2)\n•	Рыбы (19.2–20.3)\n•	Овен (21.3–19.4)\n•	Телец (20.4–20.5)\n•	Близнецы (21.5–21.6)\n•	Рак (22.6–22.7)\n•	Лев (23.7–22.8)\n•	Дева (23.8–22.9)\n•	Весы (23.9–22.10)\n•	Скорпион (23.10–22.11)\n•	Стрелец (23.11–21.12)\n•	Козерог (22.12–19.1)",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 3,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "12\n",
			'output_str': "Козерог",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "10\n11",
			'output_str': "Скорпион",
			'on_error_text': "",
		}, 
	],
}, 




task0804 = Task() 
{
	'title': "Угадай число (творческое)",
	'desc': "Написать программу, которая загадывает число от 1 до 10 (включительно) и просит пользователя угадать это число. Когда введенное число совпадет с загаданным, вывести «You win!!!». В этой задаче нет тестовых данных. Оформляйте ввод и вывод для понимания пользователей (если вы отправите программу своему другу, то он должен понять, при запуске, что нужно делать без внешних инструкций)",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 100,
	'gives_base_experience': 100,
	'order_index': 4,
	'only_manual': True,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 2, 18, 30),
	'_pairs': [

	],
}, 





lesson09 = Lesson() {
	'title': "09 Практика",
	'short_desc': "Тест + практика",
	'html': "",
	'order_index': 9,
}, lesson09.save()


task0901 = Task() 
{
	'title': "Факториал",
	'desc': "Напишите программу, которая вычисляет факториал введенного числа. Гарантируется, что вводимое число находится в диапазоне от 1 до 12 включительно",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 3, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "5",
			'output_str': "5! = 120",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "10",
			'output_str': "10! = 3628800",
			'on_error_text': "",
		}, 
	],
}, 






lesson10 = Lesson() {
	'title': "10 Массивы",
	'short_desc': "",
	'html': "",
	'order_index': 10,
}, lesson10.save()


task1001 = Task() 
{
	'title': "Массив в обратном порядке",
	'desc': "Создать массив вещественных чисел из 10 элементов. Заполнить значениями от пользователя. Вывести через запятую в обратном порядке",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 6, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "1.5\n2\n3.5\n4\n5.5\n6\n7.5\n8\n9.5\n10",
			'output_str': "10, 9.5, 8, 7.5, 6, 5.5, 4, 3.5, 2, 1.5",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "9\n8\n7\n6\n5\n4\n3\n2\n1\n0",
			'output_str': "0, 1, 2, 3, 4, 5, 6, 7, 8, 9",
			'on_error_text': "",
		}, 
	],
}, 






lesson11 = Lesson() {
	'title': "11 Практика",
	'short_desc': "",
	'html': "",
	'order_index': 11,
}, lesson11.save()


task1101 = Task() 
{
	'title': "Разница зарплат",
	'desc': "В отделе работают 3 сотрудника, которые получают заработную плату в рублях. Требуется определить: на сколько зарплата самого высокооплачиваемого из них отличается от самого низкооплачиваемого.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 9, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "35000\n28000\n25000",
			'output_str': "10000",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "10000\n14000\n16000",
			'output_str': "",
			'on_error_text': "",
		}, 
	],
}, 



task1102 = Task() 
{
	'title': "Преобразователь величин",
	'desc': "Написать программу, которая преобразует величины:\n•	температуры - из градусов Цельсия в градусы по Фаренгейту (и наоборот)\n•	скорости - км/ч в м/с (и наоборот)\nПользователь выбирает сначала что он хочет перевести (температура, скорость), а затем направление перевода (км/ч->м/с или м/с ->км/ч). Программа предлагает ввести значение и считает результат.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 2,
	'only_manual': True,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 9, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "2\n1\n860",
			'output_str': "238.8888",
			'judge_type': "15",
			'on_error_text': "",
		}, 
	],
}, 








lesson12 = Lesson() {
	'title': "12 Строки",
	'short_desc': "",
	'html': "",
	'order_index': 12,
}, lesson12.save()


task1201 = Task() 
{
	'title': "Возраст",
	'desc': "Пользователь вводит дату рождения в формате «дд.мм.гггг». Программа вычисляет его возраст и выводит с правильным склонением. Пример вывода взят при вычислении возраста на 1 января 2015.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 70,
	'gives_base_experience': 70,
	'order_index': 1,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 10, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "28.12.1993",
			'output_str': "Вам 22 года",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "03.09.2000",
			'output_str': "Вам 15 лет",
			'on_error_text': "",
		}, 
		{
			'number': 3,
			'input_str': "03.08.2037",
			'output_str': "Вы еще не родились",
			'on_error_text': "Если дата рождения указана в будущем, то нужно вывести сообщение как в примере",
		}, 
	],
}, 



task1202 = Task() 
{
	'title': "Авторизация",
	'desc': "Написать программу для авторизации. Пользователь вводит логин и пароль. Если данные совпадают с данными о пользователе в программе, то выводится \"Добро пожаловать, %username%\"(вместо %username% вставить логин пользователя), если данные не подходят, то вывести \"Доступ запрещен\" и завершить программу. Программа должна обрезать лишние пробелы, если пользователь случайно нажмет пробел до или после ввода логина или пароля. Данные о пользователях:\n•	логин: «admin», пароль: «f1a2c3»\n•	логин: «masha1993», пароль: «parol»\n•	логин: «serg», пароль: «123123»",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 100,
	'gives_base_experience': 100,
	'order_index': 2,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 10, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "admin\nf1a2c3",
			'output_str': "Добро пожаловать, admin",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "serg\n123",
			'output_str': "Доступ запрещен",
			'on_error_text': "",
		}, 
	],
	'_conditions': [
		{

			'title': "Реализуйте ввод пароля c отображением звездочек вместо вводимых символов",
			'short_desc': "+5 баллов",
			'external_code_id': 0,
		},
	],
}, 



task1203 = Task() 
{
	'title': "Только уникальные символы",
	'desc': "Напишите программу, которая получает на вход строку и заменяет все повторяющиеся символы (кроме первого) символом *, после чего выводит получившуюся строку на экран.",
	'programming_language': cpp_lang,
	'task_type': 'C',
	'difficulty': '3',
	'gives_coins': 100,
	'gives_base_experience': 100,
	'order_index': 3,
	'can_be_uploaded_until': datetime.datetime(2017, 11, 10, 18, 30),
	'_pairs': [
		{
			'number': 1,
			'input_str': "2233300102038",
			'output_str': "2*3**0*1****8",
			'on_error_text': "",
		}, 
		{
			'number': 2,
			'input_str': "Мама мыла раму...",
			'output_str': "Ма***ыл**р**у.**",
			'on_error_text': "",
		}, 
	],
}, 
