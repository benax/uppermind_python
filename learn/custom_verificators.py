
def process_task(task_exec):
	is_correct = None
	res_message = None
	status = task_exec.status
	ext_code = task_exec.task.external_code_id

	if ext_code == -1:
		is_correct = task_exec.answer == "46"
		if not is_correct:
			status = "WA"
			res_message = "Программа должна выводить сумму чисел"

	return (is_correct, status, res_message)


def process_pairs(task_exec, cases):
	status_string = None
	percentage = 0
	status = task_exec.status
	ext_code = task_exec.task.external_code_id

	if ext_code == 1:
		for i in range(len(cases)):
			if cases[i].pair.external_pair_id == 1:
				if cases[i].answer.strip() == "inf":
					cases[i].status = "WA"
				else:
					cases[i].status = "AC"
	summ = 0
	for case in reversed(cases):
		if case.status == "AC":
			summ += 1
		else:
			status = case.status
			status_string = "{} на тесте {}".format(task_exec.get_status_string(case.status), case.pair.number)
			if case.pair.on_error_text and case.status in ['WA', 'RE']:
				status_string += ": " + case.pair.on_error_text

	if len(cases) > 0:
		percentage = int(summ * 100 / len(cases))
	return (percentage, status, status_string)
