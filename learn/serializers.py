from learn import models
from rest_framework import serializers
from user_app.serializers import UserSerializer
from random import shuffle
from django.utils import timezone

class SpeakerSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Speaker
        fields = (
            'pk', 
            'image', 
            'last_name', 
            'name', 
            'surname', 
            'sex', 
            # 'work_from', 
            # 'work_to', 
        )


class CourseCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.CourseCategory
        fields = (
            'pk', 
            'title', 
        )


class BaseLessonSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    last_execution_id = serializers.SerializerMethodField()

    def get_status(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            if request.user.is_authenticated():
                return obj.get_status(request.user)
    
    def get_last_execution_id(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            if request.user.is_authenticated():
                return obj.get_last_execution_id(request.user)


class LessonSerializer(BaseLessonSerializer):

    class Meta:
        model = models.Lesson
        fields = (
            'pk', 
            'title', 
            'short_desc', 
            # 'html', 
            'status', 
            'last_execution_id',
        )



class LessonPageSerializer(BaseLessonSerializer):

    class Meta:
        model = models.Lesson
        fields = (
            'pk', 
            'title', 
            'short_desc', 
            'html', 
            # 'created_at', 
            'status', 
            'last_execution_id',
        )



class BaseCourseSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    is_free = serializers.SerializerMethodField()
    difficulty_display = serializers.SerializerMethodField()

    def get_status(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            if request.user.is_authenticated():
                return obj.get_status(request.user)

    def get_is_free(self, obj):
        return obj.is_free()

    def get_difficulty_display(self, obj):
        return obj.get_difficulty_display()
    

class CourseSerializer(BaseCourseSerializer):

    class Meta:
        model = models.Course
        fields = (
            'pk', 
            'image', 
            'title', 
            'short_desc', 
            'desc', 
            'difficulty', 
            'cost_coins', 
            'cost_crystals', 
            'cost_rubles', 
            'available_from', 
            'available_to', 
            # 'created_at', 
            'is_free',
            'difficulty_display',
            'status',
        )



class CoursePageSerializer(BaseCourseSerializer):
    lessons = LessonSerializer(many=True)


    class Meta:
        model = models.Course
        fields = (
            'pk', 
            'image', 
            'title', 
            'short_desc', 
            'desc', 
            'difficulty', 
            'cost_coins', 
            'cost_crystals', 
            'cost_rubles', 
            'available_from', 
            'available_to', 
            # 'created_at', 
            'lessons',

            'is_free',
            'difficulty_display',
            'status',
        )


class CourseExecutionSerializer(serializers.ModelSerializer):
    course = CoursePageSerializer()

    class Meta:
        model = models.CourseExecution
        fields = (
            'pk', 
            'started_at', 
            'finished_at', 
            'course',
        )



class LessonBlockSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.LessonBlock
        fields = (
            'pk', 
            'block_type', 
            'html', 
            'order_index', 
        )


class LessonVideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.LessonVideo
        fields = (
            'pk', 
            'src', 
        )


class LessonExecutionSerializer(serializers.ModelSerializer):
    lesson = LessonPageSerializer()
    tasks = serializers.SerializerMethodField()

    def get_tasks(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            if request.user.is_authenticated():
                items = obj.lesson.tasks.filter(difficulty=obj.difficulty).order_by('order_index')
                print(len(items))
                serializer = TaskSerializer(instance=items, many=True, context={'request': request})
                return serializer.data

        raise Exception("Error. user is not authenticated")

    class Meta:
        model = models.LessonExecution
        fields = (
            'pk', 
            # 'started_at', 
            # 'finished_at', 
            'difficulty', 
            
            'lesson',
            'tasks',
        )



class ChildLessonQuestionSerializer(serializers.ModelSerializer):
    from_user = UserSerializer()

    class Meta:
        model = models.LessonQuestion
        fields = (
            'pk', 
            'from_user',
            'message', 
            'childrens',
        )


class BaseLessonQuestionSerializer(serializers.ModelSerializer):
    parent = None
    from_user = UserSerializer()

    class Meta:
        model = models.LessonQuestion
        fields = (
            'pk', 
            'parent',
            'from_user',
            'message', 
            'childrens',
            # 'created_at', 
        )


class LessonQuestionSerializer(BaseLessonQuestionSerializer):
    parent = BaseLessonQuestionSerializer()
    childrens = ChildLessonQuestionSerializer(many=True)



class BaseProgrammingLanguageSerializer(serializers.ModelSerializer):
    pass


class ProgrammingLanguageSerializer(BaseProgrammingLanguageSerializer):

    class Meta:
        model = models.ProgrammingLanguage
        fields = (
            'pk', 
            'title', 
            'ace_name',
            'default_filename',
        )


class ProgrammingLanguageAdminSerializer(BaseProgrammingLanguageSerializer):

    class Meta:
        model = models.ProgrammingLanguage
        fields = (
            'pk', 
            'title', 
            'ace_name',
            'default_filename',
            'lang_id',
        )


class TaskPageSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Task
        fields = (
            'pk', 
            'task_type', 
            # 'difficulty', 
            'title', 
            'desc', 
            'gives_coins', 
            'gives_base_experience', 
            # 'order_index', 
            # 'created_at', 
        )



class CompilerNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CompilerNote
        fields = (
            'pk', 
            'note_type', 
            'note', 
        )



class CompilerCheckNoteSerializer(serializers.ModelSerializer):
    note_obj = CompilerNoteSerializer()

    class Meta:
        model = models.CompilerCheckNote
        fields = (
            'pk', 
            'check_obj',
            'note_obj',
            'line_number',
        )



class BaseTaskSerializer(serializers.ModelSerializer):
    pass


class TaskListSerializer(BaseTaskSerializer):

    class Meta:
        model = models.Task
        fields = (
            'pk', 
            'task_type', 
            'title', 
        )


class TaskSerializer(BaseTaskSerializer):
    status = serializers.SerializerMethodField()
    last_execution_id = serializers.SerializerMethodField()
    programming_language = ProgrammingLanguageSerializer()
    can_be_uploaded = serializers.SerializerMethodField()

    def get_status(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user") and request.user.is_authenticated():
            return obj.get_status(request.user)
    
    def get_last_execution_id(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user") and request.user.is_authenticated():
            return obj.get_last_execution_id(request.user)

    def get_can_be_uploaded(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user") and request.user.is_authenticated():
            return obj.get_can_be_uploaded(request.user)
        return False


    class Meta:
        model = models.Task
        fields = (
            'pk', 
            'task_type', 
            # 'difficulty', 
            'title', 
            'desc', 
            'gives_coins', 
            'gives_base_experience', 
            # 'order_index', 
            'default_value',
            'cursor_pos_row',
            'cursor_pos_col',
            'programming_language',
            'status',
            'can_be_uploaded_until',
            'last_execution_id',
            'can_be_uploaded',
        )


class TaskAdminSerializer(TaskSerializer):
    programming_language = ProgrammingLanguageAdminSerializer()

    class Meta:
        model = models.Task
        fields = (
            'pk', 
            'task_type', 
            'difficulty', 
            'title', 
            'desc', 
            'gives_coins', 
            'gives_base_experience', 
            'order_index', 
            'default_value',
            'cursor_pos_row',
            'cursor_pos_col',
            'programming_language',
            'status',
            'can_be_uploaded_until',
            'last_execution_id',
            'can_be_uploaded',
            'external_code_id',
        )


class ChildTaskQuestionSerializer(serializers.ModelSerializer):
    from_user = UserSerializer()

    class Meta:
        model = models.TaskQuestion
        fields = (
            'pk', 
            'from_user',
            'message', 
            'childrens',
        )


class BaseTaskQuestionSerializer(serializers.ModelSerializer):
    parent = None
    from_user = UserSerializer()

    class Meta:
        model = models.TaskQuestion
        fields = (
            'pk', 
            'parent',
            'from_user',
            'message', 
            'childrens',
            # 'created_at', 
        )


class TaskQuestionSerializer(BaseTaskQuestionSerializer):
    parent = BaseTaskQuestionSerializer()
    childrens = ChildTaskQuestionSerializer(many=True)




class TaskNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TaskNote
        fields = (
            'pk', 
            'note_type', 
            'note', 
            'points', 
            'task',
        )



class TaskCodeManualCheckNoteSerializer(serializers.ModelSerializer):
    note_obj = TaskNoteSerializer()

    class Meta:
        model = models.TaskCodeManualCheckNote
        fields = (
            'pk', 
            'task_exec',
            'note_obj',
            'ranges',
            # 'created_at'
        )



class TaskConditionSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TaskCondition
        fields = (
            'pk', 
            'need_to_use', 
            'title', 
            'short_desc', 
            # 'created_at', 
        )


class TaskOptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TaskOption
        fields = (
            'pk', 
            'title', 
            # 'is_correct', 
        )


class TaskCodePairSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TaskCodePair
        fields = (
            'pk', 
            'number', 
            'input_str', 
            'output_str', 
            'on_error_text', 
        )

class TaskCodePairAdminSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TaskCodePair
        fields = (
            'pk', 
            'number', 
            'input_str', 
            'output_str', 
            'on_error_text', 
            'judge_type',
        )



class TaskCodeCheckDetailSerializer(serializers.ModelSerializer):
    pair = TaskCodePairSerializer()

    class Meta:
        model = models.TaskCodeCheckDetail
        fields = (
            'pk', 
            'status', 
            'pair',
        )



class BaseTaskExecutionSerializer(serializers.ModelSerializer):
    task = TaskSerializer()
    notes = serializers.SerializerMethodField()
    notes_count = serializers.SerializerMethodField()

    def get_notes(self, obj):
        entities = obj.notes.all()
        if len(entities) > 0:
            serializer = TaskCodeManualCheckNoteSerializer(entities, many=True)
            return serializer.data
        return list()

    def get_notes_count(self, obj):
        return obj.notes.count()

    def get_options(self, obj):
        if obj.task.task_type in ['O', 'M', 'S']:
            if obj.task.task_type == 'O':
                items = models.TaskOption.objects.filter(task=obj.task)
            elif obj.task.task_type == 'M':
                items = models.TaskOption.objects.filter(task=obj.task)
            elif obj.task.task_type == 'S':
                items = models.TaskOption.objects.filter(task=obj.task, is_correct=None)

            serializer = TaskOptionSerializer(instance=items, many=True)
            data = list(serializer.data)
            shuffle(data)
            return data
        return list()

    def get_pairs(self, obj):
        if obj.task.task_type == 'C':
            items = models.TaskCodePair.objects.filter(task=obj.task, number__gt=0).order_by('number')[:obj.task.code_pairs_count]
            serializer = TaskCodePairSerializer(instance=items, many=True)
            return serializer.data
        return list()
    

class TaskExecutionSerializer(BaseTaskExecutionSerializer):
    options = serializers.SerializerMethodField()
    pairs = serializers.SerializerMethodField()
    compiler_notes = CompilerCheckNoteSerializer(many=True)


    class Meta:
        model = models.TaskExecution
        fields = (
            'pk', 
            'started_at', 
            'finished_at', 
            'task',
            'answer',
            'is_correct',
            'options',
            'pairs',
            'mark',
            'notes',
            'checked_at',
            'status', 
            'status_string', 
            'percent',
            'is_active',
            'compiler_notes',
        )


class TaskExecutionAdminSerializer(BaseTaskExecutionSerializer):
    options = serializers.SerializerMethodField()
    pairs = serializers.SerializerMethodField()
    details = serializers.SerializerMethodField()
    next_pk = serializers.SerializerMethodField()
    user = UserSerializer()
    task = TaskAdminSerializer()

    class Meta:
        model = models.TaskExecution
        fields = (
            'pk', 
            'started_at', 
            'finished_at', 
            'task',
            'user',
            'answer',
            'is_correct',
            'options',
            'pairs',
            'mark',
            'notes',
            'checked_at',
            'status', 
            'status_string', 
            'percent',
            'next_pk',
            'details',
            'is_active',
        )

    def get_next_pk(self, obj):
        item = models.TaskExecution.objects.filter(pk__gt=obj.pk, finished_at__isnull=False).order_by('pk').first()
        if item:
            return item.pk

    def get_pairs(self, obj):
        if obj.task.task_type == 'C':
            items = models.TaskCodePair.objects.filter(task=obj.task, number__gt=0).order_by('number')
            serializer = TaskCodePairAdminSerializer(instance=items, many=True)
            return serializer.data
        return list()

    def get_details(self, obj):
        if obj.task.task_type == 'C':
            items = models.TaskCodeCheckDetail.objects.filter(task_exec=obj)
            serializer = TaskCodeCheckDetailSerializer(instance=items, many=True)
            return serializer.data
        return list()


class TaskExecutionFinalSerializer(BaseTaskExecutionSerializer):

    class Meta:
        model = models.TaskExecution
        fields = (
            'pk', 
            'is_correct', 
            'started_at', 
            'finished_at', 
            'task',
            'mark',
            'checked_at',
            'notes_count',
            'status_string', 
            'percent',
            'is_active',
        )


class TaskExecutionFinalItemSerializer(BaseTaskExecutionSerializer):

    class Meta:
        model = models.TaskExecution
        fields = (
            'pk', 
            'answer', 
            'is_correct', 
            'started_at', 
            'finished_at', 
            'task',
            'is_active',
        )



class TaskExecutionForNotificationSerializer(BaseTaskExecutionSerializer):
    task = TaskListSerializer()
    
    class Meta:
        model = models.TaskExecution
        fields = (
            'pk', 
            'task',
        )



class TaskConditionHistorySerializer(serializers.ModelSerializer):
    task_exec = TaskExecutionForNotificationSerializer()
    task_condition = TaskConditionSerializer()

    class Meta:
        model = models.TaskConditionHistory
        fields = (
            'pk', 
            'is_correct', 
            'task_exec',
            'task_condition',
        )




class ReferenceCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ReferenceCode
        fields = (
            'pk', 
            'short_desc',
            'code',
        )
