from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db.models import *
from django.db import models as models
from django.contrib.auth.models import User, Group

from django.contrib.contenttypes.fields import GenericRelation
from redactor.fields import RedactorField
from django.utils import timezone


#TODO: optimize order and digitize statements
def pluralize(digit, arr):
    if digit % 100 in [11,12,13,14]:
        return "{} {}".format(digit, arr[0])
    elif digit % 10 == 1:
        return "{} {}".format(digit, arr[1])
    elif digit % 10 in [2,3,4]:
        return "{} {}".format(digit, arr[2])
    return "{} {}".format(digit, arr[0])



class Speaker(models.Model):
    SEX_CHOICES = (
        ('M', 'Мужской'),
        ('F', 'Женский'),
    )
    image = ImageField("Фото", max_length=100, upload_to='upload/speaker/')
    last_name = CharField("Фамилия", max_length=100, blank=True)
    name = CharField("Имя", max_length=50)
    surname = CharField("Отчество", max_length=80, blank=True)
    sex = CharField("Пол", max_length=1, choices=SEX_CHOICES)

    work_from = DateField("Работает с")
    work_to = DateField("Работал до", blank=True, null=True)

    def __str__(self):
        if self.last_name and self.name:
            return "{} {}".format(self.last_name, self.name)
        return self.name

    class Meta:
        verbose_name = 'Спикер'
        verbose_name_plural = 'Спикеры'



class CourseCategory(models.Model):
    parent = ForeignKey('self', verbose_name="Родитель", null=True, blank=True, related_name='childrens')
    node = ForeignKey('knowledge.GraphNode', verbose_name="Нод", on_delete=models.CASCADE)

    title = CharField("Название", max_length=255)

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Категория курса'
        verbose_name_plural = 'Категории курсов'



class Course(models.Model):
    DIFFICALTY_CHOICES = (
        ('1', 'Полный ноль'),
        ('2', 'Новичок'),
        ('3', 'Знающий'),
        ('4', 'Продвинутый'),
        ('5', 'Профессионал'),
    )
    category = ForeignKey(CourseCategory, verbose_name="Категория", related_name='courses')
    group = OneToOneField(Group, blank=True, null=True, unique=True, verbose_name="Группа пользователей", help_text='Участники ограничены в отправке решений', on_delete=models.CASCADE, related_name="course")
    
    image = ImageField("Изображение", null=True, blank=True, max_length=100, upload_to='upload/course/')
    title = CharField("Название", max_length=255)
    short_desc = TextField("Краткое описание", blank=True)
    desc = RedactorField(verbose_name="Описание", allow_file_upload=False, allow_image_upload=False, blank=True)
    difficulty = CharField("Сложность", max_length=1, choices=DIFFICALTY_CHOICES)

    cost_coins = IntegerField("Требует монет", default=0)
    cost_crystals = IntegerField("Требует кристалов", default=0)
    cost_rubles = DecimalField("Требует рублей", default=0, max_digits=10, decimal_places=2)

    available_from = DateTimeField("Будет доступен", blank=True, null=True)
    available_to = DateTimeField("Дата закрытия", blank=True, null=True)
    created_at = DateTimeField("Добавлен", auto_now_add=True)


    def __str__(self):
        return self.title


    def is_free(self):
        return self.cost_coins == 0 and self.cost_crystals == 0 and self.cost_rubles == 0


    def get_cost_string(self):
        arr = []
        if self.cost_rubles > 0:
            arr.append(pluralize(self.cost_rubles, ["рублей", "рубль", "рубля"]))
        if self.cost_crystals > 0:
            arr.append(pluralize(self.cost_crystals, ["кристалов", "кристал", "кристала"]))
        if self.cost_coins > 0:
            arr.append(pluralize(self.cost_coins, ["монет", "монета", "монеты"]))

        if len(arr) == 3:
            return "за {}, {} и {}".format(*arr)
        elif len(arr) == 2:
            return "за {} и {}".format(*arr)
        elif len(arr) == 1:
            return "за {}".format(arr[0])
        else:
            print("error: cost arr has unknown length")
            #TODO: log error
        return 'бесплатно'


    def get_absolute_url(self):
        return reverse('course_page', args=[str(self.pk)])


    def get_status(self, request_user):
        for x in request_user.course_executions.all():
            if self.pk == x.course.pk and x.finished_at is not None:
                return 2
            elif self.pk == x.course.pk:
                return 1
        return 0


    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'



class CourseExecution(models.Model):
    course = ForeignKey(Course, verbose_name="Курс", related_name='executions')
    user = ForeignKey(User, verbose_name="Пользователь", related_name='course_executions')

    started_at = DateTimeField("Начат", auto_now_add=True)
    finished_at = DateTimeField("Завершен", blank=True, null=True)
    selected_speaker = ForeignKey(Speaker, verbose_name="Выбранный спикер", blank=True, null=True)
    

    def __str__(self):
        return "{} {}".format(self.user, self.course)


    class Meta:
        verbose_name = 'Прохождение курса'
        verbose_name_plural = 'Прохождения курсов'





class Lesson(models.Model):
    course = ForeignKey(Course, verbose_name="Курс", related_name='lessons')

    title = CharField("Название", max_length=255)
    short_desc = TextField("Краткое описание", blank=True)
    html = RedactorField(verbose_name="Текст", allow_file_upload=False, allow_image_upload=False, blank=True)

    created_at = DateTimeField("Создан", auto_now_add=True)
    order_index = PositiveSmallIntegerField("Индекс сортировки", default=99)


    def __str__(self):
        return "{} ({})".format(self.title, self.course.title)


    # TODO: оптимизировать запрос
    def get_status(self, request_user):
        for x in request_user.lesson_executions.all().order_by('-id'):
            if self.pk == x.lesson.pk and x.finished_at is not None:
                return 2
            elif self.pk == x.lesson.pk:
                return 1
        return 0

    # TODO: оптимизировать запрос
    def get_last_execution_id(self, request_user):
        obj = LessonExecution.objects.filter(user=request_user, lesson=self).last()
        if obj:
            return obj.id
        # for x in request_user.lesson_executions.all().order_by('-id'):
        #     if self.pk == x.lesson.pk:
        #         return x.id


    class Meta:
        verbose_name = 'Урок'
        verbose_name_plural = 'Уроки'
        ordering = ('order_index',)

    def get_absolute_url(self):
        return reverse('lesson_page', args=[str(self.pk)])


class LessonBlock(models.Model):
    BLOCK_TYPES = (
        ('HTML', 'HTML'),
        ('PYTHON', 'Код Python'),
        ('CPP', 'Код C++'),
        ('CSHARP', 'Код C#'),
    )
    lesson = ForeignKey(Lesson, verbose_name="Урок", related_name='lesson_blocks')
    node = ForeignKey('knowledge.GraphNode', verbose_name="Нод", on_delete=models.CASCADE)
    
    block_type = CharField("Тип", max_length=20, choices=BLOCK_TYPES)
    html = RedactorField(verbose_name="HTML", allow_file_upload=False, allow_image_upload=False, blank=True)
    order_index = PositiveSmallIntegerField("Индекс сортировки", default=99)

    def __str__(self):
        return "{} {}".format(self.lesson, self.node)

    
    class Meta:
        verbose_name = 'Блок урока'
        verbose_name_plural = 'Блоки уроков'
        ordering = ('order_index',)




class LessonVideo(models.Model):
    lesson = ForeignKey(Lesson, verbose_name="Урок", related_name='lesson_videos')
    speaker = ForeignKey(Speaker, verbose_name="Спикер")
    src = CharField("Путь", max_length=255)


    def __str__(self):
        return "{} {}".format(self.lesson, self.speaker)

    
    class Meta:
        verbose_name = 'Видео урока'
        verbose_name_plural = 'Видео уроков'



class LessonExecution(models.Model):
    DIFFICALTY_CHOICES = (
        ('1', 'Очень легкий'),
        ('2', 'Легкий'),
        ('3', 'Средний'),
        ('4', 'Сложный'),
        ('5', 'Очень сложный'),
    )
    lesson = ForeignKey(Lesson, verbose_name="Урок")
    user = ForeignKey(User, verbose_name="Пользователь", related_name='lesson_executions')
    started_at = DateTimeField("Начат", auto_now_add=True)
    difficulty = CharField("Сложность задач в уроке", max_length=1, choices=DIFFICALTY_CHOICES, default='3')
    finished_at = DateTimeField("Завершен", blank=True, null=True)
    

    def calc_difficulty(self):
        #TODO: use history
        return "3"

    def select_difficulty(self):
        self.difficulty = self.calc_difficulty()
        #TODO: add

    def __str__(self):
        return "{} {}".format(self.user, self.lesson)


    class Meta:
        verbose_name = 'Прохождение урока'
        verbose_name_plural = 'Прохождения уроков'



class LessonQuestion(models.Model):
    parent = ForeignKey('self', verbose_name='Родитель', blank=True, null=True, related_name='childrens')
    lesson = ForeignKey(Lesson, verbose_name="Урок")
    from_user = ForeignKey(User, verbose_name="От пользователя")
    message = TextField("Сообщение")
    created_at = DateTimeField("Добавлен", auto_now_add=True)

    def __str__(self):
        return "{} {}".format(self.from_user, self.message[:30])


    class Meta:
        verbose_name = 'Вопрос к уроку'
        verbose_name_plural = 'Вопросы к урокам'



class ProgrammingLanguage(models.Model):
    title = CharField("Название", max_length=50)
    default_filename = CharField("Имя файла по-умолчанию", max_length=50)
    ace_name = CharField("Ace название", max_length=16, blank=True)
    ver = CharField("Версия", max_length=32, blank=True)
    lang_id = PositiveSmallIntegerField("ID в системе", default=0)

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Язык программирования'
        verbose_name_plural = 'Языки программирования'



class TaskGroup(models.Model):
    title = CharField("Название", max_length=255)
    button_text = CharField("Текст на кнопке", help_text='Если пустое значение - "Пройти тест"', max_length=255, blank=True)

    def __str__(self):
        if self.button_text:
            return "{} ({})".format(self.title, self.button_text)
        return self.title



class Task(models.Model):
    TYPE_CHOICES = (
        ('O', 'Один правильный'),
        ('M', 'Несколько правильных'),
        ('S', 'Последовательность'),
        ('A', 'Ответ'),
        ('C', 'Код'),
    )
    DIFFICALTY_CHOICES = (
        ('1', 'Очень легкий'),
        ('2', 'Легкий'),
        ('3', 'Средний'),
        ('4', 'Сложный'),
        ('5', 'Очень сложный'),
    )
    MASTER_JUDGE_CHOICES = (
        ('0', '1000. Generic masterjudge'),
        ('1', '1001. Score is % of correctly solved sets'),
    )

    lesson = ForeignKey(Lesson, verbose_name="Урок", related_name='tasks')
    task_group = ForeignKey(TaskGroup, verbose_name="Группа уроков", related_name='tasks', blank=True, null=True)
    node = ForeignKey('knowledge.GraphNode', verbose_name="Нод", on_delete=models.CASCADE, null=True, blank=True)
    programming_language = ForeignKey(ProgrammingLanguage, verbose_name="Язык программирования", blank=True, null=True)
    
    task_type = CharField("Тип", max_length=1, choices=TYPE_CHOICES)
    difficulty = CharField("Сложность", max_length=1, choices=DIFFICALTY_CHOICES)

    title = CharField("Название", max_length=255)
    desc = RedactorField(verbose_name="Описание", allow_file_upload=False, allow_image_upload=False, blank=True)

    default_value = TextField("Значение по-умолчанию", blank=True)
    cursor_pos_row = PositiveSmallIntegerField("Позиция курсора в строке", blank=True, null=True)
    cursor_pos_col = PositiveSmallIntegerField("Позиция курсора в столбце", blank=True, null=True)
    
    gives_coins = PositiveSmallIntegerField("Дает монет")
    gives_base_experience = PositiveSmallIntegerField("Дает опыта")
    can_be_uploaded_until = DateTimeField("Может быть отправлен до", blank=True, null=True)

    external_code_id = PositiveSmallIntegerField(help_text="Id дополнительного условия в коде проверки (если нет - 0)", default=0)
    order_index = PositiveIntegerField("Индекс сортировки", default=99)
    only_manual = BooleanField("Только ручная проверка", default=False)
    code_pairs_count = PositiveSmallIntegerField("Кол-во тестов в примерах", default=2)
    master_judge = CharField("Master judge", choices=MASTER_JUDGE_CHOICES, max_length=1, blank=True, null=True)
    created_at = DateTimeField("Добавлен", auto_now_add=True)


    def __str__(self):
        return "{} ({})".format(self.title, self.lesson.title)

    def get_can_be_uploaded(self, request_user):
        now = timezone.now()
        if request_user.reference_views.filter(task=self).exists():
            return False
        elif self.can_be_uploaded_until and self.lesson.course.group and request_user.groups.filter(pk=self.lesson.course.group.pk).exists(): 
            return self.can_be_uploaded_until > now
        elif self.can_be_uploaded_until and not self.lesson.course.group:
            return self.can_be_uploaded_until > now
        return True

    def get_status(self, request_user):
        if not self.get_can_be_uploaded(request_user):
            obj = TaskExecution.objects.filter(user=request_user, task=self, finished_at__isnull=False, is_active=True).last()
            if obj:
                return 2  # open
            return 0  # start
        else:
            obj = TaskExecution.objects.filter(user=request_user, task=self, is_active=True).last()
            if obj:
                if obj.finished_at is None:
                    return 1  # continue
                else: 
                    return 2  # open
            return 0  # start

    def get_last_execution_id(self, request_user):
        if not self.get_can_be_uploaded(request_user):
            obj = TaskExecution.objects.filter(user=request_user, task=self, finished_at__isnull=False, is_active=True).last()
            if obj:
                return obj.id
        else:
            obj = TaskExecution.objects.filter(user=request_user, task=self, is_active=True).last()
            if obj:
                return obj.id

    def get_sequence_options(self):
        return self.options.filter(is_correct=False)

    sequence_options = property(get_sequence_options)
    
    class Meta:
        verbose_name = 'Задание'
        verbose_name_plural = 'Задания'
        ordering = ('order_index',)



class TaskCondition(models.Model):
    task = ForeignKey(Task, verbose_name="Задание", related_name='conditions')
    node = ForeignKey('knowledge.GraphNode', verbose_name="Нод", on_delete=models.CASCADE, null=True, blank=True)
    need_to_use = NullBooleanField("Необходимо использовать", help_text='Например "Не использовать функцию sum" или "Необходимо использовать циклы"', default=None)
    
    title = CharField("Название", max_length=255)
    short_desc = CharField("Краткое описание", blank=True, max_length=255)
    uid = CharField("UID", help_text='Идентификатор для получения в коде', max_length=255, unique=True)

    gives_coins = PositiveSmallIntegerField("Дает монет")
    gives_base_experience = PositiveSmallIntegerField("Дает опыта")
    created_at = DateTimeField("Добавлен", auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Дополнительное условие'
        verbose_name_plural = 'Дополнительные условия'




class TaskOption(models.Model):
    task = ForeignKey(Task, verbose_name="Задание", related_name='options')
    title = CharField("Вариант", max_length=255)
    message = CharField("Сообщение после выполнения", max_length=255, blank=True, null=True)
    is_correct = NullBooleanField("Правильный", default=False)


    def __str__(self):
        return "{} ({})".format(self.title, self.task.title)


    class Meta:
        verbose_name = 'Вариант ответа'
        verbose_name_plural = 'Варианты ответов'



class TaskExecution(models.Model):
    STATUSES = (
        ('NO', 'Не проверено'),
        ('TO', 'Время отправки решения вышло'),
        ('RCV', 'Уже просмотрен эталонный код'),
        ('CE', 'Ошибка компиляции'),
        ('TLE', 'Превышено время выполнения'),
        ('MO', 'Превышена память'),
        ('RE', 'Ошибка выполнения'),
        ('UA', 'Неизвестный ответ'),
        ('WA', 'Неверный ответ'),
        ('AC', 'Зачтена'),
        ('OK', 'Верно'),
    )
    task = ForeignKey(Task, verbose_name="Задание")
    user = ForeignKey(User, verbose_name="Пользователь")
    
    answer = TextField("Ответ", blank=True)
    is_correct = NullBooleanField("Правильный", default=None)
    status = CharField("Статус", choices=STATUSES, max_length=3, default='NO')
    status_string = CharField("Строка статуса", max_length=255, blank=True)
    
    started_at = DateTimeField("Начат", auto_now_add=True)
    finished_at = DateTimeField("Завершен", null=True, blank=True)

    # teacher = ForeignKey(User, verbose_name="Учитель")
    percent = PositiveSmallIntegerField("Процент правильности", default=0)
    mark = DecimalField("Очков", help_text="от 0 до 1", decimal_places=2, max_digits=3, default=0)
    checked_at = DateTimeField("Проверен", null=True, blank=True)
    is_active = BooleanField("Активен", default=True)
    updated_at = DateTimeField("Обновлен", auto_now=True)

    def get_status_string(self, val):
        for k, v in self.STATUSES:
            if k == val:
                return v

    def __str__(self):
        return '%s' % self.pk
        # return "{} ({})".format(self.task.title, self.user.username)


    class Meta:
        verbose_name = 'Выполнение задания'
        verbose_name_plural = 'Выполнения заданий'



class ReferenceCode(models.Model):
    task = ForeignKey(Task, verbose_name="Задача")
    short_desc = CharField("Краткое описание", max_length=255, blank=True)
    code = TextField("Код")

    def __str__(self):
        return "{} {}".format(self.task, self.short_desc)


    class Meta:
        verbose_name = 'Эталонное решение'
        verbose_name_plural = 'Эталонные решения'



class ReferenceCodeView(models.Model):
    user = ForeignKey(User, verbose_name='Пользователь', related_name='reference_views')
    task = ForeignKey(Task, verbose_name="Задача")
    created_at = DateTimeField("Добавлен", auto_now_add=True)

    class Meta:
        verbose_name = 'Просмотр эталонный решений задачи'
        verbose_name_plural = 'Просмотры эталонных решений задач'


class TaskQuestion(models.Model):
    parent = ForeignKey('self', verbose_name='Родитель', blank=True, null=True, related_name='childrens')
    task = ForeignKey(Task, verbose_name="Задача")
    from_user = ForeignKey(User, verbose_name="От пользователя")
    message = TextField("Сообщение")
    created_at = DateTimeField("Добавлен", auto_now_add=True)

    def __str__(self):
        return "{} {}".format(self.from_user, self.message[:30])


    class Meta:
        verbose_name = 'Вопрос к задаче'
        verbose_name_plural = 'Вопросы к задачам'



class TaskConditionHistory(models.Model):
    task_exec = ForeignKey(TaskExecution, verbose_name="Выполненное задание", related_name='conditions')
    task_condition = ForeignKey(TaskCondition, verbose_name="Условие выполнения")
    is_correct = NullBooleanField("Правильно", default=None)
    created_at = DateTimeField("Добавлен", auto_now_add=True)

    #def __str__(self):
    #    return self.title

    class Meta:
        verbose_name = 'Выполнение условия'
        verbose_name_plural = 'Выполнения условий'



class TaskCodePair(models.Model):
    JUDGE_CHOICES = (
        ('1', 'Игнорировать пробелы'),
        ('2', 'Число с точностью до 2 знаков'),
        ('3', 'Число с точностью 6 знаков'),
        ('10', 'Точное совпадение'),
        ('15', 'Наличие в строке'),
    )
    task = ForeignKey(Task, verbose_name="Задание", related_name='pairs')
    node = ForeignKey('knowledge.GraphNode', verbose_name="Нод", on_delete=models.CASCADE, null=True, blank=True)
    skill = ForeignKey('user_app.Skill', verbose_name="Навык", on_delete=models.CASCADE, null=True, blank=True)

    number = PositiveSmallIntegerField("Номер теста")
    input_str = TextField("Ввод", help_text="Используйте только ASCII буквы, чтобы избежать проблем с кодировкой", blank=True)
    output_str = TextField("Вывод", help_text="Используйте только ASCII буквы, чтобы избежать проблем с кодировкой", blank=True)
    on_error_text = CharField("Текст при ошибке", help_text="Выводится после провального решения. Используйте как совет", blank=True, max_length=255)
    external_pair_id = PositiveSmallIntegerField(help_text="Id дополнительного условия в коде проверки (если нет - 0)", default=0)
    judge_type = CharField("Судья", max_length=6, choices=JUDGE_CHOICES, default='1')

    def __str__(self):
        return "{} ({})".format(self.task.title, self.number)


    class Meta:
        verbose_name = 'Пара проверок'
        verbose_name_plural = 'Пары проверок'




class TaskCodeCheckDetail(models.Model):
    PAIR_STATUSES = (
        ('AC', 'Зачтена'),
        ('WA', 'Неверный ответ'),
        ('TLE', 'Превышено время выполнения'),
        ('RE', 'Ошибка выполнения'),
        ('MO', 'Превышена память'),
        ('IE', 'Внутренняя ошибка'),
    )
    task_exec = ForeignKey(TaskExecution, verbose_name="Решение", related_name='details')
    pair = ForeignKey(TaskCodePair, verbose_name="Пара")

    status = CharField("Статус", choices=PAIR_STATUSES, max_length=3)
    answer = None


    def __str__(self):
        return "{} ({})".format(self.task_exec.task.title, self.status)


    class Meta:
        verbose_name = 'Проверка пары'
        verbose_name_plural = 'Проверки пар'



class TaskNote(models.Model):
    NOTE_TYPES = (
        ('SYNTAX', 'Синтаксис'),
        ('INDENT', 'Отступы'),
        ('NAMES', 'Названия'),
        ('STRUCT', 'Структура'),
        ('STYLE', 'Стиль кода'),
        ('OPTIM', 'Оптимизация'),
        ('TYPES', 'Типы данных'),
    )
    task = ForeignKey(Task, verbose_name="Задание", blank=True, null=True)

    note_type = CharField("Тип", max_length=6, choices=NOTE_TYPES)
    note = CharField("Заметка", max_length=255)
    points = DecimalField("Очков", help_text="от -0.5 до 0.5", decimal_places=1, max_digits=3)


    def __str__(self):
        return "{} ({})".format(self.note, self.points)

    class Meta:
        verbose_name = 'Заметка по коду'
        verbose_name_plural = 'Заметки по коду'



class TaskCodeManualCheckNote(models.Model):
    task_exec = ForeignKey(TaskExecution, verbose_name="Проверка", related_name='notes')
    note_obj = ForeignKey(TaskNote, verbose_name="Заметка")
    ranges = CharField("Диапазоны", max_length=255, help_text='Пример: 1_1:1_10', blank=True)
    created_at = DateTimeField("Добавлен", auto_now_add=True)

    class Meta:
        verbose_name = 'Заметка ручной проверки'
        verbose_name_plural = 'Заметки ручной проверки'



class ManualDifficultySelect(models.Model):
    RATE_CHOICES = (
        (2, 'Очень сложно'),
        (1, 'Сложно'),
        (0, 'Нормально'),
        (-1, 'Легко'),
        (-2, 'Очень легко'),
    )
    user = ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE, blank=True, null=True)
    task_exec = ForeignKey(TaskExecution, verbose_name="Решение")
    rate = PositiveSmallIntegerField("Смещение", choices=RATE_CHOICES, default=0)
    created_at = DateTimeField("Создан", auto_now_add=True)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Ручной выбор сложности'
        verbose_name_plural = 'Ручные выборы сложности'


class CompilerNote(models.Model):
    TYPES = (
        ('0', 'Ошибка'),
        ('1', 'Предупреждение'),
        ('2', 'Заметка'),
    )
    note_type = CharField("Тип", max_length=1, choices=TYPES)
    note = TextField("Заметка")

    def __str__(self):
        return self.note

    class Meta:
        verbose_name = 'Заметка компилятора'
        verbose_name_plural = 'Заметки компилятора'


class CompilerCheckNote(models.Model):
    check_obj = ForeignKey(TaskExecution, verbose_name="Проверка", related_name='compiler_notes')
    note_obj = ForeignKey(CompilerNote, verbose_name="Заметка")
    line_number = PositiveIntegerField("Строка")

    def __str__(self):
        return "{} ({})".format(self.note_obj.note, self.line_number)

    class Meta:
        verbose_name = 'Сообщение компилятора'
        verbose_name_plural = 'Сообщения компилятора'
